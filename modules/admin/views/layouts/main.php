<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use  yii\widgets\Menu;
use yii\helpers\Url;
use app\modules\admin\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="skin-blue sidebar-mini">

<?php $this->beginBody() ?>

<div class="wrapper">

<header class="main-header">
    <!-- Logo -->
    <a href="<?=  Yii::$app->homeUrl ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>A</b>LT</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?= Url::to(['site/logout'])?>" data-method="post" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
               <!-- Control Sidebar Toggle Button -->
            </ul>
          </li>
        </ul>
      </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <?php
      echo Menu::widget([
                  'options' => ['tag' => 'ul class="sidebar-menu" data-widget="tree"'],
                  'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
                  'items' => [
                      ['label' => 'MAIN NAVIGATION', 'options' => ['class' => 'header']],
                      
                      ['label' => 'Dashboard', 'url' => ['site/index'], 'template' => '<a href="{url}"><i class="fa fa-dashboard"></i> <span>{label}</span></a>'],

                      ['label' => 'Article', 'url' => 'article/index', 'options'=> ['class'=>'treeview'], 'template' => '<a href="{url}"><i class="fa fa-files-o"></i><span>{label}</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>', 'items' => [
                          ['label' => 'Article List', 'url' => ['article/index'], 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                          ['label' => 'Approve Request', 'url' => '#', 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                      ]],

                      ['label' => 'Short Story', 'url' => '#', 'options'=> ['class'=>'treeview'], 'template' => '<a href="{url}"><i class="fa fa-files-o"></i><span>{label}</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>', 'items' => [
                          ['label' => 'List SS', 'url' => ['ss/index'], 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                          ['label' => 'Approve Request', 'url' => '#', 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                      ]],

                      ['label' => 'Topics', 'url' => '#', 'options'=> ['class'=>'treeview'], 'template' => '<a href="{url}"><i class="fa fa-files-o"></i><span>{label}</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>', 'items' => [
                          ['label' => 'Topic List', 'url' => ['topic/index'], 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                          ['label' => 'Super Topics', 'url' => ['super-topic/index'], 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                          ['label' => 'Default Home and Nav. Topics', 'url' => '#', 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                      ]],

                      ['label' => 'User', 'url' => '#', 'options'=> ['class'=>'treeview'], 'template' => '<a href="{url}"><i class="fa fa-files-o"></i><span>{label}</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>', 'items' => [
                          ['label' => 'Manage User', 'url' => ['panel/index'], 'template' => '<li><a href="{url}"><i class="fa fa-circle-o"></i>{label}</a></li>'],
                      ]],
                  ],
              ]);


      ?>
    </section>
    <!-- /.sidebar -->
</aside>


<div class="content-wrapper">

    <section class="content-header">
      <h1>
        List of Articles
        <small>Manage Articles</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
