<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TopicSuper */

$this->title = 'Update Topic Super: ' . $model->super_topic_id;
$this->params['breadcrumbs'][] = ['label' => 'Topic Supers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->super_topic_id, 'url' => ['view', 'id' => $model->super_topic_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="topic-super-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,  'media'=> $media
    ]) ?>

</div>
