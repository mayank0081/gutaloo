<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TopicSuperSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Topic Supers';
$this->params['breadcrumbs'][] = $this->title;
?>


<?= Html::a('Create Super Topic', ['create'], ['class' => 'btn bg-navy btn-flat margin']) ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <div class="box-body">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        'super_topic_id',
                        'topic_name',
                        //'tag_line',
                        //'logo_path',
                        //'cover_img_path',
                        //'description',
                        //'slug',
                        'page_status',
                        //'created_at',
                        //'created_by_user_id',
                        //'approved_by_user_id',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>


<div class="topic-super-index">

    
</div>
