<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TopicSuper */

$this->title = $model->super_topic_id;
$this->params['breadcrumbs'][] = ['label' => 'Topic Supers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topic-super-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->super_topic_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->super_topic_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'super_topic_id',
            'topic_name',
            'tag_line',
            'logo_path',
            'cover_img_path',
            'description',
            'slug',
            'page_status',
            'created_at',
            'created_by_user_id',
            'approved_by_user_id',
        ],
    ]) ?>

</div>
