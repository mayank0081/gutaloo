<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TopicSuper */

$this->title = 'Create Topic Super';
$this->params['breadcrumbs'][] = ['label' => 'Topic Supers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

