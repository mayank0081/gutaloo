<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;
use kartik\range\RangeInput;
/* @var $this yii\web\View */
/* @var $model backend\models\TopicSuper */
/* @var $form yii\widgets\ActiveForm */
$form = ActiveForm::begin();
?>


<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Select2</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6"> 
                <div class="form-group">
                    <?= $form->field($model, 'topic_name')->input('text', ['placeholder'=>'Enter Topic Name',]) ?>       
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'tag_line')->textArea([ 
                        'placeholder' => 'Enter tag line at max 100 character .....', 
                        'rows' => 2
                    ])?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'description')->textArea([
                        'id' => 'address-input', 
                        'placeholder' => 'Enter description at max 175 character.....', 
                        'rows' => 4
                    ])?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'slug', [
                        'addon'=> [
                            'prepend' => [
                                'content' => 'www.gutaloo.com/topic/id/',
                                'options' => [
                                    'style' => 'background-color: #e6e4ff'
                                ]
                            ]
                        ]
                    ])->input('text', ['placeholder'=>'URL',]) ?>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                 <?php
                 if(!is_null($model->logo_path)){               
                    $initialPreview = Url::base()."/media/topic/s/".$model->super_topic_id."/".$model->logo_path;
                    
                    $initialPreviewConfig = array(array("caption"=> $model->logo_path, "key" => $model->super_topic_id ));   
                }else{
                    $initialPreview = "";
                    $initialPreviewConfig = "";
                }
                               
                ?>
                <?=  $form->field($model, 'logoImg')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'minImageWidth'=> 220,
                            'minImageHeight' => 150,
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false,
                            'showCancel' => false,
                            'deleteUrl' => Url::to(['/admin/super-topic/deleteimg', "type" => "img"]),
                            'initialPreview'=>$initialPreview,
                            'initialPreviewAsData'=>true,
                            'initialPreviewConfig' => $initialPreviewConfig,
                            'overwriteInitial'=>true,
                        ]
                    ]); 


                ?> 
               </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Cover Image</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <?php    
                if(is_null($model->cover_img_path)){
                    $initialPreview = "";
                    $initialPreviewConfig = "";
                }else{         
                    $initialPreview = Url::base()."/media/topic/s/".$model->super_topic_id."/".$model->cover_img_path;
                
                    $initialPreviewConfig = array(array("caption"=> $model->cover_img_path, "key" => $model->super_topic_id ));   
                }       
            ?>
            <?= $form->field($model, 'coverImg')->widget(FileInput::classname(),[
                    'name' => 'coverImg',
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                       // 'uploadUrl' => Url::to(['/site/wp']),
                        'deleteUrl' => Url::to(['/admin/super-topic/deleteimg', "type" => "cover"]),
                        'minImageWidth'=> 1600,
                        'minImageHeight' => 400,
                        'maxFileCount' => 1,
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'showCancel' => false,
                        'initialPreview'=>$initialPreview,
                       'initialPreviewAsData'=>true,
                       'initialPreviewConfig' => $initialPreviewConfig,
                        'overwriteInitial'=>true,
                        'maxFileCount' => 1,
                    ]
                ]);
            ?>
          </div>
        </div>
        <div class="col-md-6">
          
          <?= $form->field($model, 'transparent_score')->widget(
          RangeInput::classname(),[
            'name' => 'range_1',
            'value' => 50,
            'html5Container' => ['style' => 'width:350px'],
            'html5Options' => ['min' => 0, 'max' => 100],
            'addon' => ['append' => ['content' => '%']]
            ]) 
          ?>
          <div style="width: 400px; height: 150px;background-size: 100%;">
              <div  id="coverImage" style="width: 400px; height: 150px; background-color: black; opacity: 0.5"></div>
          </div>

        </div>
      </div>  
      
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <?php
                if(isset($media)){
                    $count = count($media);
                    $initialPreview = array();
                    $initialPreviewConfig = array();
                    for ($i = 0; $i< $count; $i++){
                        
                        $initialPreview[] = Url::base()."/media/topic/s/".$model->super_topic_id."/".$media[$i]["media_path"];
                        
                        $initialPreviewConfig[] = array("caption"=> $media[$i]["media_path"], "key" =>$media[$i]["id"]);                   
                    }
                }else{
                    $initialPreview = "";
                    $initialPreviewConfig = "";
                }
                
            ?>

            <?= $form->field($model, 'extraCoverImg[]')->widget(FileInput::classname(),[
                    'name' => 'attachment_48[]',
                    'options'=>[
                        'multiple'=>true
                    ],
                    'pluginOptions' => [
                      //  'uploadUrl' => Url::to(['/admin/super-topic/deleteimg']),
                        'deleteUrl' => Url::to(['/admin/super-topic/deleteimg', "type" => 'extra']),
                        'showUpload' => false,
                        'minImageWidth'=> 1600,
                        'minImageHeight' => 400,
                        'maxFileCount' => 10,
                        'initialPreview'=> $initialPreview ,
                        'initialPreviewAsData'=>true,
                        'initialPreviewConfig' => $initialPreviewConfig,
                        'overwriteInitial'=>false,
                    ]
                ]);
            ?>
          </div>
        </div>
      </div>
      
      <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                 <?= $form->field($model, 'page_status')->dropdownList(
                    $data = [0 => 'Inactive', 1=>'Active'], 
                    ['custom' => true, 'prompt' => 'Select ..']
                )
                ?> 
              </div>
          </div>
      </div>  
    </div>
    <div class="box-footer">
        <?= $form->field($model, 'created_by_user_id')->input('hidden', ['value' => Yii::$app->user->id ])->label(false) ?>
        <?= Html::button('Draft', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Proceed to Publish', ['class' => 'btn btn-info pull-right']) ?>      
    </div>
</div>

<?php  ActiveForm::end();?>

<?php
$js = <<< 'JS'
$('#topicsuper-transparent_score-source').on('change', function(e) {
  $("#coverImage").css("opacity",$(this).val()/100);
});
JS;
$this->registerJs($js);
?>
