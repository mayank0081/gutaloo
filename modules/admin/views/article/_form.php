<?php


use app\models\TopicSuper;
use app\models\Article;

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use kartik\range\RangeInput;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Topic */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin();
?>

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">General Information</h3>

    <div class="box-tools pull-right">
      <button class="btn btn-box-tool btn-default">Save Unpublished</button>
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="form-group">
          <?= $form->field($model, 'title')->input('text', ['placeholder'=>'Enter Article Title', 'id'=>'articleTitle']) ?>  
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?= $form->field($model, 'slug', [
              'addon'=> [
                  'prepend' => [
                      'content' => 'www.gutaloo.com/article/id/',
                      'options' => [
                          'style' => 'background-color: #e6e4ff'
                      ]
                  ]
              ]
          ])->input('text', ['placeholder'=>'URL', 'id'=> 'articles-slug']) ?>
        </div>
      </div>
    </div>

    <!-- Topic Information Start -->
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <?php 
            $super = TopicSuper::find()->all(); 
            $data = ArrayHelper::toArray($super, [
                'app\models\TopicSuper' => [
                    'super_topic_id',
                    'topic_name',
                ],
            ]);
            $data = ArrayHelper::map($data, 'super_topic_id', 'topic_name');
          ?>  
          <?= 
            $form->field($model, 'main_topic')->widget(Select2::classname(), [
              'data' => $data,
              'options' => ['placeholder' => 'Select Main Topic', 'class'=> 'form-control'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
            ])->label('Main Topic')
          
          ?>
        </div>
        <!-- /.form-group -->
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?= 
          $form->field($model, 'subTopic')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Select Sub Topic', 'multiple' => true],
                'pluginOptions' => [
                    'maximumSelectionLength' => 2
                ],
            ])->label('Sub Topic (Any 2)')
          ?>
        </div>
        <!-- /.form-group -->
      </div>
      <!-- /.col -->
    </div>
    <!-- ./ Topic Information -->

  </div>
</div>

<div class="box box-default  collapsed-box">
  <div class="box-header with-border">
    <h3 class="box-title">Editor</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
    </div>
  </div>
  <!-- ./ Box header  -->
  <div class="box-body">
    <div id="editor">
        <?php
        $text = html::encode('
          <h1>Full Featured</h1>

          <p>This is the full featured Froala WYSIWYG HTML editor.</p>

          <img class="fr-fir fr-dii" src="dist/img/photo1.jpg" alt="Old Clock" width="300"/>Lorem  <strong>ipsum</strong> dolor sit amet, consectetur <strong>adipiscing <em>elit.</em> Donec</strong> facilisis diam in odio iaculis blandit. Nunc eu mauris sit amet purus <strong>viverra</strong><em> gravida</em> ut a dui.<br/>
          <ul><li>Vivamus nec rutrum augue, pharetra faucibus purus. Maecenas non orci sagittis, vehicula lorem et, dignissim nunc.</li> <li>Suspendisse suscipit, diam non varius facilisis, enim libero tincidunt magna, sit amet iaculis eros libero sit amet eros. Vestibulum a rhoncus felis.<ol><li>Nam lacus nulla, consequat ac lacus sit amet, accumsan pellentesque risus. Aenean viverra mi at urna mattis fermentum.</li> <li>Curabitur porta metus in tortor elementum, in semper nulla ullamcorper. Vestibulum mattis tempor tortor quis gravida. In rhoncus risus nibh. Nullam condimentum dapibus massa vel fringilla. Sed hendrerit sed est quis facilisis. Ut sit amet nibh sem. Pellentesque imperdiet mollis libero.</li></ol></li></ul>

          <table style="width: 100%;">
            <tr>
              <td style="width: 25%;"></td>
              <td style="width: 25%;"></td>
              <td style="width: 25%;"></td>
              <td style="width: 25%;"></td>
            </tr>
            <tr>
              <td style="width: 25%;"></td>
              <td style="width: 25%;"></td>
              <td style="width: 25%;"></td>
              <td style="width: 25%;"></td>
            </tr>
          </table>

          <a href="http://google.com" title="Aenean sed hendrerit">Aenean sed hendrerit</a> velit. Nullam eu mi dolor. Maecenas et erat risus. Nulla ac auctor diam, non aliquet ante. Fusce ullamcorper, ipsum id tempor lacinia, sem tellus malesuada libero, quis ornare sem massa in orci. Sed dictum dictum tristique. Proin eros turpis, ultricies eu sapien eget, ornare rutrum ipsum. Pellentesque eros nisl, ornare nec ipsum sed, aliquet sollicitudin erat. Nulla tincidunt porta <strong>vehicula.</strong><br/>');
        $model->content = html::decode($text);
        ?>
       <!--  <div id='edit' style="margin-top: 30px;">
          
        </div> -->

        <?= 
         $form->field($model,'content')->textArea(
          ['id' => 'edit']
        )
        ?>
    </div>
  </div>
  <!-- ./ box body  -->
  <div class="box-footer"></div>
  <!-- ./ box footer  -->
</div>

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Cover Image and Thumbnail</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'description')->textArea([
                'id' => 'description', 
                'placeholder' => 'Enter description at max 175 character.....', 
                'rows' => 3
          ])?>
        </div>
        <div class="form-group">
          <?= 
          $form->field($model, 'tags')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Enter Tags', 'multiple' => true],
                'pluginOptions' => [
                    'maximumSelectionLength' => 5,
                    'tags' => true,
                    'tokenSeparators' => [','],
                ],
            ])->label('Tags (Any 5)')
          ?>
        </div>

        <div class="form-group">
          <?= 
              $form->field($model,'language')->widget(Select2::classname(),[
                  'name' => 'language',
                  'hideSearch' => true,
                  'data' => ["english" => 'English', "hindi" => 'Hindi'],
                  'options' => ['placeholder' => 'Select status...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
            ?>
        </div>
        <div class="form-group">
          <?= 
            $form->field($model, 'article_type')->widget(Select2::classname(), [
              'data' => ["information" => 'Information', "news" => 'News'],
              'options' => ['placeholder' => '', 'class'=> 'form-control'],
              'pluginOptions' => [
                  'allowClear' => true,
                  'tags' => true
              ],
            ])->label('Article Type')
          
          ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label>Thumbnail Image</label> <small>Size (200*200 px)</small>
          <?=  $form->field($model, 'media_thumb')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
                  'pluginOptions' => [
                      'showPreview' => true,
                      'showCaption' => true,
                      'showRemove' => true,
                      'showUpload' => false,
                      'showCancel' => false,
                     // 'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => "img"]),
                     // 'initialPreview'=>$initialPreview,
                     // 'initialPreviewAsData'=>true,
                     // 'initialPreviewConfig' => $initialPreviewConfig,
                      'overwriteInitial'=>true,
                  ]
              ])->label(false); 


          ?> 
        </div>
      </div>
      
    </div>
    <!-- ./row -->

    <hr>

    <div class= "row">
        
        <div class="col-md-6">
          <div class="form-group">

            <label>Cover Image</label>
            <button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#modal-default">Default Images</button>
            <?php    
             /*   if(is_null($model->cover_img_path)){
                    $initialPreview = "";
                    $initialPreviewConfig = "";
                }else{         
                    $initialPreview = Url::base()."/media/topic/t/".$model->topic_id."/".$model->cover_img_path;
                
                    $initialPreviewConfig = array(array("caption"=> $model->cover_img_path, "key" => $model->topic_id ));   
                }
                */       
            ?>
            <?= $form->field($model, 'media_cover')->widget(FileInput::classname(),[
                    'name' => 'coverImg',
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                       // 'uploadUrl' => Url::to(['/site/wp']),
                      //  'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => "cover"]),
                        'minImageWidth'=> 500,
                        'minImageHeight' => 500,
                        'maxFileCount' => 1,
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'showCancel' => false,
                      //  'initialPreview'=>$initialPreview,
                       'initialPreviewAsData'=>true,
                      // 'initialPreviewConfig' => $initialPreviewConfig,
                        'overwriteInitial'=>true,
                        'maxFileCount' => 1,
                    ]
                ])->label(false);
            ?>
          </div>
            
  


          
        <!-- ./Default Images Selection -->

          <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Default Cover Image</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="box box-widget">
                        <div class="box-body">
                          <div class="form-group">
                            <label>
                              <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                              <img src="http://localhost/p/media/topic/t/5/Yve5QO3rcn.png" class="img-responsive" />
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="box box-widget">
                        <div class="box-body">
                          <div class="form-group">
                            <label>
                              <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                              <img src="http://localhost/p/media/topic/t/5/dGgWtGQCkF.jpg" class="img-responsive" />
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="box box-widget">
                        <div class="box-body">
                          <div class="form-group">
                            <label>
                              <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                              <img src="http://localhost/p/media/topic/t/5/eTGWn4NzKT.jpg" class="img-responsive" />
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-primary">Select</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>    
        </div>
        <div class="col-md-6">
          <?= $form->field($model, 'transprancy_score')->widget(
          RangeInput::classname(),[
            'name' => 'range_1',
            'value' => 50,
            'html5Container' => ['style' => 'width:350px'],
            'html5Options' => ['min' => 0, 'max' => 100],
            'addon' => ['append' => ['content' => '%']]
            ]) 
          ?>
          <div style="width: 400px; height: 150px;background-size: 100%;">
                <div  id="coverImage" style="width: 400px; height: 150px; background-color: black; opacity: 0.5"></div>
           </div>

        </div>
      </div>  
  </div>

  <div class="box-footer">
    <button class="btn btn-danger">Remove</button>
    <div class="pull-right">                
      <button class="btn btn-default">Save Unpublished</button>
     <a href="ViewArticle.php"><button class="btn btn-primary">Preview</button></a>
    </div> 
  </div>
</div>
<!-- ./ Cover Image and Thumbnail Information -->


<?php ActiveForm::end(); ?>

<?php
$js = <<< 'JS'
$('#article-transprancy_score-source').on('change', function(e) {
  $("#coverImage").css("opacity",$(this).val()/100);
});

$("#articleTitle").keyup(function(){
    var title = $("#articleTitle").val();
    var str = title.replace(/\s+/g, '-').toLowerCase();
    var today = new Date();
    var after_slug = '_'+today.getFullYear()+''+(today.getMonth()+1)+''+today.getDate() + ''+today.getHours() ;
   // document.getElementById("slug-placeholder").setAttribute("data-placeholder",after_slug);
    var str = str;
    $("#articles-slug").val(str);
});

$(function(){
    $('#edit').froalaEditor()
  });

JS;
$this->registerJs($js);
?>


