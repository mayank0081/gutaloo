<?php


use app\models\TopicSuper;
use app\models\Article;
use app\models\Topic;
use yii\helpers\Json;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use kartik\range\RangeInput;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use richardfan\widget\JSRegister;
use yii\web\JsExpression;

use kartik\depdrop\DepDrop;
use app\models\SsContent;
use app\models\SsTags;

/* @var $this yii\web\View */
/* @var $model app\models\Topic */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin();
?>

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">General Information</h3>

    <div class="box-tools pull-right">
      <button class="btn btn-box-tool btn-default">Save Unpublished</button>
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="form-group">
          <?= $form->field($model, 'title')->input('text', ['placeholder'=>'Enter Article Title', 'id'=>'articleTitle']) ?>  
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?= $form->field($model, 'slug', [
              'addon'=> [
                  'prepend' => [
                      'content' => 'www.gutaloo.com/article/id/',
                      'options' => [
                          'style' => 'background-color: #e6e4ff'
                      ]
                  ]
              ]
          ])->input('text', ['placeholder'=>'URL', 'id'=> 'articles-slug']) ?>
        </div>
      </div>
    </div>

    <!-- Topic Information Start -->
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <?php 
            $super = TopicSuper::find()->where(['page_status' =>1])->all(); 
            $data = ArrayHelper::toArray($super, [
                'app\models\TopicSuper' => [
                    'super_topic_id',
                    'topic_name',
                ],
            ]);
            $data = ArrayHelper::map($data, 'super_topic_id', 'topic_name');
          ?>  
          <?= 
            $form->field($model, 'main_topic')->widget(Select2::classname(), [
              'data' => $data,
              'options' => ['placeholder' => 'Select Main Topic', 'class'=> 'form-control', 'id' => 'topic-id'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
            ])->label('Main Topic')
          
          ?>
        </div>
        <!-- /.form-group -->
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?php
            
            $sub_topic = Topic::find()->select('topic_id as id, topic_name as name')->where(['topic_id'=> array($model->topic_1, $model->topic_2)])->asArray()->all();
            // echo "<pre>";
            // print_r($sub_topic);
            // echo "</pre>";
          //  $model->subTopic = $sub_topic;
          ?>
          <?= 
          $form->field($model, 'subTopic')->widget(DepDrop::classname(), [
                'options'=>['id'=>'subtopic-id', 'multiple' => true],
                'type'=>DepDrop::TYPE_SELECT2,
                'select2Options'=>[
                  'pluginOptions'=>[
                    'allowClear'=>true,
                    'maximumSelectionLength' => 2
                  ]
                ],
                'pluginOptions'=>[
                    'depends'=>['topic-id'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['ss/ab'])
                ]
            ])->label('Sub Topic (Any 2)')
          ?>
        </div>
        <!-- /.form-group -->
      </div>
      <!-- /.col -->
    </div>
    <!-- ./ Topic Information -->

  </div>
</div>  

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Editor</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <!-- ./ Box header  -->
  <div class="box-body">
    <div class="SSContent">
      <div class="hiddenInput">
        <div class="row"> 
          <button type="button" class="btn btn-default imgtext">Image Text</button>
          <button type="button" class="btn btn-default onlytext">Text</button>
          <button type="button" class="btn btn-default onlyimg">Image</button>
        </div>
        <?php
          if(isset($model->ss_id)){
        
            $data_content = SsContent::find()->where(['ss_id'=> $model->ss_id])->all();
            $len = sizeof($data_content);
          }else{
            $len = 0;
          }
          
          for($i = 0; $i< $len; $i++){
            if(isset($data_content[$i]->content) && isset($data_content[$i]->Img_path)){

              echo '<div class="slide slide_10'.$i.'_'.$data_content[$i]->id.'">
              <div class="row">
                <div class="pull-right">
                  <button type="button" class="btn btn-box-tool edit-content"><i class="fa fa-edit"></i></button>
                  <button type="button" class="btn btn-box-tool remove"><i class="fa fa-close"></i></button> 
                </div>
                <div class="alert">
                  
                  <div class="col-md-6">
                    <img id="imgwithtextcontent" class="img-responsive" src="'.Url::base().'/media/ss/'.$data_content[$i]->ss_id.'/'.$data_content[$i]->Img_path.'" style="max-width: 450px; height: auto;">
                  </div>
                  <div class="col-md-6 slide-content">'.$data_content[$i]->content.'</div>
                </div>  
              </div>
              <div class="row">
                <div class="inputfields" style="display: none;">
                  <div class="col-md-6">
                     <div class="form-group">
                      <label>Image</label>
                      <input type="file" id="X_'.$data_content[$i]->id.'" name="X_'.$data_content[$i]->id.'" class="form-control inputimgtext" style="width: 100%">
                      <span style="color: red;"></span>
                    </div>
                    <span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Content</label>
                      <div class="editor">
                        <textarea id="Y_'.$data_content[$i]->id.'" name="Y_'.$data_content[$i]->id.'" class="edit1" style="margin-top: 30px;" placeholder="Type some text">'.$data_content[$i]->content.'</textarea>
                      </div>
                    </div>
                    <input type="button" class="submitImgtext" value="Submit">
                  </div>
                </div> 
              </div>
            </div>';


            }elseif (isset($data_content[$i]->Img_path)) {
              
              echo '<hr><div class="slide slide_10'.$i.'_'.$data_content[$i]->id.'">
              <div class="row">
                <div class="pull-right">
                  <button type="button" class="btn btn-box-tool edit-content"><i class="fa fa-edit"></i></button>
                  <button type="button" class="btn btn-box-tool remove"><i class="fa fa-close"></i></button> 
                </div>
                <div class="alert">
                  
                  <div class="col-md-12">
                    <img id="imgwithtextcontent" class="img-responsive" src="'.Url::base().'/media/ss/'.$data_content[$i]->ss_id.'/'.$data_content[$i]->Img_path.'" style="max-width: 576px; max-height:576px;">
                  </div>
                </div>  
              </div>
              <div class="row">
                <div class="inputfields" style="display: none;">
                  <div class="col-md-6">
                     <div class="form-group">
                      <label>Image</label>
                      <input type="file" id="X_'.$data_content[$i]->id.'" name="X_'.$data_content[$i]->id.'" class="form-control inputonlyimg" style="width: 100%">
                      <span style="color: red;"></span>
                    </div>
                    <span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>
                  </div>
                    <input type="button" class="submitImg" value="Submit">
                </div> 
              </div>
            </div>';

            }elseif (isset($data_content[$i]->content)) {
              echo '<hr><div class="slide slide_10'.$i.'_'.$data_content[$i]->id.'">
              <div class="row">
                <div class="pull-right">
                  <button type="button" class="btn btn-box-tool edit-content"><i class="fa fa-edit"></i></button>
                  <button type="button" class="btn btn-box-tool remove"><i class="fa fa-close"></i></button> 
                </div>
                <div class="alert">
                  <div class="col-md-12 slide-content">'.$data_content[$i]->content.'</div>
                </div>  
              </div>
              <div class="row">
                <div class="inputfields"  style="display: none;" >
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Content</label>
                      <div class="editor">
                        <textarea id="Y_'.$data_content[$i]->id.'" name="Y_'.$data_content[$i]->id.'" class="edit2" style="margin-top: 30px;" placeholder="Type some text">'.$data_content[$i]->content.'</textarea>
                      </div>
                    </div>
                    <input type="button" class="submittext" value="Submit">
                  </div>
                </div> 
              </div>
            </div>';

            }
            
          }

        ?>
      </div> 
    </div>
  </div>
  <!-- ./ box body  -->
  
</div>

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Cover Image and Thumbnail</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'description')->textArea([
                'id' => 'description', 
                'placeholder' => 'Enter description at max 175 character.....', 
                'rows' => 3
          ])?>
        </div>
        <div class="form-group">
          <?php
            if(isset($model->ss_id)){
              $tags = SsTags::find()->select('tag_name')->where(['ss_id'=> $model->ss_id])->asArray()->all();
              $model->tags = array_column($tags, 'tag_name');
            }else{              
              $model->tags = array();
            }
          ?>
          <?= 
          $form->field($model, 'tags')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Enter Tags', 'multiple' => true],
                'pluginOptions' => [
                    'maximumSelectionLength' => 5,
                    'tags' => true,
                    'tokenSeparators' => [','],
                ],
            ])->label('Tags (Any 5)')
          ?>
        </div>

        <div class="form-group">
          <?= 
              $form->field($model,'language')->widget(Select2::classname(),[
                  'name' => 'language',
                  'hideSearch' => true,
                  'data' => ["english" => 'English', "hindi" => 'Hindi'],
                  'options' => ['placeholder' => 'Select status...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
            ?>
        </div>
        <div class="form-group">
          <?= 
            $form->field($model, 'ss_type')->widget(Select2::classname(), [
              'data' => ["information" => 'Information', "news" => 'News'],
              'options' => ['placeholder' => '', 'class'=> 'form-control'],
              'pluginOptions' => [
                  'allowClear' => true,
                  'tags' => true
              ],
            ])->label('SS Type')
            
          ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'article_url')->input('text', ['placeholder'=>'Article URL']) ?>  
        </div>

        <div class="form-group">
          <?= 
              $form->field($model,'published_status')->widget(Select2::classname(),[
                  'name' => 'published_status',
                  'hideSearch' => true,
                  'data' => ["draft" => 'Draft', "unpublish" => 'Unublish', "publish" => 'Publish'],
                  'options' => ['placeholder' => 'Select status...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
            ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <?php    
            if(is_null($model->media_thumb) || $model->media_thumb == ''){
                
                $initialPreview = "";
                $initialPreviewConfig = "";
            }else{

                $initialPreview = Url::base().'/media/ss/'.$model->ss_id.'/'.$model->media_thumb;
            
                $initialPreviewConfig = array(array("caption"=> $model->media_thumb, "key" => $model->ss_id ));   
            }   
             
          ?>
          <label>Thumbnail Image</label> <small>Size (220*150 px)</small>
          <?=  $form->field($model, 'media_thumb')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
                  'pluginOptions' => [
                      'minImageWidth'=> 220,
                      'minImageHeight' => 150,
                      'showPreview' => true,
                      'showCaption' => true,
                      'showRemove' => true,
                      'showUpload' => false,
                      'showCancel' => false,
                     // 'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => "img"]),
                      'initialPreview'=>$initialPreview,
                      'initialPreviewAsData'=>true,
                      'initialPreviewConfig' => $initialPreviewConfig,
                      'overwriteInitial'=>true,
                  ]
              ])->label(false); 


          ?> 
        </div>
      </div>
      
    </div>
    <!-- ./row -->

    <hr>

    <div class= "row">
        
        <div class="col-md-6">
          <div class="form-group">

            <label>Cover Image</label>
            <button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#modal-default">Default Images</button>
            <?php    
                if(is_null($model->media_cover) || $model->media_cover == ''){
                    
                    $initialPreview = "";
                    $initialPreviewConfig = "";
                }else{

                    $initialPreview = Url::base().$model->media_cover;
                
                    $initialPreviewConfig = array(array("caption"=> $model->media_cover, "key" => $model->ss_id ));   
                }
         
            ?>
            <?= $form->field($model, 'media_cover')->widget(FileInput::classname(),[
                    'name' => 'coverImg',
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                       // 'uploadUrl' => Url::to(['/site/wp']),
                      //  'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => "cover"]),
                        'minImageWidth'=> 1600,
                        'minImageHeight' => 400,
                        'maxFileCount' => 1,
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'showCancel' => false,
                        'initialPreview'=>$initialPreview,
                        'initialPreviewAsData'=>true,
                        'initialPreviewConfig' => $initialPreviewConfig,
                        'overwriteInitial'=>true,
                        'maxFileCount' => 1,
                    ]
                ])->label(false);
            ?>
          </div>


        <!-- ./Default Images Selection -->

          <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Default Cover Image</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    
                  </div>
                  <div class="row topiccoverimage">
                   
                  </div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-primary select">Select</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>    
        </div>
        <div class="col-md-6">
          <?= $form->field($model, 'transparency_score')->widget(
          RangeInput::classname(),[
            'name' => 'range_1',
            'value' => 50,
            'html5Container' => ['style' => 'width:350px'],
            'html5Options' => ['min' => 0, 'max' => 100],
            'addon' => ['append' => ['content' => '%']]
            ]) 
          ?>
          <div style="width: 400px; height: 150px;background-size: 100%;">
                <div  id="coverImagePreview" style="width: 400px; height: 150px; background-color: black; opacity: <?= $model->transparency_score/100; ?>"></div>
          </div>

        </div>
      </div>  
  </div>

  <div class="box-footer">
    <?= $form->field($model, 'author_id')->input('hidden', ['value' => Yii::$app->user->id ])->label(false) ?>
    <button class="btn btn-danger">Remove</button>
    <div class="pull-right">                
      <button class="btn btn-default">Save Unpublished</button>
     <button class="btn btn-primary">Preview</button>
    </div> 
  </div>
</div>


<?php ActiveForm::end(); ?>


<?php JSRegister::begin(); ?>
<script>
  var _URL = window.URL || window.webkitURL;
  var img; 
  var slide = 0;
  window.data = "";

  // Limitation to only 5 Slides
  // Code Pending

  //Top 3 button config
  

  $("#subtopic-id").change(function(){
    //alert($(this).val());
    
    if(!($(this).val() == '')){
      $.ajax({
        url: '<?= Url::to(['ss/coverimage']) ?>',
        type: 'post',
        data : {
          topic_id : $(this).val(),
           _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
        },
        success: function (data){
          window.data = data.search;
       //   console.log(window.data);
          covertImage();
        }
      });
    }
  })

  
  
  function covertImage(){
    var len = window.data.length;
    var html = "<div class='col-sm-4'>"+
                      "<div class='form-group'>"+
                        "<input type='radio' name='coverImage' id='coverImage' value='0' checked='checked'>"+
                        "<p>None</p>"+
                      "</div>"+
                    "</div>";
    for(var i=0; i < len; i++  ){
      html += "<div class='col-sm-4'>"+
                      "<div class='form-group'>"+
                        "<input type='radio' name='coverImage' id='coverImage' value='"+window.data[i].id+"'>"+
                        "<img src='<?= Url::base() ?>/media/topic/t/"+window.data[i].topic_id+"/"+ window.data[i].media_path+"' class='img-responsive' />"+
                      "</div>"+
                    "</div>";
    }
    $('.topiccoverimage').html(html);
  }
  covertImage();

  $("#modal-default button[class$='select']").click(function(){
    var val = $("input[name='coverImage']:checked").val();
    if(val){
      var form_node = $("input[name='coverImage']:checked").parents(".form-group");
      var src = $(form_node).children("img").attr("src");
      if(src == undefined){
        if($("#shortstory-media_cover").val() == ''){
          alert("Eitehr select cover image from available option or upload manually");
        }else{
            // Cover Image uploaded manually          
        }
      }else{
        // Cover Image seleced from available option of selected topic
      }
    }
    $("#modal-default").modal('toggle');
  });

  $(".hiddenInput .imgtext").click(function(){
    var html = "<hr><div class='slide slide_"+slide+"'>"+
        "<div class='row'>"+
          "<div class='pull-right'>"+
            "<button type='button' class='btn btn-box-tool edit-content' style='display: none;'><i class='fa fa-edit'></i></button>"+
            "<button type='button' class='btn btn-box-tool remove'><i class='fa fa-close'></i></button>"+ 
          "</div>"+
          "<div class='alert' style='display: none;'>"+
            
            "<div class='col-md-6'>"+
              "<img id='imgwithtextcontent' class='img-responsive' src='' style='max-width: 450px; height: auto;'>"+
            "</div>"+
            "<div class='col-md-6 slide-content'></div>"+
          "</div>  "+
        "</div>"+
        "<div class='row'>"+
          "<div class='inputfields'>"+
            "<div class='col-md-6'>"+
               "<div class='form-group'>"+
                "<label>Image</label>"+
                "<input type='file' id='x_"+slide+"' name='x_"+slide+"' class='form-control inputimgtext' style='width: 100%'>"+
                "<span style='color: red;'></span>"+
              "</div>"+
              "<span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>"+
            "</div>"+
            "<div class='col-md-6'>"+
              "<div class='form-group'>"+
                "<label>Content</label>"+
                "<div class='editor'>"+
                  "<textarea id='y_"+slide+"' name='y_"+slide+"' class='edit1' style='margin-top: 30px;' placeholder='Type some text'></textarea>"+
                "</div>"+
              "</div>"+
              "<input type='button' class='submitImgtext' value='Submit'>"+
            "</div>"+
          "</div> "+
        "</div>"+
      "</div>";


    slide++;
    $(".SSContent").append(html);
    edit();
  });

  $(".hiddenInput .onlytext").click(function(){
    var html = "<hr><div class='slide slide_"+slide+"'>"+
        "<div class='row'>"+
          "<div class='pull-right'>"+
            "<button type='button' class='btn btn-box-tool edit-content' style='display: none;'><i class='fa fa-edit'></i></button>"+
            "<button type='button' class='btn btn-box-tool remove'><i class='fa fa-close'></i></button>"+ 
          "</div>"+
          "<div class='alert' style='display: none;'>"+
            "<div class='col-md-12 slide-content'></div>"+
          "</div>  "+
        "</div>"+
        "<div class='row'>"+
          "<div class='inputfields'>"+
            "<div class='col-md-6'>"+
              "<div class='form-group'>"+
                "<label>Content</label>"+
                "<div class='editor'>"+
                  "<textarea id='y_"+slide+"' name='y_"+slide+"' class='edit2' style='margin-top: 30px;' placeholder='Type some text'></textarea>"+
                "</div>"+
              "</div>"+
              "<input type='button' class='submittext' value='Submit'>"+
            "</div>"+
          "</div> "+
        "</div>"+
      "</div>";


    slide++;
    $(".SSContent").append(html);
    edit();
  });

  $(".hiddenInput .onlyimg").click(function(){
    var html = "<hr><div class='slide slide_"+slide+"'>"+
        "<div class='row'>"+
          "<div class='pull-right'>"+
            "<button type='button' class='btn btn-box-tool edit-content' style='display: none;'><i class='fa fa-edit'></i></button>"+
            "<button type='button' class='btn btn-box-tool remove'><i class='fa fa-close'></i></button>"+ 
          "</div>"+
          "<div class='alert' style='display: none;'>"+
            
            "<div class='col-md-12'>"+
              "<img id='imgwithtextcontent' class='img-responsive' src='' style='max-width: 576px; max-height:576px;'>"+
            "</div>"+
          "</div>  "+
        "</div>"+
        "<div class='row'>"+
          "<div class='inputfields'>"+
            "<div class='col-md-6'>"+
               "<div class='form-group'>"+
                "<label>Image</label>"+
                "<input type='file' id='x_"+slide+"' name='x_"+slide+"' class='form-control inputonlyimg' style='width: 100%'>"+
                "<span style='color: red;'></span>"+
              "</div>"+
              "<span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>"+
            "</div>"+
              "<input type='button' class='submitImg' value='Submit'>"+
          "</div> "+
        "</div>"+
      "</div>";


    slide++;
    $(".SSContent").append(html);
    edit();
  });


  // Submit button of each type of form 
 $(".SSContent").on('click',".submitImgtext", function(){
    var index = $(this).parents(".slide").index();
    var top_node = $(this).parents(".slide");
    var input_field_node = $(this).parents(".inputfields");
    var classname = $(top_node).attr('class');
    var classname = classname.split(" ");
    var child_node_span = $(input_field_node).children("span");

    if($("."+classname[1]+ " .edit1").val() != "" && $("."+classname[1] + " .inputimgtext").val()){
      
      if(  $(child_node_span).text() == ""){
        var text =  $("."+classname[1]+ " .edit1").val();
        
        $("."+classname[1]+" .alert img").attr("src", img.src);
        $("."+classname[1]+" .alert .slide-content").html(text);


        $("."+classname[1] + " .inputfields").hide();
        $("."+classname[1] + " .alert").show();
        $("."+classname[1] + " .edit-content").show();    
        
      }else{
        alert("Upload correct size of image");
      }
    }else{
      alert("Something missing either Content or Image");

        if($("."+classname[1]+ " .edit1").val() != ""){
          var text =  $("."+classname[1]+ " .edit1").val();
          $("."+classname[1]+" .alert .slide-content").html(text);
        }

        $("."+classname[1] + " .inputfields").hide();
        $("."+classname[1] + " .alert").show();
        $("."+classname[1] + " .edit-content").show(); 
    }

    return false;  
  });

 $(".SSContent").on('click',".submittext", function(){
    var index = $(this).parents(".slide").index();
    var top_node = $(this).parents(".slide");
    var input_field_node = $(this).parents(".inputfields");
    var classname = $(top_node).attr('class');
    var classname = classname.split(" ");

    if($("."+classname[1]+ " .edit1").val() != ""){
      var text =  $("."+classname[1]+ " .edit2").val();
      
      $("."+classname[1]+" .alert .slide-content").html(text);

      $("."+classname[1] + " .inputfields").hide();
      $("."+classname[1] + " .alert").show();
      $("."+classname[1] + " .edit-content").show();    
    }else{
      alert("Write some content before submitting");
    }
  });
  
  $(".SSContent").on('click',".submitImg", function(){
    var index = $(this).parents(".slide").index();
    var top_node = $(this).parents(".slide");
    var input_field_node = $(this).parents(".inputfields");
    var classname = $(top_node).attr('class');
    var classname = classname.split(" ");

    if($("."+classname[1] + " .inputonlyimg").val()){
      var child_node_span = $(input_field_node).children("span");
      if(  $(child_node_span).text() == ""){

        $("."+classname[1]+" .alert img").attr("src", img.src);


        $("."+classname[1] + " .inputfields").hide();
        $("."+classname[1] + " .alert").show();
        $("."+classname[1] + " .edit-content").show();    
        
      }else{
        alert("Upload correct size of image");
      }
    }else{
      alert("Upload Image");
      $("."+classname[1] + " .inputfields").hide();
        $("."+classname[1] + " .alert").show();
        $("."+classname[1] + " .edit-content").show(); 
    }

    return false;  
  });


  // other config
  // - - Check chnage after every image is correct or not
  $('.SSContent').on('change','.inputimgtext',function(){
    var form_group_node = $(this).parents(".form-group");
    var child_node = $(form_group_node).children("span");
    $(child_node).text("");

    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            if(this.width < 600){
              $(child_node).text("Image size should be min 600px width")
            }
            if(this.height < 400){
              $(child_node).text("Image size should be min 400px height")
            }
          //  alert(this.width + " " + this.height);
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }

  })

  $('.SSContent').on('change','.inputonlyimg',function(){
    var form_group_node = $(this).parents(".form-group");
    var child_node = $(form_group_node).children("span");
    $(child_node).text("");

    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            if(this.width < 600){
              $(child_node).text("Image size should be min 600px width")
            }
            if(this.height < 600){
              $(child_node).text("Image size should be min 600px height")
            }
          //  alert(this.width + " " + this.height);
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }

  })


  // Edit and close button
  $(".SSContent").on('click', '.remove', function(){
    var slide_node = $(this).parents(".slide");
    var classname = $(slide_node).attr('class');
    var classname = classname.split(" ");
    var slide_num = classname[1].split("_");

    if(slide_num[1] >= 100){
      alert("Hi");
      $.ajax({
          url: '<?= Url::to(['ss/ar']);?>',
          type: 'POST',
          data: { content_id :slide_num[2] },
          success: function(data) {
              alert(data);

          }
       });
      $(slide_node).remove();
    }else{
      $(slide_node).remove();
    }
    
  })

  $(".SSContent").on('click', '.edit-content', function(){
    var index = $(this).parents(".slide").index();
    var top_node = $(this).parents(".slide");
    var classname = $(top_node).attr('class');
    var classname = classname.split(" ");

    $("."+classname[1] + " .inputfields").show();
    $("."+classname[1] + " .alert").hide();
    $("."+classname[1] + " .edit-content").hide();
  })

  function edit(){
    $('.edit1').froalaEditor({
      enter: $.FroalaEditor.ENTER_P, 
      theme : 'royal',
      toolbarButtons: [ 'bold', 'italic', 'underline', 'undo', 'redo', 'codeView'],
       charCounterMax: 400,
       heightMin: 200,
       pastePlain: true
    });
    $('.edit2').froalaEditor({
      enter: $.FroalaEditor.ENTER_P, 
      theme : 'royal',
      toolbarButtons: [ 'bold', 'italic', 'underline', 'undo', 'redo', 'codeView'],
       charCounterMax: 800,
       heightMin: 200,
       pastePlain: true
    })
  }
  edit();  

  $("#articleTitle").keyup(function(){
      var title = $("#articleTitle").val();
      var str = title.replace(/\s+/g, '-').toLowerCase();
      var today = new Date();
      var after_slug = '_'+today.getFullYear()+''+(today.getMonth()+1)+''+today.getDate() + ''+today.getHours() ;
      var str = str;
      $("#articles-slug").val(str);
  });

  $('#shortstory-transparency_score-source').on('change', function(e) {
    $("#coverImagePreview").css("opacity",$(this).val()/100);
  });
  
</script>

<?php JSRegister::end(); ?>
