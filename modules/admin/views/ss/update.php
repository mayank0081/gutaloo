<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Shortstory */

$this->title = 'Update Shortstory: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Shortstories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->ss_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<?= $this->render('_form', [
        'model' => $model,
    ]) ?>

