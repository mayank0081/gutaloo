<?php


use app\models\TopicSuper;
use app\models\Article;

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use kartik\range\RangeInput;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use richardfan\widget\JSRegister;
/* @var $this yii\web\View */
/* @var $model app\models\Topic */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin();
?>

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">General Information</h3>

    <div class="box-tools pull-right">
      <button class="btn btn-box-tool btn-default">Save Unpublished</button>
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="form-group">
          <?= $form->field($model, 'title')->input('text', ['placeholder'=>'Enter Article Title', 'id'=>'articleTitle']) ?>  
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?= $form->field($model, 'slug', [
              'addon'=> [
                  'prepend' => [
                      'content' => 'www.gutaloo.com/article/id/',
                      'options' => [
                          'style' => 'background-color: #e6e4ff'
                      ]
                  ]
              ]
          ])->input('text', ['placeholder'=>'URL', 'id'=> 'articles-slug']) ?>
        </div>
      </div>
    </div>

    <!-- Topic Information Start -->
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <?php 
            $super = TopicSuper::find()->all(); 
            $data = ArrayHelper::toArray($super, [
                'app\models\TopicSuper' => [
                    'super_topic_id',
                    'topic_name',
                ],
            ]);
            $data = ArrayHelper::map($data, 'super_topic_id', 'topic_name');
          ?>  
          <?= 
            $form->field($model, 'main_topic')->widget(Select2::classname(), [
              'data' => $data,
              'options' => ['placeholder' => 'Select Main Topic', 'class'=> 'form-control'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
            ])->label('Main Topic')
          
          ?>
        </div>
        <!-- /.form-group -->
        <div class="form-group">
          <?= $form->field($model, 'media_cover')->widget(FileInput::classname(),[
                    'name' => 'coverImg',
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                       // 'uploadUrl' => Url::to(['/site/wp']),
                       // 'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => "cover"]),
                        'minImageWidth'=> 500,
                        'minImageHeight' => 500,
                        'maxFileCount' => 1,
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'showCancel' => false,
                      //  'initialPreview'=>$initialPreview,
                      // 'initialPreviewAsData'=>true,
                      // 'initialPreviewConfig' => $initialPreviewConfig,
                        'overwriteInitial'=>true,
                        'maxFileCount' => 1,
                    ]
                ]);
            ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?= 
          $form->field($model, 'subTopic')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Select Sub Topic', 'multiple' => true],
                'pluginOptions' => [
                    'maximumSelectionLength' => 2
                ],
            ])->label('Sub Topic (Any 2)')
          ?>
        </div>
        <!-- /.form-group -->
      </div>
      <!-- /.col -->
    </div>
    <!-- ./ Topic Information -->

  </div>
</div>  

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Editor</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <!-- ./ Box header  -->
  <div class="box-body">
    <div class="SSContent">
      <div class="row">
        <div class="alert">
          <div class="pull-right">
            <button type="button" class="btn btn-box-tool"><i class="fa fa-edit"></i></button>
             <!-- <button type="button" class="btn btn-box-tool"><i class="fa fa-close"></i></button> -->
          </div>
          <div class="col-md-6">
            <img id="imgwithtextcontent" class="img-responsive" src="dist/img/hiking-fb-cover.jpg" style="max-width: 450px; height: auto;">
          </div>
          <div class="col-md-6">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
          </div>
        </div>  
        <div class="inputfields" style="display: none;">
          <div class="col-md-6">
             <div class="form-group">
              <label>Image</label>
              <input type="file" id="imgtext" class="form-control" style="width: 100%" name="">
              <span style="color: red;"></span>
            </div>
            <span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Content</label>
              <div class="editor">
                <form>
                  <textarea id='edit1' style="margin-top: 30px;" placeholder="Type some text">
                    
                  </textarea>

                  
                </form>
              </div>
            </div>
          </div>
        </div> 
      </div>


      <!-- <div class="row">
        <div class="alert">
          <div class="pull-right">
             <button type="button" class="btn btn-box-tool"><i class="fa fa-close"></i></button>
          </div>
          <div class="col-md-6">
            <img id="imgwithtextcontent" class="img-responsive" src="dist/img/hiking-fb-cover.jpg" style="max-width: 450px; height: auto;">
          </div>
          <div class="col-md-6">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
          </div>
        </div>
      </div> --> 
      <!-- ./ Img with text -->
      <!-- <div class="row">
        <div class="alert">
          <div class="pull-right">
             <button type="button" class="btn btn-box-tool" onclick="RemoveSlide(this)"><i class="fa fa-close"></i></button>
          </div>
          <div class="col-md-12">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
            </p>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
            </p>
          </div>
        </div>
      </div> -->
      <!-- ./ Text -->
      <!-- <div class="row">
        <div class="alert">
          <div class="pull-right">
             <button type="button" class="btn btn-box-tool"  onclick="RemoveSlide(this)"><i class="fa fa-close"></i></button>
          </div>
          <div class="col-md-12">
            <img class="img-responsive" src="dist/img/hiking-fb-cover.jpg" style="max-width: 576px; max-height: 576px;">
          </div>
        </div>
      </div> -->
      <!-- Img -->
    </div>
    <hr>
    <div class="hiddenInput">
      <div class="row">  
        <button type="button" class="btn btn-default imgtext">Image Text</button>
        <button type="button" class="btn btn-default onlytext">Text</button>
        <button type="button" class="btn btn-default">Image</button>
      </div>      
    </div> 
    <div class="samples">
      <div class="slide slide_0">
        <div class="row">
          <div class="pull-right">
            <button type="button" class="btn btn-box-tool edit-content" style="display: none;"><i class="fa fa-edit"></i></button>
            <button type="button" class="btn btn-box-tool"><i class="fa fa-close remove"></i></button> 
          </div>
          <div class="alert" style="display: none;">
            
            <div class="col-md-6">
              <img id="imgwithtextcontent" class="img-responsive" src="" style="max-width: 450px; height: auto;">
            </div>
            <div class="col-md-6 slide-content">
              
            </div>
          </div>  
        </div>
        <div class="row">
          <div class="inputfields">
            <div class="col-md-6">
               <div class="form-group">
                <label>Image</label>
                <input type="file" id="x_0" name="x_0" class="form-control inputimgtext" style="width: 100%">
                <span style="color: red;"></span>
              </div>
              <span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Content</label>
                <div class="editor">
                  <textarea id="x_0" name="x_0" class="edit1" style="margin-top: 30px;" placeholder="Type some text">
                      
                  </textarea>
                </div>
              </div>
              <input type="button" class="submitImgtext" value="Submit">
            </div>
          </div> 
        </div>
      </div>
      <!-- Img and Text -->
    </div>
    <hr>
    <div id="editor" class="box box-primary nav-tabs-custom" style="display: none;">
      <h4 class="box-title"> Add Next Slide</h4>
      <ul class="nav nav-tabs">
        <li class="active" ><a href="#slideimgtxt" data-toggle="tab">Image with Text</a></li>
        <li><a href="#slidetxt" data-toggle="tab">Text</a></li>
        <li><a href="#slideimg" data-toggle="tab">Image</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="slideimgtxt">
          <div class="form-group">
            <label>Image</label>
            <input type="file" id="imgtext" class="form-control" style="width: 100%" name="">
            <span style="color: red;"></span>
          </div>
          <span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>
          <br>
          <br>
          <div class="form-group">
            <label>Content</label>
            <div class="editor">
              <form>
                <textarea id='edit1' style="margin-top: 30px;" placeholder="Type some text">
                  
                </textarea>

                
              </form>
            </div>
          </div>
           <input type="button" name="" class="sub" value="sublit">
        </div>
        <!-- /.tab-pane = Network -->

        <div class="tab-pane" id="slidetxt">
          <div class="form-group">
            <label>Content</label>
            <div class="editor">
              <form>
                <textarea id='edit2' style="margin-top: 30px;" placeholder="Type some text">
                  
                </textarea>

                
              </form>
            </div>
          </div>
           <input type="button" name="" class="sub" value="sublit">
        </div>

        <div class="tab-pane" id="slideimg">
          <div class="form-group">
            <label>Image</label>
            <input type="file" id="onlyimg" class="form-control" style="width: 100%" name="">
          </div>
          <span><strong>Note : </strong> Image should be having min 576px in width and 546px in height</span>
          <br>
          <br>
            <input type="button" name="" class="sub" value="sublit"> 
        </div>
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- ./ box body  -->
  <div class="box-footer">
    <?= Html::submitButton('Proceed to Publish', ['class' => 'btn btn-info pull-right']) ?>
  </div>
  <!-- ./ box footer  -->
</div>



<?php ActiveForm::end(); ?>


<?php JSRegister::begin(); ?>
<script>
  var _URL = window.URL || window.webkitURL;
  var img; 
  var slide = 0;
  //Top 3 button config
  $(".hiddenInput .imgtext").click(function(){
    alert("Hi");
  });



  // Submit button of each type of form 
  $(".submitImgtext").click(function(){
    var index = $(this).parents(".slide").index();
    var top_node = $(this).parents(".slide");
    var input_field_node = $(this).parents(".inputfields");
    var classname = $(top_node).attr('class');
    var classname = classname.split(" ");

    if($("."+classname[1]+ " .edit1").val() != "" && $("."+classname[1] + " .inputimgtext").val()){
      var child_node_span = $(input_field_node).children("span");
      if(  $(child_node_span).text() == ""){
        var text =  $("."+classname[1]+ " .edit1").val();
        
        $("."+classname[1]+" .alert img").attr("src", img.src);
        $("."+classname[1]+" .alert .slide-content").html(text);


        slide++;

        $("."+classname[1] + " .inputfields").hide();
        $("."+classname[1] + " .alert").show();
        $("."+classname[1] + " .edit-content").show();    
        
      }else{
        alert("Upload correct size of image");
      }
    }else{
      alert("Something missing either Content or Image");
    }

    return false;
    
  });

  


  // other config
  // - - Check chnage after every image is correct or not
  $('.inputimgtext').change(function(){
    var form_group_node = $(this).parents(".form-group");
    var child_node = $(form_group_node).children("span");
    $(child_node).text("");

    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            if(this.width < 576){
              $(child_node).text("Image size should be min 576px width")
            }
            if(this.height < 256){
              $(child_node).text("Image size should be min 256px height")
            }
          //  alert(this.width + " " + this.height);
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }

  })

  // Edit and close button
  $(".remove").click(function(){
    $(this).parents(".slide").remove();
  })

  $(".edit-content").click(function(){
    
    
  })


  $('.edit1').froalaEditor({
      enter: $.FroalaEditor.ENTER_P, 
      theme : 'royal',
      toolbarButtons: [ 'bold', 'italic', 'underline', 'undo', 'redo', 'codeView'],
       charCounterMax: 400,
       heightMin: 200,
       pastePlain: true
    });

  $('.edit2').froalaEditor({
      enter: $.FroalaEditor.ENTER_P, 
      theme : 'royal',
      toolbarButtons: [ 'bold', 'italic', 'underline', 'undo', 'redo', 'codeView'],
       charCounterMax: 800,
       heightMin: 200,
       pastePlain: true
    }) 
 //  Older function
  $(".hiddenInput .onlytext").click(function(){
    html = "<div class='row'>   </div>";

    $(".SSContent>.row:nth-last-child(1)").after(html);
  });

   
  var x = 0;

  // Verification Upload Image and Size(Only Image)
  $('#onlyimg').change(function () {
    $("#slideimg>.form-group>span").text("")
     
     if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            if(this.width < 576){
              $("#slideimg>.form-group>span").text("Image size should be min 576px width")
            }
            if(this.height < 256){
              $("#slideimg>.form-group>span").text("Image size should be min 256px height")
            }
           // alert(this.width + " " + this.height);
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }
  });  
  // Verification Upload Image and Size(Image with Text)
  $('#imgtext').change(function () {
      $("#slideimgtxt>.form-group>span").text("")
       var file;
       if ((file = this.files[0])) {
          img = new Image();
          img.onload = function() {
              if(this.width < 576){
                $("#slideimgtxt>.form-group>span").text("Image size should be min 576px width")
              }
              if(this.height < 256){
                $("#slideimgtxt>.form-group>span").text("Image size should be min 256px height")
              }
              //alert(this.width + " " + this.height);
          };
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = _URL.createObjectURL(file);
      }
  }); 


  $("#slideimg > .sub").click(function(){
    if($("#onlyimg").val()){
      if(  $("#slideimg>.form-group>span").text() == ""){
        var html = "<div class='row'>"+              
                      "<div class='alert'>"+
                        "<div class='pull-right'>"+
                          "<button type='button' class='btn btn-box-tool' onclick='movedown(this)'><i class='fa  fa-arrow-down'></i></button>"+
                          "<button type='button' class='btn btn-box-tool' onclick='RemoveSlide(this)'><i class='fa fa-close'></i></button>"+
                          "</div>"+             
                          "<div class='col-md-12'>"+                 
                            "<img class='img-responsive' src='"+ img.src+ "' style='max-width: 576px; max-height: 576px;'>"+
                          "</div>"+
                        "</div>"+          
                      "</div>";
        $(".SSContent>.row:nth-last-child(1)").after(html);       
         $("#hiddenInput").append('<input type="file" name="s_'+x+'" id="s_'+x+'" value="'+ file+'" hidden="hidden">');
        RemoveDownArrow();
        ShowDownArrow();
      }else{
        alert("Upload correct size of image");
      }
    }else{
      alert("Upload Image");
    }
  });

  $("#slidetxt > .sub").click(function(){
    if($("#edit2").val() != ""){
      var text =  $("#edit2").val();
      var html = "<div class='row'>" + 
                  "<div class='alert' >" + 
                    "<div class='pull-right'>"+
                      "<button type='button' class='btn btn-box-tool' onclick='movedown(this)'><i class='fa  fa-arrow-down'></i></button>"+
                      "<button type='button' class='btn btn-box-tool' onclick='RemoveSlide(this)'><i class='fa fa-close'></i></button>"+
                  "</div><div class='col-md-12'>"+
                     text    + 
                  "</div></div></div>";
      $(".SSContent>.row:nth-last-child(1)").after(html);
      $("#hiddenInput").append('<textarea id="s_'+x+'" name="s_'+x+'" hidden="hidden">'+text+'</textarea>');
      x++;
      RemoveDownArrow();
      ShowDownArrow();
    }else{
      alert("Write some content before submitting");
    }
  });

  $("#slideimgtxt > .sub").click(function(){
    if($("#edit1").val() != "" && $("#imgtext").val()){
      if(  $("#slideimgtxt>.form-group>span").text() == ""){
        var text =  $("#edit1").val();
       // alert(text);

        var html = "<div class='row'>" +
                  "<div class='alert'> " + 
                    "<div class='pull-right'>" + 
                      "<button type='button' class='btn btn-box-tool' onclick='RemoveSlide(this)'><i class='fa fa-close'></i></button>" + 
                    "</div>" + 
                    "<div class='col-md-6'>" + 
                      "<img id='imgwithtextcontent' class='img-responsive' src='"+img.src+"' style='max-width: 450px; height: auto;'>" + 
                    "</div>" + 
                    "<div class='col-md-6'>" + 
                      text+ 
                    "</div>" + 
                  "</div>" + 
                "</div> ";
        $('.SSContent>.row:nth-last-child(1)').after(html);
        RemoveDownArrow();
        ShowDownArrow();
        
        
      }else{
        alert("Upload correct size of image");
      }
    }else{
      alert("Something missing either Content or Image");
    }

    return false;
  });

   


</script>

<?php JSRegister::end(); ?>
