<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Shortstory */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Shortstories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shortstory-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ss_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ss_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ss_id',
            'title',
            'slug',
            'created_date',
            'published_status',
            'main_topic',
            'topic_1',
            'topic_2',
            'content_path',
            'media_thumb',
            'media_cover',
            'transparency_score',
            'description',
            'language',
            'ss_type',
            'published_date',
            'author_id',
            'approved_user_id',
            'channel_id',
        ],
    ]) ?>

</div>
