<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ShortstorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shortstory-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ss_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'slug') ?>

    <?= $form->field($model, 'created_date') ?>

    <?= $form->field($model, 'published_status') ?>

    <?php // echo $form->field($model, 'main_topic') ?>

    <?php // echo $form->field($model, 'topic_1') ?>

    <?php // echo $form->field($model, 'topic_2') ?>

    <?php // echo $form->field($model, 'content_path') ?>

    <?php // echo $form->field($model, 'media_thumb') ?>

    <?php // echo $form->field($model, 'media_cover') ?>

    <?php // echo $form->field($model, 'transparency_score') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'language') ?>

    <?php // echo $form->field($model, 'ss_type') ?>

    <?php // echo $form->field($model, 'published_date') ?>

    <?php // echo $form->field($model, 'author_id') ?>

    <?php // echo $form->field($model, 'approved_user_id') ?>

    <?php // echo $form->field($model, 'channel_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
