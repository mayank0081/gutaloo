<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Shortstory */

$this->title = 'Create Shortstory';
$this->params['breadcrumbs'][] = ['label' => 'Shortstories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?= $this->render('_form', [
	'model' => $model,
]) ?>
