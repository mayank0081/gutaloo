<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShortstorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shortstories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shortstory-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Shortstory', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ss_id',
            'title',
            'slug',
            'created_date',
            'published_status',
            //'main_topic',
            //'topic_1',
            //'topic_2',
            //'content_path',
            //'media_thumb',
            //'media_cover',
            //'transparency_score',
            //'description',
            //'language',
            //'ss_type',
            //'published_date',
            //'author_id',
            //'approved_user_id',
            //'channel_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
