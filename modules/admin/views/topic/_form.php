<?php


use app\models\TopicSuper;
use app\models\Topic;

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use kartik\range\RangeInput;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Topic */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin();
?>



<div class="box box-default">
	<div class="box-header with-border">
	  <h3 class="box-title">General Information</h3>

	  <div class="box-tools pull-right">
		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	  </div>
	</div>
	<div class="box-body">
		<div class="row">
			
			<div class="col-md-6">
			  <div class="form-group">
			  	<?php 
						$super = TopicSuper::find()->where(['page_status' => 1])->all();	
						$data = ArrayHelper::toArray($super, [
						    'app\models\TopicSuper' => [
						        'super_topic_id',
						        'topic_name',
						    ],
						]);
						$data = ArrayHelper::map($data, 'super_topic_id', 'topic_name');
					?>	
			  	<?= 
			  		$form->field($model, 'super_topic_id')->widget(Select2::classname(), [
					    'data' => $data,
					    'options' => ['placeholder' => 'Select Main Topic', 'class'=> 'form-control', 'id'=>'main_topic'],
					    'pluginOptions' => [
					        'allowClear' => true
					    ],
						])->label('Main Topic')
					
					?>
			  </div>
				 
			  <div class="form-group">	
					<?= 
	          $form->field($model, 'parent_topic')->widget(DepDrop::classname(), [
                'options'=>['id'=>'parent-id', 'multiple' => true],
                'type'=>DepDrop::TYPE_SELECT2,
                'select2Options'=>[
                	'pluginOptions'=>[
                		'allowClear'=>true,
                		'maximumSelectionLength' => 2
                	]
                ],
                'pluginOptions'=>[
                    'depends'=>['main_topic'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['topic/ab'])
                ]
            ])->label('Parent Topic (Any 2)')
          ?>
			  </div>

	
			  <div class="form-group">
					<?= $form->field($model, 'topic_name')->input('text', ['placeholder'=>'Enter Topic Name',]) ?>  
			  </div>
			</div>

			<div class="col-md-4">
			  <div class="form-group">
					<?php
						if(!is_null($model->logo_path)){            
	            $initialPreview = Url::base()."/media/topic/t/".$model->topic_id."/".$model->logo_path;
	            
	            $initialPreviewConfig = array(array("caption"=> $model->logo_path, "key" => $model->topic_id ));   
	        }else{
	            $initialPreview = "";
	            $initialPreviewConfig = "";
	        }
	                       
	        ?>
	        <?=  $form->field($model, 'logoImg')->widget(FileInput::classname(), [
	                'options' => ['accept' => 'image/*'],
	                'pluginOptions' => [
	                    'showPreview' => true,
	                    'showCaption' => true,
	                    'showRemove' => true,
	                    'showUpload' => false,
	                    'showCancel' => false,
	                    'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => "img"]),
	                    'initialPreview'=>$initialPreview,
	                    'initialPreviewAsData'=>true,
	                    'initialPreviewConfig' => $initialPreviewConfig,
	                    'overwriteInitial'=>true,
	                ]
	            ]); 


	        ?> 
			  </div>
			</div>
	  </div>
	</div>
	<div class="box-footer">
		<?= Html::submitButton('Create Topic', ['class' => 'btn btn-info pull-right', 'value'=>'submit']) ?>
	</div>
	<!-- /.box-footer -->
</div>



<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Cover Image</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<div class= "row">
    		
    		<div class="col-md-6">
    			<div class="form-group">
		        <?php    
		            if(is_null($model->cover_img_path)){
		                $initialPreview = "";
		                $initialPreviewConfig = "";
		            }else{         
		                $initialPreview = Url::base()."/media/topic/t/".$model->topic_id."/".$model->cover_img_path;
		            
		                $initialPreviewConfig = array(array("caption"=> $model->cover_img_path, "key" => $model->topic_id ));   
		            }       
		        ?>
		        <?= $form->field($model, 'coverImg')->widget(FileInput::classname(),[
		                'name' => 'coverImg',
		                'options' => ['accept' => 'image/*'],
		                'pluginOptions' => [
		                   // 'uploadUrl' => Url::to(['/site/wp']),
		                    'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => "cover"]),
		                    'minImageWidth'=> 500,
		                    'minImageHeight' => 500,
		                    'maxFileCount' => 1,
		                    'showPreview' => true,
		                    'showCaption' => true,
		                    'showRemove' => true,
		                    'showUpload' => false,
		                    'showCancel' => false,
		                    'initialPreview'=>$initialPreview,
		                   'initialPreviewAsData'=>true,
		                   'initialPreviewConfig' => $initialPreviewConfig,
		                    'overwriteInitial'=>true,
		                    'maxFileCount' => 1,
		                ]
		            ]);
		        ?>
		      </div>			
    		</div>
    		<div class="col-md-6">
    			<?= $form->field($model, 'transparent_score')->widget(
    			RangeInput::classname(),[
				    'name' => 'range_1',
				    'value' => 50,
				    'html5Container' => ['style' => 'width:350px'],
				    'html5Options' => ['min' => 0, 'max' => 100],
				    'addon' => ['append' => ['content' => '%']]
						]) 
    			?>
    			<div style="width: 400px; height: 150px;background-size: 100%;">
        	    <div  id="coverImage" style="width: 400px; height: 150px; background-color: black; opacity: 0.5"></div>
          </div>

    		</div>
    	</div>		      
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
		        <?php
		            if(isset($media)){
		                $count = count($media);
		                $initialPreview = array();
		                $initialPreviewConfig = array();
		                for ($i = 0; $i< $count; $i++){
		                    
		                    $initialPreview[] = Url::base()."/media/topic/t/".$model->topic_id."/".$media[$i]["media_path"];
		                    
		                    $initialPreviewConfig[] = array("caption"=> $media[$i]["media_path"], "key" =>$media[$i]["id"]);                   
		                }
		            }else{
		                $initialPreview = "";
		                $initialPreviewConfig = "";
		            }
		            
		        ?>

		        <?= $form->field($model, 'extraCoverImg[]')->widget(FileInput::classname(),[
		                'name' => 'attachment_48[]',
		                'options'=>[
		                    'multiple'=>true
		                ],
		                'pluginOptions' => [
		                  //  'uploadUrl' => Url::to(['/admin/topic/deleteimg']),
		                    'deleteUrl' => Url::to(['/admin/topic/deleteimg', "type" => 'extra']),
		                    'showUpload' => false,
		                    'minImageWidth'=> 500,
		                    'minImageHeight' => 500,
		                    'maxFileCount' => 10,
		                    'initialPreview'=> $initialPreview ,
		                    'initialPreviewAsData'=>true,
		                    'initialPreviewConfig' => $initialPreviewConfig,
		                    'overwriteInitial'=>false,
		                ]
		            ]);
		        ?>
		      </div>  
        </div>
      </div>  
      
    </div>
    
</div>

<div class="box box-default">
	<div class="box-header with-border">
    <h3 class="box-title">Topic Information</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>

	<div class="box-body">
		<div class="row">
      	<div class="col-md-6">
      		<div class="form-group">
              <?= $form->field($model, 'tag_line')->textArea([ 
                  'placeholder' => 'Enter tag line at max 100 character .....', 
                  'rows' => 2
              ])?>
          </div>
          <div class="form-group">
	          <?= $form->field($model, 'description')->textArea([
	                'id' => 'address-input', 
	                'placeholder' => 'Enter description at max 175 character.....', 
	                'rows' => 4
	          ])?>
	        </div>
      	</div>
      	<div class="col-md-6">
      		<div class="form-group">
            <?= $form->field($model, 'slug', [
                'addon'=> [
                    'prepend' => [
                        'content' => 'www.gutaloo.com/topic/id/',
                        'options' => [
                            'style' => 'background-color: #e6e4ff'
                        ]
                    ]
                ]
            ])->input('text', ['placeholder'=>'URL',]) ?>
          </div>
          <?php
          	$model->visibility_status = 0;
          	$model->page_status = 0;
          ?>
          <div class="form-group">
          	<?= 
          		$form->field($model,'visibility_status')->widget(Select2::classname(),[
							    'name' => 'status',
							    'hideSearch' => true,
							    'data' => [1 => 'Active', 0 => 'Inactive'],
							    'options' => ['placeholder' => 'Select status...'],
							    'pluginOptions' => [
							        'allowClear' => true
							    ],
							]);
          	?>
          </div>

          <div class="form-group">
          	<?= 
          		$form->field($model,'page_status')->widget(Select2::classname(),[
							    'name' => 'status',
							    'hideSearch' => true,
							    'data' => [1 => 'Active', 0 => 'Inactive'],
							    'options' => ['placeholder' => 'Select status...'],
							    'pluginOptions' => [
							        'allowClear' => true,
							    ],
							]);
          	?>
          </div>
      	</div>
      </div>
	</div>

	<div class="box-footer">
    <?= $form->field($model, 'created_by_user_id')->input('hidden', ['value' => Yii::$app->user->id ])->label(false) ?>
    <?= Html::button('Draft', ['class' => 'btn btn-default']) ?>
    <?= Html::submitButton('Proceed to Publish', ['class' => 'btn btn-info pull-right']) ?>      
  </div>
</div>


<?php ActiveForm::end(); ?>

<?php
$js = <<< 'JS'
$('#topic-transparent_score-source').on('change', function(e) {
	$("#coverImage").css("opacity",$(this).val()/100);
});
JS;
$this->registerJs($js);
?>



