<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EditorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'web/bower_components/font-awesome/css/font-awesome.min.css',
        'web/bower_components/Ionicons/css/ionicons.min.css',
        'web/css/AdminLTE.min.css',
        '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
        '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css',
        'web/css/froala_editor.css',
        'web/css/froala_style.css',
        'web/css/plugins/code_view.css',
        'web/css/plugins/draggable.css',
        'web/css/plugins/colors.css',
        'web/css/plugins/emoticons.css',
        'web/css/plugins/image_manager.css',
        'web/css/plugins/image.css',
        'web/css/plugins/line_breaker.css',
        'web/css/plugins/table.css',
        'web/css/plugins/char_counter.css',
        'web/css/plugins/video.css',
        'web/css/plugins/fullscreen.css',
        'web/css/plugins/file.css',
        'web/css/plugins/quick_insert.css',
        'web/css/plugins/help.css',
        'web/css/third_party/spell_checker.css',
        'web/css/plugins/special_characters.css',
        '//cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css',
        'web/css/blue.css',
        'web/css/skins/skin-blue.min.css',
    ];
    public $js = [
      //  'bower_components/jquery/dist/jquery.js',
       // 'web/bower_components/bootstrap/dist/js/bootstrap.js',
        'web/js/adminlte.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js',
        'web/js/froala_editor.min.js',
        'web/js/plugins/align.min.js',
        'web/js/plugins/char_counter.min.js',
        'web/js/plugins/code_beautifier.min.js',
        'web/js/plugins/code_view.min.js',
        'web/js/plugins/colors.min.js',
        'web/js/plugins/draggable.min.js',
        'web/js/plugins/emoticons.min.js',
        'web/js/plugins/entities.min.js',
        'web/js/plugins/file.min.js',
        'web/js/plugins/font_size.min.js',
        'web/js/plugins/font_family.min.js',
        'web/js/plugins/fullscreen.min.js',
        'web/js/plugins/image.min.js',
        'web/js/plugins/image_manager.min.js',
        'web/js/plugins/line_breaker.min.js',
        'web/js/plugins/inline_style.min.js',
        'web/js/plugins/link.min.js',
        'web/js/plugins/lists.min.js',
        'web/js/plugins/paragraph_format.min.js',
        'web/js/plugins/paragraph_style.min.js',
        'web/js/plugins/quick_insert.min.js',
        'web/js/plugins/quote.min.js',
        'web/js/plugins/table.min.js',
        'web/js/plugins/save.min.js',
        'web/js/plugins/url.min.js',
        'web/js/plugins/video.min.js',
        'web/js/plugins/help.min.js',
        'web/js/plugins/print.min.js',
        'web/js/third_party/spell_checker.min.js',
        'web/js/plugins/special_characters.min.js',
        'web/js/plugins/word_paste.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
