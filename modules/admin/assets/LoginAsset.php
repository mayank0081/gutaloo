<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'web/bower_components/font-awesome/css/font-awesome.min.css',
        'web/bower_components/Ionicons/css/ionicons.min.css',
        'web/css/AdminLTE.min.css',
        'web/css/blue.css',
        '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
    ];
    public $js = [
     //   'bower_components/jquery/dist/jquery.min.js',
     //   'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'web/js/icheck.min.js',
            
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
