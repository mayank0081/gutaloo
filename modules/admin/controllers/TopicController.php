<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Topic;
use app\models\TopicSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use app\models\TopicMedia;
use yii\helpers\Json;
use app\models\TopicNode;
/**
 * TopicController implements the CRUD actions for Topic model.
 */
class TopicController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Topic models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TopicSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Topic model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Topic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Topic();
        $extraImgCount = 2;

        if ($model->load(Yii::$app->request->post())){
            
             if($model->save()){
                $model =  $this->findModel($model->topic_id);

                $dir_path = Yii::getAlias('@webroot') .'/media/topic/t/'. $model->topic_id;

                if(! is_dir($dir_path)){
                    if(!FileHelper::createDirectory($dir_path, 0775))
                      throw new LocationException($dir_path . ' is not write');
                }


                // Logo File
                $error_type = basename($_FILES["Topic"]["error"]['logoImg']);
                if($error_type == 0){
                  $LogoImg = $this->validateImg('logoImg', $dir_path , 50, 50);
                  $LogoImg = json_decode($LogoImg);
                  if(isset($LogoImg->success) ){
                      $model->logo_path =  $LogoImg->success;
                  }else{
                      // Error - Image is not uploaded into server

                  }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    // Images alredy in database then no need to change page status
                    if(is_null($model->logo_path)){
                        if($model->page_status == 1){
                            $model->page_status = 0;
                            // Update session alert
                        }

                        if($model->visibility_status == 1){
                            $model->visibility_status = 0;
                            // Update session alert
                        }

                    }                    
                }


                //Cover image
                $error_type = basename($_FILES["Topic"]["error"]['coverImg']);
                if($error_type == 0){
                    $LogoImg = $this->validateImg('coverImg', $dir_path , 250, 250);
                    $LogoImg = json_decode($LogoImg);
                    if(isset($LogoImg->success)){
                        $model->cover_img_path =  $LogoImg->success;
                    }else{
                        // Update session alert

                    }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    if(is_null($model->cover_img_path)){
                        if($model->page_status == 1){
                            $model->page_status = 0;
                            // Update session alert
                            
                        }
                    }
                }

                // Extra Cover Image
                // Check cout of extra image in db and file 
                $count = TopicMedia::find()->where(['topic_id'=>$model->topic_id])->count('*');
                if($count == NULL){
                    $count = 0;
                }

                $count_up = count($_FILES["Topic"]["name"]['extraCoverImg']);
               
               
                for($i = 0; $i < $count_up; $i++){
                    $error_type = $_FILES["Topic"]["error"]['extraCoverImg'][$i];
                    if($error_type == 0){
                        $LogoImg = $this->validateImArray('extraCoverImg', $i, $dir_path , 250, 250);

                        $LogoImg = json_decode($LogoImg);
                        if(isset($LogoImg->success)){
                            $TopicMedia = new TopicMedia();

                            $TopicMedia->media_path =  $LogoImg->success;
                            $TopicMedia->topic_id = $model->topic_id;
                            if($TopicMedia->save()){
                                $count++;
                            }



                        }
                    }
                }
                if($count < $extraImgCount){
                   
                    if($model->page_status == 1){
                        $model->page_status = 0;
                        // Session Falsh
                    }
                    //Session
                }
                if(is_null($model->transparent_score)){
                  $model->transparent_score = 50;
                }


                // Parent Topic
                $tempcount =  $_POST['Topic']['parent_topic'];
                if($tempcount != null){
                  for($i = 0; $i < count($tempcount); $i++){
                    $TopicNode = new TopicNode() ;    
                    $TopicNode->topic_id = $model->topic_id;
                    $TopicNode->in_relation = $_POST['Topic']['parent_topic'][$i];
                    $TopicNode->save();
                  }  
                }

            
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->topic_id]);
                }


            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Topic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $media = TopicMedia::find()->where(['topic_id'=>$model->topic_id])->asArray()->all();

        if ($model->load(Yii::$app->request->post())) {
            $extraImgCount = 2;
            if($model->save()){
                $model =  $this->findModel($model->topic_id);

                // Creating Folder
                $dir_path = Yii::getAlias('@webroot') .'/media/topic/t/'. $model->topic_id;
                if(! is_dir($dir_path)){
                    if(!FileHelper::createDirectory($dir_path, 0775))
                      throw new LocationException($dir_path . ' is not write');
                }


                // Logo File

                  $error_type = basename($_FILES["Topic"]["error"]['logoImg']);
                if($error_type == 0){
                  $LogoImg = $this->validateImg('logoImg', $dir_path , 50, 50);
                  $LogoImg = json_decode($LogoImg);
                  if(isset($LogoImg->success) ){
                      $model->logo_path =  $LogoImg->success;
                  }else{
                      // Error - Image is not uploaded into server

                  }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    // Images alredy in database then no need to change page status
                    if(is_null($model->logo_path)){
                        if($model->page_status == 1){
                            $model->page_status = 0;
                            // Update session alert
                        }

                        if($model->visibility_status == 1){
                            $model->visibility_status = 0;
                            // Update session alert
                        }

                    }                   
                }

                //Cover image
                $error_type = basename($_FILES["Topic"]["error"]['coverImg']);
                if($error_type == 0){
                    $LogoImg = $this->validateImg('coverImg', $dir_path , 250, 250);
                    $LogoImg = json_decode($LogoImg);
                    if(isset($LogoImg->success)){
                        $model->cover_img_path =  $LogoImg->success;
                    }else{
                        // Update session alert

                    }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    if(is_null($model->cover_img_path)){
                        if($model->page_status == 1){
                            $model->page_status = 0;
                            // Update session alert
                            
                        }
                    }
                }


                // Extra Cover Image
                // Check cout of extra image in db and file 
                $count = TopicMedia::find()->where(['topic_id'=>$model->topic_id])->count('*');

                if($count == NULL){
                    $count = 0;
                }
                $count_up = count($_FILES["Topic"]["name"]['extraCoverImg']);
               
               
                for($i = 0; $i < $count_up; $i++){
                    $error_type = $_FILES["Topic"]["error"]['extraCoverImg'][$i];
                    if($error_type == 0){
                        $LogoImg = $this->validateImArray('extraCoverImg', $i, $dir_path , 250, 250);

                        $LogoImg = json_decode($LogoImg);
                        if(isset($LogoImg->success)){
                            $TopicMedia = new TopicMedia();

                            $TopicMedia->media_path =  $LogoImg->success;
                            $TopicMedia->topic_id = $model->topic_id;
                            if($TopicMedia->save()){
                                $count++;
                            }



                        }
                    }
                }
                if($count < $extraImgCount){
                   
                    if($model->page_status == 1){
                        $model->page_status = 0;
                        // Session Falsh
                    }
                    //Session
                }

                //Parent Topic
                $tempcount =  $_POST['Topic']['parent_topic'];
                if($tempcount != null){
                  TopicNode::deleteAll(['topic_id'=> $model->topic_id]);
                  for($i = 0; $i < count($tempcount); $i++){
                    $TopicNode = new TopicNode() ;    
                    $TopicNode->topic_id = $model->topic_id;
                    $TopicNode->in_relation = $_POST['Topic']['parent_topic'][$i];
                    $TopicNode->save();
                  }  
                }



                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->topic_id]);
                }

            }
 
        }

        return $this->render('update', [
            'model' => $model, 'media'=> $media
        ]);
    }

    /**
     * Deletes an existing Topic model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      // Only safitissfy certain condition
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Topic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Topic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Topic::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    private function validateImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){

      $file_name =  basename($_FILES["Topic"]["name"][$imgFile]);
      $target_file = $dir .'/'.$file_name;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

      $new_file_name = $this->generateRandomString().".".$imageFileType;
      $target_file = $dir .'/'.$new_file_name;

      if (file_exists($target_file)) {
         // echo "Sorry, file already exists.";
          $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
          $target_file =  $dir ."/". $new_file_name;
      }

      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $check = getimagesize($_FILES["Topic"]["tmp_name"][$imgFile]);
       // Width = $check[0];
       // Height = $check[1];
      if($check !== false) {
          if($check[0] >= $width and $check[1] >= $height){
              // Update Value and Upload photo
          }else{
             // Height and width is min 50px width and height
             return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
             $uploadOk = 0;
          }
      }else{
          return json_encode(array("error" => "File is not an image.")) ;
          $uploadOk = 0;
      }

      //Checking format of Image
      if(!in_array($imageFileType, $format, TRUE)){
          return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
         $uploadOk = 0;
      }

      if ($uploadOk == 0) {
          return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["Topic"]["tmp_name"][$imgFile], $target_file)) {
            // Update Model Data
            return  json_encode(array('success'=> $new_file_name));
          } else {
            // $error = "Sorry, there was an error uploading your file.";
            return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
          }
      }


    }

    private function validateImArray($imgFile, $arrayNumber, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){
        $file_name =  basename($_FILES["Topic"]["name"][$imgFile][$arrayNumber]);
         
        $target_file = $dir .'/'.$file_name;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        $new_file_name = $this->generateRandomString().".".$imageFileType;
        $target_file = $dir .'/'.$new_file_name;

        if (file_exists($target_file)) {
           // echo "Sorry, file already exists.";
            $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
            $target_file =  $dir ."/". $new_file_name;
        }

        $uploadOk = 1;
        $check = getimagesize($_FILES["Topic"]["tmp_name"][$imgFile][$arrayNumber]);
         // Width = $check[0];
         // Height = $check[1];
        if($check !== false) {
            if($check[0] >= $width and $check[1] >= $height){
                // Update Value and Upload photo
            }else{
               // Height and width is min 50px width and height
               return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
               $uploadOk = 0;
            }
        }else{
            return json_encode(array("error" => "File is not an image.")) ;
            $uploadOk = 0;
        }

        //Checking format of Image
        if(!in_array($imageFileType, $format, TRUE)){
            return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
           $uploadOk = 0;
        }
        

        if ($uploadOk == 0) {
            return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["Topic"]["tmp_name"][$imgFile][$arrayNumber], $target_file)) {
                // Update Model Data
                return  json_encode(array('success'=> $new_file_name));
            } else {
               // $error = "Sorry, there was an error uploading your file.";
              return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
            }
        }

    }


    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function actionAb(){
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $topic_id = $parents[0];

              //  $out = self::getSubCatList($cat_id); 
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                $super = Topic::find()->select(['topic_id','topic_name'])->where(['super_topic_id'=>$topic_id, 'page_status' => 1])->all(); 
                $out = ArrayHelper::toArray($super, [
                    'app\models\Topic' => [
                        'id'=>'topic_id',
                        'name'=>'topic_name',
                    ],
                ]);
    
                /*$out = [
                    ['id'=>$parents[0], 'name'=>'abc'],
                    ['id'=>2, 'name'=>'xyz']
                 ];*/
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionDeleteimg(){
        $key = $_POST['key'];
        switch ($_GET["type"]) {
            case 'extra':
                # Extra Cover Image of Topic

                #Step - 1 : Don't remove from directory becuase some else article or ss using that image. so remove it from database only 
                if(TopicMedia::find()->where('id = '.$key)->one()->delete()){
                     return  json_encode(array('success'=> 'Image successfully removed'));
                }else{
                     return  json_encode(array('error'=> 'Not able to remove image'));
                }
                break;
            case 'cover':
                # Current cover Image of topic

                #step - 1 : Remove from directory and Database
                $SuperTopic =  Topic::findOne($key);                
                
                $path = Yii::getAlias('@webroot') .'/media/topic/t/'. $key.'/'.$SuperTopic["cover_img_path"];
            
                if(file_exists($path)){
                    if(!unlink($path)){
                        return  json_encode(array('error'=> 'Not able to remove image from directory'));
                    }else{
                        
                        $SuperTopic->cover_img_path = NULL;
                        if ($SuperTopic->update() !== false) {
                            // update successful
                            return  json_encode(array('success'=> 'Cover Image Removed'));
                        } else {
                            // update failed
                            return  json_encode(array('error'=> 'Records not updated. Refresh Page'));
                        }
                    
                    }
                }else{
                    $SuperTopic->cover_img_path = NULL;
                    if ($SuperTopic->update() !== false) {
                        // update successful
                        return  json_encode(array('success'=> 'Cover Image Removed'));
                    } else {
                        // update failed
                        return  json_encode(array('error'=> 'Records not updated. Refresh Page'));
                    }
                }
                break;
            case 'img':
                # Current Logo of Iamge
                # remove from directory and database
                $SuperTopic =  Topic::findOne($key);                
                
                $path = Yii::getAlias('@webroot') .'/media/topic/t/'. $key.'/'.$SuperTopic["logo_path"];
            
                if(file_exists($path)){
                    if(!unlink($path)){
                        return  json_encode(array('error'=> 'Not able to remove image from directory'));
                    }else{
                        
                        $SuperTopic->logo_path = NULL;
                        if ($SuperTopic->update() !== false) {
                            // update successful
                            return  json_encode(array('success'=> 'Logo Removed Successfully'));
                        } else {
                            // update failed
                            return  json_encode(array('error'=> 'Records not updated. Please Refresh Page'));
                        }
                    
                    }
                }else{
                    $SuperTopic->logo_path = NULL;
                    if ($SuperTopic->update() !== false) {
                        // update successful
                        return  json_encode(array('success'=> 'Cover Image Removed Successfully'));
                    } else {
                        // update failed
                        return  json_encode(array('error'=> 'Records not updated. Please Refresh Page'));
                    }
                }
                break;
        }
    }


}
