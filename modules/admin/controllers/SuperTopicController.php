<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\TopicSuper;
use app\models\TopicSuperSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\helpers\FileHelper;
use app\models\TopicSuperMedia;
use yii\helpers\Url;

use yii\web\UrlManager;


/**
 * SuperTopicController implements the CRUD actions for TopicSuper model.
 */
class SuperTopicController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TopicSuper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TopicSuperSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TopicSuper model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TopicSuper model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TopicSuper();
        $extraImgCount = 2;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                $model =  $this->findModel($model->super_topic_id);

                // Creating Folder
                $dir_path = Yii::getAlias('@webroot') .'/media/topic/s/'. $model->super_topic_id;
                if(! is_dir($dir_path)){
                    if(!FileHelper::createDirectory($dir_path, 0775))
                      throw new LocationException($dir_path . ' is not write');
                }


                // Logo File

                $error_type = basename($_FILES["TopicSuper"]["error"]['logoImg']);
                if($error_type == 0){
                    $LogoImg = $this->validateImg('logoImg', $dir_path , 220, 150);
                    $LogoImg = json_decode($LogoImg);
                    if(isset($LogoImg->success) ){
                        $model->logo_path =  $LogoImg->success;
                    }else{
                        // Error

                    }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    if($model->page_status == 1){
                        $model->page_status = 0;
                        // Update session alert

                    }
                }

                //Cover image
                $error_type = basename($_FILES["TopicSuper"]["error"]['coverImg']);
                if($error_type == 0){
                    $LogoImg = $this->validateImg('coverImg', $dir_path , 1600, 400);
                    $LogoImg = json_decode($LogoImg);
                    if(isset($LogoImg->success)){
                        $model->cover_img_path =  $LogoImg->success;
                    }else{
                        // Update session alert

                    }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    if($model->page_status == 1){
                        $model->page_status = 0;
                        // Update session alert
                        
                    }
                }


                // Extra Cover Image
                // Check cout of extra image in db and file 
                $count = TopicSuperMedia::find()->where(['super_topic_id'=>$model->super_topic_id])->count('*');
                if($count == NULL){
                    $count = 0;
                }

                $count_up = count($_FILES["TopicSuper"]["name"]['extraCoverImg']);
               
               
                for($i = 0; $i < $count_up; $i++){
                    $error_type = $_FILES["TopicSuper"]["error"]['extraCoverImg'][$i];
                    if($error_type == 0){
                        $LogoImg = $this->validateImArray('extraCoverImg', $i, $dir_path , 1600, 400);

                        $LogoImg = json_decode($LogoImg);
                        if(isset($LogoImg->success)){
                            $TopicSuperMedia = new TopicSuperMedia();

                            $TopicSuperMedia->media_path =  $LogoImg->success;
                            $TopicSuperMedia->super_topic_id = $model->super_topic_id;
                            if($TopicSuperMedia->save()){
                                $count++;
                            }



                        }
                    }
                }
                if($count < $extraImgCount){
                   
                    if($model->page_status == 1){
                        $model->page_status = 0;
                        // Session Falsh
                    }
                    //Session
                }

                if(is_null($model->transparent_score)){
                  $model->transparent_score = 50;
                }
                
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->super_topic_id]);
                }

            }
 
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TopicSuper model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $media = TopicSuperMedia::find()->where(['super_topic_id'=>$model->super_topic_id])->asArray()->all();


        if ($model->load(Yii::$app->request->post())) {
            $extraImgCount = 2;
            if($model->save()){
                $model =  $this->findModel($model->super_topic_id);

                // Creating Folder
                $dir_path = Yii::getAlias('@webroot') .'/media/topic/s/'. $model->super_topic_id;
                if(! is_dir($dir_path)){
                    if(!FileHelper::createDirectory($dir_path, 0775))
                      throw new LocationException($dir_path . ' is not write');
                }


                // Logo File

                  $error_type = basename($_FILES["TopicSuper"]["error"]['logoImg']);
                if($error_type == 0){
                  $LogoImg = $this->validateImg('logoImg', $dir_path , 220, 150);
                  $LogoImg = json_decode($LogoImg);
                  if(isset($LogoImg->success) ){
                      $model->logo_path =  $LogoImg->success;
                  }else{
                      // Error - Image is not uploaded into server

                  }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    // Images alredy in database then no need to change page status
                    if(is_null($model->logo_path)){
                        if($model->page_status == 1){
                            $model->page_status = 0;
                            // Update session alert
                        }
                    }                    
                }

                //Cover image
                $error_type = basename($_FILES["TopicSuper"]["error"]['coverImg']);
                if($error_type == 0){
                    $LogoImg = $this->validateImg('coverImg', $dir_path , 1600, 400);
                    $LogoImg = json_decode($LogoImg);
                    if(isset($LogoImg->success)){
                        $model->cover_img_path =  $LogoImg->success;
                    }else{
                        // Update session alert

                    }
                }else{
                    // Page Status - Active then It matters or Updating old logo
                    if(is_null($model->cover_img_path)){
                        if($model->page_status == 1){
                            $model->page_status = 0;
                            // Update session alert
                            
                        }
                    }
                }


                // Extra Cover Image
                // Check cout of extra image in db and file 
                $count = TopicSuperMedia::find()->where(['super_topic_id'=>$model->super_topic_id])->count('*');
                if($count == NULL){
                    $count = 0;
                }

                $count_up = count($_FILES["TopicSuper"]["name"]['extraCoverImg']);
               
               
                for($i = 0; $i < $count_up; $i++){
                    $error_type = $_FILES["TopicSuper"]["error"]['extraCoverImg'][$i];
                    if($error_type == 0){
                        $LogoImg = $this->validateImArray('extraCoverImg', $i, $dir_path , 1600, 400);

                        $LogoImg = json_decode($LogoImg);
                        if(isset($LogoImg->success)){
                            $TopicSuperMedia = new TopicSuperMedia();

                            $TopicSuperMedia->media_path =  $LogoImg->success;
                            $TopicSuperMedia->super_topic_id = $model->super_topic_id;
                            if($TopicSuperMedia->save()){
                                $count++;
                            }



                        }
                    }
                }
                if($count < $extraImgCount){
                   
                    if($model->page_status == 1){
                        $model->page_status = 0;
                        // Session Falsh
                    }
                    //Session
                }

                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->super_topic_id]);
                }

            }
 
        }

        return $this->render('update', [
            'model' => $model, 'media'=> $media
        ]);
    }

    /**
     * Deletes an existing TopicSuper model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TopicSuper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TopicSuper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = TopicSuper::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function validateImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){

        $file_name =  basename($_FILES["TopicSuper"]["name"][$imgFile]);
        $target_file = $dir .'/'.$file_name;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        $new_file_name = $this->generateRandomString().".".$imageFileType;
        $target_file = $dir .'/'.$new_file_name;

        if (file_exists($target_file)) {
           // echo "Sorry, file already exists.";
            $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
            $target_file =  $dir ."/". $new_file_name;
        }

        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $check = getimagesize($_FILES["TopicSuper"]["tmp_name"][$imgFile]);
         // Width = $check[0];
         // Height = $check[1];
        if($check !== false) {
            if($check[0] >= $width and $check[1] >= $height){
                // Update Value and Upload photo
            }else{
               // Height and width is min 50px width and height
               return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
               $uploadOk = 0;
            }
        }else{
            return json_encode(array("error" => "File is not an image.")) ;
            $uploadOk = 0;
        }

        //Checking format of Image
        if(!in_array($imageFileType, $format, TRUE)){
            return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
           $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["TopicSuper"]["tmp_name"][$imgFile], $target_file)) {
                // Update Model Data
                return  json_encode(array('success'=> $new_file_name));
            } else {
               // $error = "Sorry, there was an error uploading your file.";
              return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
            }
        }


    }

    private function validateImArray($imgFile, $arrayNumber, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){
        $file_name =  basename($_FILES["TopicSuper"]["name"][$imgFile][$arrayNumber]);
         
        $target_file = $dir .'/'.$file_name;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        $new_file_name = $this->generateRandomString().".".$imageFileType;
        $target_file = $dir .'/'.$new_file_name;

        if (file_exists($target_file)) {
           // echo "Sorry, file already exists.";
            $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
            $target_file =  $dir ."/". $new_file_name;
        }

        $uploadOk = 1;
        $check = getimagesize($_FILES["TopicSuper"]["tmp_name"][$imgFile][$arrayNumber]);
         // Width = $check[0];
         // Height = $check[1];
        if($check !== false) {
            if($check[0] >= $width and $check[1] >= $height){
                // Update Value and Upload photo
            }else{
               // Height and width is min 50px width and height
               return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
               $uploadOk = 0;
            }
        }else{
            return json_encode(array("error" => "File is not an image.")) ;
            $uploadOk = 0;
        }

        //Checking format of Image
        if(!in_array($imageFileType, $format, TRUE)){
            return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
           $uploadOk = 0;
        }
        

        if ($uploadOk == 0) {
            return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["TopicSuper"]["tmp_name"][$imgFile][$arrayNumber], $target_file)) {
                // Update Model Data
                return  json_encode(array('success'=> $new_file_name));
            } else {
               // $error = "Sorry, there was an error uploading your file.";
              return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
            }
        }

    }


    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function actionDeleteimg(){
        $key = $_POST['key'];
        switch ($_GET["type"]) {
            case 'extra':
                # Extra Cover Image of Topic

                #Step - 1 : Don't remove from directory becuase some else article or ss using that image. so remove it from database only 
                if(TopicSuperMedia::find()->where('id = '.$key)->one()->delete()){
                     return  json_encode(array('success'=> 'Image successfully removed'));
                }else{
                     return  json_encode(array('error'=> 'Not able to remove image'));
                }
                break;
            case 'cover':
                # Current cover Image of topic

                #step - 1 : Remove from directory and Database
                $SuperTopic =  TopicSuper::findOne($key);                
                
                $path = Yii::getAlias('@webroot') .'/media/topic/s/'. $key.'/'.$SuperTopic["cover_img_path"];
            
                if(file_exists($path)){
                    if(!unlink($path)){
                        return  json_encode(array('error'=> 'Not able to remove image from directory'));
                    }else{
                        
                        $SuperTopic->cover_img_path = NULL;
                        if ($SuperTopic->update() !== false) {
                            // update successful
                            return  json_encode(array('success'=> 'Cover Image Removed'));
                        } else {
                            // update failed
                            return  json_encode(array('error'=> 'Records not updated. Refresh Page'));
                        }
                    
                    }
                }else{
                    $SuperTopic->cover_img_path = NULL;
                    if ($SuperTopic->update() !== false) {
                        // update successful
                        return  json_encode(array('success'=> 'Cover Image Removed'));
                    } else {
                        // update failed
                        return  json_encode(array('error'=> 'Records not updated. Refresh Page'));
                    }
                }
                break;
            case 'img':
                # Current Logo of Iamge
                # remove from directory and database
                $SuperTopic =  TopicSuper::findOne($key);                
                
                $path = Yii::getAlias('@webroot') .'/media/topic/s/'. $key.'/'.$SuperTopic["logo_path"];
            
                if(file_exists($path)){
                    if(!unlink($path)){
                        return  json_encode(array('error'=> 'Not able to remove image from directory'));
                    }else{
                        
                        $SuperTopic->logo_path = NULL;
                        if ($SuperTopic->update() !== false) {
                            // update successful
                            return  json_encode(array('success'=> 'Logo Removed Successfully'));
                        } else {
                            // update failed
                            return  json_encode(array('error'=> 'Records not updated. Please Refresh Page'));
                        }
                    
                    }
                }else{
                    $SuperTopic->logo_path = NULL;
                    if ($SuperTopic->update() !== false) {
                        // update successful
                        return  json_encode(array('success'=> 'Cover Image Removed Successfully'));
                    } else {
                        // update failed
                        return  json_encode(array('error'=> 'Records not updated. Please Refresh Page'));
                    }
                }
                break;
        }
    }
}
