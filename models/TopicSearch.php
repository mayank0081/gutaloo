<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Topic;

/**
 * TopicSearch represents the model behind the search form of `app\models\Topic`.
 */
class TopicSearch extends Topic
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['topic_id', 'super_topic_id', 'page_status', 'visibility_status', 'created_by_user_id', 'approved_by_user_id'], 'integer'],
            [['topic_name', 'logo_path', 'cover_img_path', 'tag_line', 'description', 'slug', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Topic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'topic_id' => $this->topic_id,
            'super_topic_id' => $this->super_topic_id,
            'page_status' => $this->page_status,
            'visibility_status' => $this->visibility_status,
            'created_at' => $this->created_at,
            'created_by_user_id' => $this->created_by_user_id,
            'approved_by_user_id' => $this->approved_by_user_id,
        ]);

        $query->andFilterWhere(['like', 'topic_name', $this->topic_name])
            ->andFilterWhere(['like', 'logo_path', $this->logo_path])
            ->andFilterWhere(['like', 'cover_img_path', $this->cover_img_path])
            ->andFilterWhere(['like', 'tag_line', $this->tag_line])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }

    public function search_front($params)
    {
        $query = Topic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!empty($_GET['name'])){
            $name = $_GET['name'];
        }else{
            $name = $this->topic_name;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'topic_id' => $this->topic_id,
            'super_topic_id' => $this->super_topic_id,
            'page_status' => $this->page_status,
            'visibility_status' => $this->visibility_status,
            'created_at' => $this->created_at,
            'created_by_user_id' => $this->created_by_user_id,
            'approved_by_user_id' => $this->approved_by_user_id,
        ]);

        $query->andFilterWhere(['like', 'topic_name', $name]);

        return $dataProvider;
    }
}
