<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "topic_super_media".
 *
 * @property int $id
 * @property int $super_topic_id
 * @property string $media_path
 *
 * @property TopicSuper $superTopic
 */
class TopicSuperMedia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'topic_super_media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['super_topic_id', 'media_path'], 'required'],
            [['super_topic_id'], 'integer'],
            [['media_path'], 'string', 'max' => 255],
            [['super_topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => TopicSuper::className(), 'targetAttribute' => ['super_topic_id' => 'super_topic_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'super_topic_id' => 'Super Topic ID',
            'media_path' => 'Media Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuperTopic()
    {
        return $this->hasOne(TopicSuper::className(), ['super_topic_id' => 'super_topic_id']);
    }
}
