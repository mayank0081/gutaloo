<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "topic_media".
 *
 * @property int $id
 * @property int $topic_id
 * @property string $media_path
 *
 * @property Topic $topic
 */
class TopicMedia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'topic_media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['topic_id', 'media_path'], 'required'],
            [['topic_id'], 'integer'],
            [['media_path'], 'string', 'max' => 255],
            [['topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::className(), 'targetAttribute' => ['topic_id' => 'topic_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'topic_id' => 'Topic ID',
            'media_path' => 'Media Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(Topic::className(), ['topic_id' => 'topic_id']);
    }
}
