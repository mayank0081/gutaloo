<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_content".
 *
 * @property int $id
 * @property int $ss_id
 * @property string $Img_path
 * @property string $content
 *
 * @property Shortstory $ss
 */
class SsContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ss_id'], 'required'],
            [['ss_id'], 'integer'],
            [['Img_path'], 'string', 'max' => 500],
            [['content'], 'string', 'max' => 1000],
            [['ss_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shortstory::className(), 'targetAttribute' => ['ss_id' => 'ss_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ss_id' => 'Ss ID',
            'Img_path' => 'Img Path',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSs()
    {
        return $this->hasOne(Shortstory::className(), ['ss_id' => 'ss_id']);
    }
}
