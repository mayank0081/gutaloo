<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "topic_super".
 *
 * @property int $super_topic_id
 * @property string $topic_name
 * @property string $tag_line
 * @property string $logo_path
 * @property string $cover_img_path
 * @property string $description
 * @property string $slug
 * @property int $page_status
 * @property string $created_at
 * @property int $created_by_user_id
 * @property int $approved_by_user_id
 *
 * @property User $createdByUser
 * @property User $approvedByUser
 * @property TopicSuperMedia[] $topicSuperMedia
 */
class TopicSuper extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $logoImg;
    public $coverImg;
    public $extraCoverImg;


    public static function tableName()
    {
        return 'topic_super';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['topic_name', 'tag_line', 'description', 'slug', 'created_by_user_id'], 'required'],
            [['page_status', 'created_by_user_id', 'approved_by_user_id', 'transparent_score'], 'integer'],
            [['created_at'], 'safe'],
            [['topic_name', 'slug'], 'string', 'max' => 50],
            [['tag_line'], 'string', 'max' => 100],
            [['logo_path', 'cover_img_path'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 175],
            [['created_by_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by_user_id' => 'id']],
            [['approved_by_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['approved_by_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'super_topic_id' => 'Super Topic ID',
            'topic_name' => 'Topic Name',
            'tag_line' => 'Tag Line',
            'logo_path' => 'Logo Path',
            'cover_img_path' => 'Cover Img Path',
            'description' => 'Description',
            'slug' => 'Slug',
            'page_status' => 'Page Status',
            'created_at' => 'Created At',
            'created_by_user_id' => 'Created By User ID',
            'approved_by_user_id' => 'Approved By User ID',
            'transparent_score' => 'transparency Score'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'approved_by_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopicSuperMedia()
    {
        return $this->hasMany(TopicSuperMedia::className(), ['super_topic_id' => 'super_topic_id']);
    }

}
