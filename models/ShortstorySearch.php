<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Shortstory;

/**
 * ShortstorySearch represents the model behind the search form of `app\models\Shortstory`.
 */
class ShortstorySearch extends Shortstory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ss_id', 'main_topic', 'topic_1', 'topic_2', 'transparency_score', 'author_id', 'approved_user_id', 'channel_id'], 'integer'],
            [['title', 'slug', 'created_date', 'published_status', 'content_path', 'media_thumb', 'media_cover', 'description', 'language', 'ss_type', 'published_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shortstory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ss_id' => $this->ss_id,
            'created_date' => $this->created_date,
            'main_topic' => $this->main_topic,
            'topic_1' => $this->topic_1,
            'topic_2' => $this->topic_2,
            'transparency_score' => $this->transparency_score,
            'published_date' => $this->published_date,
            'author_id' => $this->author_id,
            'approved_user_id' => $this->approved_user_id,
            'channel_id' => $this->channel_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'published_status', $this->published_status])
            ->andFilterWhere(['like', 'content_path', $this->content_path])
            ->andFilterWhere(['like', 'media_thumb', $this->media_thumb])
            ->andFilterWhere(['like', 'media_cover', $this->media_cover])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'ss_type', $this->ss_type]);

        return $dataProvider;
    }
}
