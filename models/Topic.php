<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "topic".
 *
 * @property int $topic_id
 * @property string $topic_name
 * @property int $super_topic_id
 * @property string $logo_path
 * @property string $cover_img_path
 * @property string $tag_line
 * @property string $description
 * @property string $slug
 * @property int $page_status
 * @property int $visibility_status
 * @property string $created_at
 * @property int $created_by_user_id
 * @property int $approved_by_user_id
 *
 * @property TopicSuper $superTopic
 * @property User $createdByUser
 * @property User $approvedByUser
 * @property TopicMedia[] $topicMedia
 * @property TopicNode[] $topicNodes
 * @property TopicNode[] $topicNodes0
 * @property TopicNode[] $topicNodes1
 */
class Topic extends \yii\db\ActiveRecord
{

    public $parent_topic;

    public $logoImg;
    public $coverImg;
    public $extraCoverImg;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'topic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['topic_name', 'super_topic_id', 'created_by_user_id'], 'required'],
            [['super_topic_id', 'page_status', 'visibility_status', 'created_by_user_id', 'approved_by_user_id', 'transparent_score'], 'integer'],
            [['created_at'], 'safe'],
            [['topic_name', 'slug'], 'string', 'max' => 50],
            [['logo_path', 'cover_img_path'], 'string', 'max' => 255],
            [['tag_line'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 175],
            [['super_topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => TopicSuper::className(), 'targetAttribute' => ['super_topic_id' => 'super_topic_id']],
            [['created_by_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by_user_id' => 'id']],
            [['approved_by_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['approved_by_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'topic_id' => 'Topic ID',
            'topic_name' => 'Topic Name',
            'super_topic_id' => 'Super Topic ID',
            'logo_path' => 'Logo Path',
            'cover_img_path' => 'Cover Img Path',
            'tag_line' => 'Tag Line',
            'description' => 'Description',
            'slug' => 'Slug',
            'page_status' => 'Page Status',
            'visibility_status' => 'Visibility Status',
            'created_at' => 'Created At',
            'created_by_user_id' => 'Created By User ID',
            'approved_by_user_id' => 'Approved By User ID',
            'transparent_score' => 'Transparency Score'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuperTopic()
    {
        return $this->hasOne(TopicSuper::className(), ['super_topic_id' => 'super_topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'approved_by_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopicMedia()
    {
        return $this->hasMany(TopicMedia::className(), ['topic_id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopicNodes()
    {
        return $this->hasMany(TopicNode::className(), ['topic_id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopicNodes0()
    {
        return $this->hasMany(TopicNode::className(), ['in_relation' => 'topic_id']);
    }
}
