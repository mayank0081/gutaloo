<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_tags".
 *
 * @property int $ss_id
 * @property string $tag_name
 *
 * @property Shortstory $ss
 */
class SsTags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ss_id', 'tag_name'], 'required'],
            [['ss_id'], 'integer'],
            [['tag_name'], 'string', 'max' => 50],
            [['ss_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shortstory::className(), 'targetAttribute' => ['ss_id' => 'ss_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ss_id' => 'Ss ID',
            'tag_name' => 'Tag Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSs()
    {
        return $this->hasOne(Shortstory::className(), ['ss_id' => 'ss_id']);
    }
}
