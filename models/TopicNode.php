<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "topic_node".
 *
 * @property int $node_id
 * @property int $in_relation

 *
 * @property Topic $node
 * @property Topic $inRelation

 */
class TopicNode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'topic_node';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['topic_id'], 'required'],
            [['topic_id', 'in_relation'], 'integer'],
            [['topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::className(), 'targetAttribute' => ['topic_id' => 'topic_id']],
            [['in_relation'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::className(), 'targetAttribute' => ['in_relation' => 'topic_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'topic_id' => 'Node ID',
            'in_relation' => 'In Relation',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNode()
    {
        return $this->hasOne(Topic::className(), ['topic_id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInRelation()
    {
        return $this->hasOne(Topic::className(), ['topic_id' => 'in_relation']);
    }

}
