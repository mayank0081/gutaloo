<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
//use app\assets\AppInnerAsset;
//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <style>
    .alert-success {
    color: #fff !important;
    background-color: #640344  !important;
    border-color: #640344;
    }
    #notifications {
        cursor: pointer;
        position: fixed;
        right: 0px;
        z-index: 9999;
        bottom: 0px;
        margin-bottom: 22px;
        margin-right: 15px;
        max-width: 300px;   
    }
    .summary{display:none;}
    .main-section{font-size:15px !important;}
    .modal-dialog{top:10%;}
    .modal-header {
        background-color: #593466;
        color: whitesmoke;
        font-weight: bold;
    }
    .btn.btn-green {
        background-color: forestgreen;
        color: whitesmoke;
    }
    .btn.btn-orange {
        background-color: tomato;
        color: white;
    }
    sup {
        color: red;
    }
    #st-1 {
        z-index: 0;
    }
    .autocomplete-items {
      position: absolute;
      border-bottom: none;
      border-top: none;
      z-index: 99;
      top: 100%;
      left: 0;
      right: 0;
      text-align: justify;
    }
    .autocomplete-items ul {
      padding: 5px;
      cursor: pointer;
      background-color: #fff;
      border: 1px solid #d4d4d4;
    }
    .autocomplete-items ul li{
      padding: 5px;
      list-style:none;
    }
    .autocomplete-items ul li:hover {
        background-color: #e9e9e9;
    }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo Url::to(['/themes/front/css/sidebar.css']); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Url::to(['/themes/front/css/style.css']); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Url::to(['/themes/front/css/bootstrap.css']); ?>" rel="stylesheet">
    <link href="<?php echo Url::to(['/themes/front/css/custom.css']); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Url::to(['/themes/front/css/owl.carousel.min.css']); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Url::to(['/themes/front/css/jquery.mCustomScrollbar.css']); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- JavaScripts -->
    <script src="<?php echo Url::to(['/themes/front/js/jquery.min.js']); ?>"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
        /* ---    header css    ---    */


.main-header #side-menu a, #side-menu span {
    width: 35px;
    height: 35px;
    line-height: 35px;
    border-radius: 50%;
    background-color: transparent;
    display: inline-block;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
}

.main-header #side-menu a:hover {
    border: 2px solid #fff;
}

.main-header .searchbox input[type=search] {
            background: #fff;
}


.main-header .searchbox{
    position:relative;
    min-width:35px;
    width:0%;
   height: 35px;
    float:right;
    overflow:hidden;
  border-radius: 50%;
      
    
    -webkit-transition: width 0.3s;
    -moz-transition: width 0.3s;
    -ms-transition: width 0.3s;
    -o-transition: width 0.3s;
    transition: width 0.3s;
}

.main-header .searchbox-input{
    top:0;
    right:0;
    border:0;
    outline:0;
    background:#dcddd8;
    width:100%;
    height: 35px;
    margin:0;
    padding:0px 55px 0px 20px;
    font-size:20px;
    color: #000;
}
.main-header .searchbox-input::-webkit-input-placeholder {
    color: #dcddd8;
}
.main-header .searchbox-input:-moz-placeholder {
    color: #dcddd8;
}
.main-header .searchbox-input::-moz-placeholder {
    color: #dcddd8;
}
.main-header .searchbox-input:-ms-input-placeholder {
    color: #dcddd8;
}

.main-header .searchbox-icon,
.main-header .searchbox-submit{
    width: 35px;
    height: 35px;
    display: block;
    position: absolute;
    top: -2px;
    font-family: verdana;
    font-size: 22px;
    right: 0;
    padding: 0;
    margin: 0;
    border: 0;
    outline: 0;
    line-height: 35px;
    text-align: center;
    cursor: pointer;
    color: #340644;
    background: #fff;
}



.main-header .searchbox-open{
    width:100%;
     border-radius: 12px;
  
}




.sidenav .navbar-nav li.active a, .sidenav .navbar-nav li.active a:hover, .sidenav .navbar-nav li.active a:focus {
    color: #777;
    font-weight: 700;
    background: transparent;
}

.sidenav .navbar-nav li a {
  color: #777;

}


.sidenav .navbar-nav li a:hover, .sidenav .navbar-nav li a:focus {
    background-color: transparent;
  color: #fff;
}


#mySidenav  footer .menu-btn li a{
    background-color: transparent;
    color: #777;
}

#mySidenav  footer .menu-btn li a:hover {
    background-color: transparent;
    color: #fff;
}

.sidenav .navbar-nav li a {
  background-color: transparent;
}

.sidenav .btn-div a {
    color: #777;
}

.sidenav .btn-div a:hover {
    background-color: transparent;
    color: #fff;

}

#mySidenav  footer h2 {
    color: #777;
}

#mySidenav  footer p {
    margin: 0;
    font-size: 14px;
    color: #777;
}


.sidenav .navbar-nav li a {
    padding: 5px 15px;
  font-size: 14px;
 
}

#mySidenav  footer .search-bar {
    margin: 10px 0px 16px 0px;
}


.sidenav .category-menu {
 height: 250px;

}


.main-header .searchbox-open #side-menu {
display:none;
}



  .dropbtn {
  background-color: transparent;
  color: white;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
      background-color: transparent;
}

.dropdown {
    display: inline-block;
    position: absolute;
    left: 40%;
    /* top: 95px; */
    margin-top: 20px;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
     z-index: 9999;
}

 .dropdown-content a {
  color: black !important;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
      font-size: 12px;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
 


.scale-zoom-box{
overflow: hidden;
 transition: all 0.8s
}

.scale-zoom-box img{
transition: transform 0.8s ease-in-out;
}

.scale-zoom-box:hover img{
 transform: scale(1.1);
}


.scale-zoom-box:hover:before{
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.3);
    content: "";
  transition: all 0.8s;
}
 

 
 .scale-zoom-box{
overflow: hidden;
 transition: all 0.8s
}

.scale-zoom-box img{
transition: transform 0.8s ease-in-out;
}

.scale-zoom-box:hover img{
 transform: scale(1.1);
}


.scale-zoom-box:hover:before{
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.3);
    content: "";
  transition: all 0.8s;
}


 #counter{
 
     font-size: 20px;
    color: #000;
 }

 .owl-carousel .owl-nav.disabled {
    display: unset;
}

.client-testimonials{
  position:relative;
}
 #counter{
    position: absolute;
    top: 15px;
  right:15px;
    background: #000000ba;;
    z-index: 1;
    border-radius: 50#;
    width: 50px;
    height: 50px;
    padding: 6px;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 12px;
  color:#fff;
}
    </style>
</head>
<body id="page-top">
<div id="notifications"></div>  
  <?php $this->beginBody() ?>
    <?= \app\components\HeaderWidget::widget(); ?>
    <?= \app\components\SidenavWidget::widget(); ?>
            <?= $content ?>
<?php 
    if(Yii::$app->session->hasFlash('success')){
         $success_msg = Yii::$app->session->getFlash('success');
    }else{
        $success_msg = '';
    }
?>  



<script src="<?php echo Url::to(['/themes/front/js/bootstrap.min.js']); ?>"></script>
<script src="<?php echo Url::to(['/themes/front/js/owl.carousel.min.js']); ?>"></script>
<script src="<?php echo Url::to(['/themes/front/js/jquery.mCustomScrollbar.js']); ?>"></script>
<script src="<?php echo Url::to(['/themes/front/js/search-box.js']); ?>"></script>
<script src="<?php echo Url::to(['/themes/front/js/nav-bar.js']); ?>"></script>
<script src="<?php echo Url::to(['/themes/front/js/sidebar.js']); ?>"></script>
<!-- JS END -->
<script src="<?php echo Url::to(['/themes/classic/noty.js']);?>"></script>
<script src="<?php echo Url::to(['/themes/classic/Notify.js']);?>"></script>
<script>
var success_msg = "<?php echo $success_msg;?>";
</script>
<?php
$script = <<< JS
if(success_msg != ''){
    var style = 'success';
    Notify(success_msg, style); 
}
JS;
$this->registerJs($script);
?>  
<!-- Bootstrap core JavaScript -->
<!-- JS START -->
 


<!-- owlCarousel js -->
<script>
    $('.tesimonail').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        }
    }
})
</script>


<!-- top scrollbtn js -->
<script>
jQuery(window).bind('scroll', function () {
    if (jQuery(window).scrollTop() > 600) {
        jQuery('.topscroll-btn').addClass('fixed');
    } else {
         jQuery('.topscroll-btn').removeClass('fixed');
     }
 });

$('.topscroll-btn').click(function() { 
    $('body,html').animate({
        scrollTop : 0 
    }, 500);
});

</script>
<script>
        $(".mCustomScrollbar").mCustomScrollbar({
          scrollButtons:{enable:true},
          theme:"3d-thick"
        });
</script>


<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>



<script>
// external js: flickity.pkgd.js



</script>

 <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
