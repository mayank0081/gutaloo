<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
$this->title = Yii::t('app', 'Home');
?>
<style>
.help-block-error{
    color:red;
}
.main-section {
    font-size: 15px;
}
.ias-trigger a {
    background-color: #593466;
    color: #fff;
    padding:10px;
}
</style>
<div id="overlay" onclick="off()"></div>
<!-- MAIN SECTION START -->
<div class="main-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-3">
        <?= \app\components\TopicWidget::widget(); ?>
      </div>
      <div class="col-sm-6">
        <div class="meddle-section">
          <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_home_ss',
                //'itemOptions' => ['class' => 'item'],
                //'pager' => ['class' => \kop\y2sp\ScrollPager::className()]
            ]); ?>
        </div>
      </div>
   
      <div class="col-sm-3">
          <div class="right-section-wrapper">
            <?=  \app\components\TrendingWidget::widget(); ?>
          </div>
      </div>
    </div>
  </div>
</div>







