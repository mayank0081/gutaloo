<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('app', $model->topic_name);
?>
<style>
.technology{
	font-size:15px;
}
</style>
<!-- MAIN SECTION START -->
<?php
	if(isset($model->super_topic_id) && !isset($model->topic_id)){
		$topic_id = $model->super_topic_id;
		$type = 's';
	}elseif(isset($model->topic_id)){
		$topic_id = $model->topic_id;
		$type = 't';
	}
?>

<div class="main-section technology">
  <div class="banner-section"><!-- banner section start-->

    <figure style="background-image:url(<?=Url::to(['/media/topic/'.$type.'/'.$topic_id.'/'.$model->cover_img_path]);?>); background-repeat: no-repeat; background-size: cover;"><img width = "1920" height = "402" style="background-color: black; opacity: <?= $model->transparent_score/100;?>"></figure>
      <div class="container">
      <div class="text-box">
        <p>138,648 Followers </p>
        <h1><?= $model->topic_name;?></h1>
        <p><?= $model->tag_line; ?></p>
        <div class="btn-div">
          <button type="submit">follow</button>
          <button type="submit">Add to home</button>
          <button type="submit">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">
              <path d="M30,16c4.411,0,8-3.589,8-8s-3.589-8-8-8s-8,3.589-8,8S25.589,16,30,16z"/>
              <path d="M30,44c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,44,30,44z"/>
              <path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z"/>
            </svg>
          </button>
        </div>
      </div>  
    </div>
  </div><!-- banner section end-->
  <div class="container">
    <?= \app\components\TopicWidget::widget(); ?>
    <div class="meddle-section">
			<?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_home_ss',
            //'itemOptions' => ['class' => 'item'],
            //'pager' => ['class' => \kop\y2sp\ScrollPager::className()]
        ]); ?>
	 	</div>
	  <div class="right-section">
	    <?php // \app\components\TrandingWidget::widget(); 
	    ?>
		<div class="topscroll-btn">
	        <a href="#"><img src="<?=Url::to(['/themes/front/images/topscroll-btn.png']);?>" alt="img"></a>
	      </div>
	  </div>
  </div>
</div>
<?php // echo \app\components\ReportAbuseWidget::widget(); ?>
