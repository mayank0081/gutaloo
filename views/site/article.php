<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;



$this->title = Yii::t('app', $slug);
?>
<style>
  .Article .related-post .item .posts {
    max-width: 100%;
}
 #side-menu a, #side-menu span {
    width: 35px;
    height: 35px;
    line-height: 35px;
    border-radius: 50%;
    background-color: transparent;
    display: inline-block;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
}

#side-menu a:hover {
    border: 2px solid #fff;
}

.searchbox input[type=search] {
            background: #fff;
}


.searchbox{
    position:relative;
    min-width:35px;
    width:0%;
   height: 35px;
    float:right;
    overflow:hidden;
border-radius: 50%;
      
    
    -webkit-transition: width 0.3s;
    -moz-transition: width 0.3s;
    -ms-transition: width 0.3s;
    -o-transition: width 0.3s;
    transition: width 0.3s;
}

.searchbox-input,.searchbox-input-mobile{
    top:0;
    right:0;
    border:0;
    outline:0;
    background:#dcddd8;
    width:100%;
    height: 35px;
    margin:0;
    padding:0px 55px 0px 20px;
    font-size:20px;
    color: #000;
}
.searchbox-input::-webkit-input-placeholder {
    color: #dcddd8;
}
.searchbox-input:-moz-placeholder {
    color: #dcddd8;
}
.searchbox-input::-moz-placeholder {
    color: #dcddd8;
}
.searchbox-input:-ms-input-placeholder {
    color: #dcddd8;
}

.searchbox-icon,
.searchbox-submit{
    width: 35px;
    height: 35px;
    display: block;
    position: absolute;
    top: -2px;
    font-family: verdana;
    font-size: 22px;
    right: 0;
    padding: 0;
    margin: 0;
    border: 0;
    outline: 0;
    line-height: 35px;
    text-align: center;
    cursor: pointer;
    color: #340644;
    background: #fff;
}



.searchbox-open{
    width:100%;
     border-radius: 12px;
  
}

.mCustomScrollbar .mCS-3d-thick.mCSB_scrollTools .mCSB_draggerContainer .mCSB_dragger_bar {
    width: 8px;

}

#mySidenav{
    background-color: rgba(5, 20, 41, 0.96) !important;

}

.sidenav .navbar-nav li a {

    border-radius: 0px;

}


.sidenav .navbar-nav li.active a, .sidenav .navbar-nav li.active a:hover, .sidenav .navbar-nav li.active a:focus {
    color: #777;
    font-weight: 700;
    background: transparent;
}

.sidenav .navbar-nav li a {
  color: #777;

}


.sidenav .navbar-nav li a:hover, .sidenav .navbar-nav li a:focus {
    background-color: transparent;
  color: #fff;
}


footer .menu-btn li a{
    background-color: transparent;
    color: #777;
}

footer .menu-btn li a:hover {
    background-color: transparent;
    color: #fff;
}

.sidenav .navbar-nav li a {
  background-color: transparent;
}

.sidenav .btn-div a {
    color: #777;
}

.sidenav .btn-div a:hover {
    background-color: transparent;
    color: #fff;

}

footer h2 {
    color: #777;
}

footer p {
    margin: 0;
    font-size: 14px;
    color: #777;
}


.sidenav .navbar-nav li a {
    padding: 5px 15px;
  font-size: 14px;
 
}

footer .search-bar {
    margin: 10px 0px 16px 0px;
}


#mCSB_2_container{
height: 500px;
}

.sidenav .category-menu {
 height: 250px;

}

@media (max-width: 767px) {

.mobile-container{
width:100%;

}

.main-header .text-right .btn-bar {
    /* display: flex !important; */
    display: inherit;
align-items: center;
}
.main-header .text-right .btn-bar .user-img {
    top: -0!important;
}
.user-img figure {
    height: auto;

}

.btn-bar .user-img {
   margin-top: 0;
}

.main-header .margin-top-mobile {
    display: flex;
    align-items: center;
    margin-top: 0; 
}


.flex-desktop{
    display: flex;
    justify-content: flex-end;
    margin-right: 0;

}


.searchbox {
    position: absolute;
    right:15px !important;
}
.searchbox-open {
    width: 90%;
}

.main-header .text-right {
    text-align: right;
    position: inherit;
    right: 20px;
    top: 15px;
}

.main-header .searchbox {
    top: 6px;
}

.searchbox{
position:absolute;
    right: 0;

}
.desktop-only{
display:none;
}


.searchbox {
    min-width: 30px;
    height: 30px;

}

.main-header .searchbox {
    top: 10px;
}


.searchbox-icon, .searchbox-submit {
  top: -5px;
  right: -3px;
}

.logo-center {
    margin-right: 14px;
}

.left-section ,.right-section-wrapper{
display:none !important;
}



}

@media (min-width: 1000px) {
.mobile-only {
display:none;
}


.main-header .flex-desktop {
    display: flex !important;
      align-items: center;
        justify-content: space-between;
}


.main-header .text-right {
    order: 2;
}

}
  
  .dropbtn {
  background-color: transparent;
  color: white;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
      background-color: transparent;
}

.dropdown {
    display: inline-block;
    position: absolute;
    left: 40%;
    /* top: 95px; */
    margin-top: 20px;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

 .dropdown-content a {
  color: black !important;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
      font-size: 12px;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
  
  
  
  
.gray-background{
    background: #f7f7f7;
    padding-bottom: 100px;
    padding-top: 50px;
}

.gray-background .item{
    background: #fff;
  box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.56);
}

.gray-background h3 {
    font-size: 12px;
    line-height: 20px;
    font-weight: 600;
    text-transform: uppercase;
    text-align: center;
    padding-bottom: 50px;
}

.Article .meddle-section  .client-section{
          padding-top: 50px;
      padding-bottom: 50px;
}


.Article .meddle-section .client-section .icon li {
    display: inline-block;
    margin: 5px;
    text-align: center;
    padding: 5px 7px;
}

.Article .meddle-section .client-section .text h2 span {
    color: #000;
    font-size: 14px;
    font-weight: normal;
    font-weight: 700;
    font-family: 'Montserrat', sans-serif;
}


.Article .meddle-section .client-section .icon ul {

 text-align: left;

}

.Article .meddle-section .social-icon {
  margin-top: 60px;
    border-top: unset;
    margin-bottom: 60px;
    padding-bottom: 60px;
}


.modal-body p{
margin-bottom:0;
}

.modal-content {
    border-radius: 12px;
}


.modal-header .close {
    padding-right: 18px;
}

.modal-header {
    border-bottom: unset;
    padding: 10px 0px 15px 15px !important;
}
.modal-header p{
text-align:center;
    margin-bottom: 0;
}

.modal-header  a{
    color: #ed4956;
  font-weight: 700;
    color: #ed4956;
    text-decoration: none;

}

.modal-body a{
    font-size: 14px;
   display: flex;
    align-items: center;
    justify-content: center;
    color: #777;
    font-weight: 700;
    border-right: 0;
    border-top: 1px solid #efefef;
    cursor: pointer;
    line-height: 1.5;
    margin: 0;
    min-height: 48px;
    padding: 4px 8px;
    text-align: center;
  text-decoration: none;
}


.modal {
    position: fixed;
    top: 90px;
    right: 0;
    bottom: 0;
    left: 28%;
    z-index: 1050;
    display: none;
    overflow: hidden;
    -webkit-overflow-scrolling: touch;
    outline: 0;
    width: 40%;
}


.gray-background .hr-line{

    position: relative;
}

.gray-background .hr-line:before{
    content: "";
    position: absolute;
    top: 50%;
    height: 1px;
    width: 99%;
    display: block;
    border-bottom: 1px solid #000;
    right: 100%;
    margin-right: 25px;
    opacity:  .15;

}

.gray-background .hr-line:after{
    content: "";
    position: absolute;
    top: 50%;
    height: 1px;
    width: 99%;
    display: block;
    border-bottom: 1px solid #000;
    left: 100%;
    margin-left: 25px;
    opacity:  .15;

}

.gray-background .tesimonail-related-post{
box-shadow: 0px 0px 8px 1px rgba(0,0,0,0.12);
}

.Article .client-testimonials{
box-shadow: 0px 0px 8px 1px rgba(0,0,0,0.12)
}


.Article .client-testimonials .owl-nav .owl-prev ,.Article .client-testimonials .owl-nav .owl-next{
    top: 34%;
    border-radius: 50%;
}

.Article .client-testimonials .owl-nav .owl-prev {
  left:-15px;
}
.Article .client-testimonials .owl-nav .owl-next {
  right:-15px;
}

.gray-background .item:hover .related-post .item .posts figure img {
    opacity: 0.5;
    transform: scale(1.2, 1.2);
    transition: all 0.3s ease 0s;
}

.scale-zoom-box figure{
overflow: hidden;
}

.scale-zoom-box img{
transition: transform 0.8s ease-in-out;
}

.scale-zoom-box:hover img{
 transform: scale(1.4);
}



.publish-date-text{
    font-size: 11px;
  }
  
.client-des span a{
display: inherit;
  }
  
  
.author-text{
    font-size: 11px;
    padding-left:5px;
  display: inherit;
}

.author-text:hover{
 color:red !important;
}

 .meddle-section  {
  /*border: 8px solid #340644;*/

    box-shadow: 0px -8px 0px white, 0px 0px, 0px 0px 10px 0px #3406448a;

padding: 15px;
    margin-bottom: 70px;
}
 
.related-post .client-des span{
padding-left:10px;
}


.technology .banner-section .text-box {
    padding: 160px 0px 0px 100px;
}


.Article .banner-section h1 {
    font-size: 48px;
    margin: 10px 0;
    line-height: 1.3;
    font-weight: 700;
}

.Article .banner-section span:first-child {
    margin-bottom: unset;
}

.Article .banner-section .auctor-name {
    border-bottom: unset !important;
  font-size: 13px;
    line-height: 1.2;
  color: #fff;
    opacity: .9;
    font-weight: 600;
}

.btn-follow{
  background: #340644;
    color: #fff;
    padding: 7px 22px;
  letter-spacing: 1px;
}

.Article .meddle-section .client-section figure {
    max-width: 140px; 
}


.Article .meddle-section .social-icon .like-icon {
   padding-left: 20px;
}

.Article .meddle-section .social-icon .bookmark-icon {
   padding-right: 20px;
}

.Article .meddle-section {
    margin-top: -73px;
    z-index: 999999;
    background: #fff;
    border-radius: 10px;
    padding-top: 37px;
}



 </style>

<!-- MAIN SECTION START -->
<div class="main-section technology Article">
    <div class="banner-section"><!-- banner section start-->
        <figure><img src="<?php echo Url::to(['/themes/front/images/Article-banner.jpg'])?>" alt="img"></figure>
        <div class="container">
            <div class="text-box">
                <a href="#" id="bluebutton1"><span>Bodybuilding</span></a>
                <h1>Gym Equipment Is Covered in Germs</h1>
                <span>April 3, 2018</span>
                <span> <a href="#" id="btn-author"><span class="auctor-name"> Written by Reuben Westmaas </span></a></span>
            </div>
        </div>
    </div><!-- banner section end-->
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                <div class="left-section">
                    <div class="Article-social">
                        <div class="like-icon">
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <span>230k</span>
                        </div>
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                        </ul>
                        <div class="bookmark-icon">
                            <a href="#"><i class="fa fa-bookmark"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-7">
                <div class="meddle-section">
                    <div class="text">
                        <p><span>Co</span>nsectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo enim ad minim commodo enim ad minim veniam enim consequat.  cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo enim enim ad minim veniam enim enim ad minim veniam enim  ad minim veniam enim consequat.  cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                    </div>
                    <div class="client-testimonials poll">
                        <div class="testimonail-profile question">
                            <div class="client-des">
                                <h4 class="poll-heading">poll</h4>
                                <h3>Example - Q: Where is Taj Mahal ? </h3>
                                <h3><input type="radio" name="answer"> A. India</h3>
                                <h3><input type="radio" name="answer"> B. US</h3>
                                <h3><input type="radio" name="answer"> C. China</h3>
                                <h3><input type="radio" name="answer"> D. Landon</h3>
                                <button type="submit">submit</button>
                            </div>
                        </div>
                        <div class="testimonail-profile answer">
                            <div class="client-des">
                                <h4 class="poll-heading">poll</h4>
                                <h3>Example - Q: Where is Taj Mahal ? </h3>
                                <h3><span>A.</span> India</h3>
                                <h3><span>B.</span> US</h3>
                                <h3><span>C.</span> China</h3>
                                <h3><span>D.</span> Landon</h3>
                                <div class="progress-box">
                                    <span>A</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%"></div>              
                                        </div>  
                                    <span>B</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    </div>
                                    <span>C</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width:55%"></div>
                                    </div> 
                                    <span>D</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"></div>
                                    </div>
                                </div>   
                                <h3>Correct Answer : A India</h3>
                            </div>
                        </div>
                    </div> 
                    <div class="text-image-box">
                        <div class="client-testimonials">
            <div class="client-des">
              <h4>Find out how much right amount.</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. </p>
              <div class="image-box">
                <div class="left-div">
                  <figure><img src="images/img1.jpg" alt="img"></figure>
                </div>
                <div class="right-div">
                  <div class="right-div1"><figure><img src="images/img1.jpg" alt="img"></figure></div>
                  <div class="right-div2"><figure><img src="images/img1.jpg" alt="img"></figure></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            <p contenteditable="true">This is a paragraph. It is editable. Try to change this Html Codes.</p>
      
      </div>
          </div> 
        </div>
    
    <!-- table section start-->
        <div class="table-responsive table-box">
          <table class="table">
            <thead>
              <tr>
                <th>first</th>
                <th>seciond</th>
                <th>third</th>
                <th>forth</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>first</td>
                <td>seciond</td>
                <td>third</td>
                <td>forth</td>
              </tr>
              <tr>
                <td>first</td>
                <td>seciond</td>
                <td>third</td>
                <td>forth</td>
              </tr>
              <tr>
                <td>first</td>
                <td>seciond</td>
                <td>third</td>
                <td>forth</td>
              </tr>
            </tbody>
          </table>
        </div>
    <!-- table section end-->
        <div class="quartz">
          <p><span><i class="fas fa-quote-left"></i></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet,  pharetra efficitur. Lorem ipsum dolor <span><i class="fas fa-quote-right"></i></span></p>
        </div>
        <div class="point">
          <h4>Vestibulum pharetra</h4>
          <ul>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur 
              <ul>
              <li><span>Consectetur adipiscing elit</span> dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. </li>
              <li><span>Consectetur adipiscing elit</span> dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. </li>
              <li><span>Consectetur adipiscing elit</span> dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. </li>
              </ul>
            </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
          </ul>
        </div>
        <div class="code">
          <h3>Html Codes</h3>
          <p>edit code here</p>
          <figure>
      <p contenteditable="true">$(document).ready(function(){
    $("#hide").click(function(){
      $(this).find("i").toggleClass("fa-bell");     
  })
  })</p>
      
      
      
      </figure>
        </div>
        <div class="client-testimonials" id="hide-btn-section">
          <div class="owl-carousel tesimonail">
       <div class="item">
              <div class="testimonail-profile">
                  <figure><img src="images/Article-slider-banner.jpg" alt="img"></figure>
              </div>
            </div>
  
    </div>
    
    
    
<div class="social-icon" id="aricle-social"> 
          <div class="like-icon"><a href="#" class="circle-background"><i class="fa fa-heart"></i></a>
      </div>
      
          <ul>
            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
       <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
          <li><a href="#"<button  data-toggle="modal" data-target="#modal-1"><i class="fas fa-ellipsis-v dropbtn"></i></button></a></li>
    
      
      
</ul>
  <div class="dropdown">
  
  <div id="myDropdown" class="dropdown-content">
    <a href="#" id="myBtn">Add Bookmark</a>
    
  
  </div>
</div>
      
      
      
          
          <div class="bookmark-icon"><a href="#" class="circle-background"><i class="fa fa-bookmark"></i></a></div>
      </div>
    
        </div>
    
    <div class="client-section">
          <div class="row">
            <div class="col-sm-3">
              <figure>
                <img src="images/client-img.png" alt="img">
              </figure>
            </div>
            <div class="col-sm-9">
              <div class="text">
                <h2><span>Shahid Khan</span></h2>
           
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
          </p>
          
            <div class="icon">
                  <ul>
                    <li><a href="#"><button type="button" class="btn btn-follow">follow</button></a></li>
                  
                  </ul>  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="client-section">
    <div class="row">
            <div class="col-sm-3">
              <figure>
                <img src="images/client-img.png" alt="img">
              </figure>
            </div>
            <div class="col-sm-9">
              <div class="text">
                <h2><span>Shahid Khan</span></h2>
           
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
          </p>
          
            <div class="icon">
                  <ul>
                    <li><a href="#"><button type="button" class="btn btn-follow">follow</button></a></li>
                  
                  </ul>  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
    
  </div> <!---end container -section--->
    
    
    
    
    
    
    
<div class="col-sm-3">
    <div class="right-section-wrapper">
      <h2>feed topics</h2>
      <div class="right-section container-flow" id="style-1">
        <div class="feed-topic-relative">
      <div class="text">
      <ul>
        <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="img-1"></a></figure>
              <a class="youtube-btn" href="#"><img src="<?php echo Url::to(['/themes/front/images/youtube-icon.png'])?>" alt="youtube-icon"></a>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li>
            <li>
              <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
              <label>BY Author</label>
              <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
            </li> 
          </ul>
  
  </div>

   </div>
  
    </div>
<button type="submit">All topics</button>

</div>
</div><!----end right column--->
    
</div>
  
</div><!----end container row--->
  


<div class="container-fluid gray-background"> <!----Recommended container start--->
    <div class="row"> 
        <div class="container">
            <h3><span class="hr-line">Related Post</span></h3>
            <div class="related-post">
                <div class="client-testimonials">
                    <div class="owl-carousel tesimonail-related-post">
                        <?php foreach($related_article as $article) { ?>  
                            <div class="item">
                                <div class="posts">
                                    <div class="testimonail-profile">
                                        <figure><img src="<?php echo Url::to(['/themes/front/images/related-post1.jpg'])?>" alt="img"></figure>
                                    </div>
                                    <div class="client-des">
                                        <h5><?=$article->title?></h5>
                                        <p><?=$article->description?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?> 
                    </div>
                </div>
            </div>
        </div>
    </div> <!----end container recomendation-->
</div>

<!-- <!-- start The Modal -->
<!-- Button trigger modal -->
  
  

  
 

 <!-- #modal 1 -->
<div class="modal fade" id="modal-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <p><a href="#modal-2" data-toggle="modal" data-dismiss="modal">go to share.</a></p>
      </div>
      <div class="modal-body">
   <p><a href="#modal-2" data-toggle="modal" data-dismiss="modal">Share </a></p>
      <p><a href="#modal-2" data-toggle="modal" data-dismiss="modal">Share</a></p>
      <p><a href="#modal-2" data-toggle="modal" data-dismiss="modal"> Share</a></p>
       <p><a href="#modal-2" data-toggle="modal" data-dismiss="modal">Share </a></p>
     <p><a href="#modal-2"> Cancel.</a></p>
        
  <a href="#modal-2" data-toggle="modal" data-dismiss="modal" id="hide-btn">Not Intrested</a>
  <a href="#modal-3" data-toggle="modal" data-dismiss="modal" id="#">Report</a>

  </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

 
  
  <!-- #modal 2 -->
<div class="modal fade" id="modal-2">
  <div class="modal-dialog">
    <div class="modal-content">
 <div class="modal-body">
    <a href="#modal-3" data-toggle="modal" data-dismiss="modal"> go to 3</a>
      <div class="modal-body">
        <p><a href="https://www.facebook.com">Share to Facebook.</a></p>
        <p><a href="#modal-1">Share to Messenger</a></p>
        <p><a href="#modal-1"> Share to Twitter.</a></p>
        <p><a href="#modal-1">Share via Email.</a></p>
        <p><a href="#modal-1" data-dismiss="modal"> Cancel.</a></p>
      </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  


<div class="modal fade" id="modal-3">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
     <div class="modal-body">
   <a href="#modal-1" data-toggle="modal" data-dismiss="modal"> go to 1</a>
          <p><a href="#modal-1">Report inappropriate.</a></p>
      <p><a href="#modal-1"> Embed.</a></p>
       <p><a href="#modal-1">Share.</a></p>
    <p><a href="#modal-1" data-dismiss="modal"> Cancel.</a></p>
        </div>
   
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  





<!--End  Modal --> 
  
  
  
  
 </div>









<script>

$("#hide-btn").click(function(){
  $("#hide-btn-section").hide();
});


</script>


      