<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

	if(isset($model->super_topic_id) && !isset($model->topic_id)){
		$topic_id = $model->super_topic_id;
		$type = 's';
	}elseif(isset($model->topic_id)){
		$topic_id = $model->topic_id;
		$type = 't';
	}

?>

  <div class="col-sm-3">
	<div class="topic-box">
	<a href="<?=Url::to(['/site/topic', 'type'=> $type, 'id' => $topic_id, 'name' => $model->topic_name]);?>">
	<?php if(!empty($model->logo_path)){ ?>
	  <figure><img width="240" height="180" src="<?=Url::to(['/media/topic/'.$type.'/'.$topic_id.'/'.$model->logo_path]);?>" alt="<?= $model->topic_name ?>"></figure>
	<?php } else { ?>
		<figure><img width="240" height="180" src="<?=Url::to(['/media/topic/no.jpg']);?>" alt=""></figure>
	<?php } ?>	
	</a>
	  <h4><?=$model->topic_name;?></h4>
	  <?php 
	  /* if(!empty(Yii::$app->user->id)){
		 $follow =  \app\models\UserTopics::find()->where(['user_id' => Yii::$app->user->id, 'topic_id' => $model->id])->one();
		  
			if(empty($follow)){
		*/
		?>
	  <a href="<?=Url::to(['/site/add-follow','id' => $model->super_topic_id]);?>" title="Click Here To Follow">
		<button class="topic-id" rel="<?=$model->super_topic_id; ?>" type="submit">
			<img src="<?=Url::to(['/themes/front/images/follow-icon.png']);?>" alt="follow-icon">
			Follow
		</button>
		</a>
	  <?php
	  /* } else { 
		*/
	  ?>
	  <!-- <a href="<?=Url::to(['/site/remove-follow','id' => $model->super_topic_id]);?>" title="Click Here To Remove Follow">
		<button class="topic-id" rel="<?=$model->super_topic_id; ?>" type="submit">
			<img src="<?=Url::to(['/themes/front/images/follow-icon.png']);?>" alt="follow-icon">
			Following
		</button>
		</a> -->
	  <?php 
		/*	}
	  } else { */

	  ?>
	  <!-- <a href="javascript:void(0);" title="Need To Login First">
		<button class="topic-id" rel="<?=$model->super_topic_id; ?>" type="submit">
			<img src="<?=Url::to(['/themes/front/images/follow-icon.png']);?>" alt="follow-icon">
			Follow
		</button>
		</a> -->
	  <?php // } ?>
	</div>
  </div>
    
