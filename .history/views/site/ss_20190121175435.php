<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

$this->title = Yii::t('app', 'Topics');
?>
<style>
.main-section {
    font-size: 15px;
}
.ias-trigger a {
    background-color: #593466;
    color: #fff;
	padding:10px;
}
</style>
<!-- MAIN SECTION START -->

<div class="main-section technology All-topic">

  <div class="container">
    <div class="row">
    
    <div class="row">
   	  </div>

    <hr>
    <div class="row">
      <div class="col-md-6">
        <h3>Topics</h3>
      </div>
    </div>
    <hr>
    <div class="row">
    
    
    </div>
	
  </div>
</div>
<?php $form = ActiveForm::begin([
					'id' => 'id-form',
					'method'=>'POST',
					'action' => ['/site/topics'],
				]); ?>
	<input type="hidden" name="id">			
<?php ActiveForm::end(); ?> 				
<script>
$(document).ready(function(){
	$(".topic-id").click(function () {
        var id = $(this).attr("rel");
		//alert(id);
    });
}); 
</script>