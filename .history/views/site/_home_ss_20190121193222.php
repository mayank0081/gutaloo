<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\SsContent;
?>
<?php //pr($model->topic->name);die;?>
<!-- first client-testimonials -->
	
	<?php
// 		echo "<pre>";
// 		print_r($model);
// 		echo "</pre>";
//      die;
	?>
	<div class="client-testimonials">
        <div class="owl-carousel tesimonail">
	        <?php
	        	$slide = SsContent::find()->where(['ss_id'=>$model->ss_id])->all();
	        	
	        	$slide_count = count($slide);
	        	
	        	for($i = 0; $i < $slide_count; $i++){
	 
	        ?>	
          <div class="item">
          		<?php
          			if(isset($slide[$i]->Img_path)){
          		?>
              <div class="testimonail-profile">
                  <figure><img src="<?= Url::to(['/media/ss/'.$slide[$i]->ss_id.'/'.$slide[$i]->Img_path]); ?>" alt="img"></figure>
                  <?php
                  	if($i == 0){
                  ?>
                  <div class="banner-text"><p><a href="<?= Url::to(['/ss?slug='.$model->slug]); ?>"><?= $model->title ?></a></p></div>
                  <?php
                  	}
                  ?>
              </div>
              
              <div class="client-des">
                <h4><?php echo date("d-M", strtotime($model->published_date)); ?></h4>
                <?= $slide[$i]->content ?>
                <span><a href="#">View Article</a></span>
              </div>
              <?php
                }else{
              ?>
              <div class="client-des same">
                 <h4><?= $model->title;?></h4>
                 <h5><?php echo date("d-M", strtotime($model->published_date)); ?></h5>
                
                <?= $slide[$i]->content ?>
                <span><a href="#">View Article</a></span>
              </div>
              <?php    
                }
              ?>            
          </div>
	        <?php
	        	}
	        ?>
        </div>
        <div class="social-icon"> 
          <div class="like-icon"><a href="#"><img src="<?php echo Url::to(['/themes/front/images/like.png'])?>" alt="like"></a></div>
          <ul>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/facebook.png'])?>" alt="facebook"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/twitter.png'])?>" alt="twitter"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/instagram.png'])?>" alt="instagram"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/google-plus.png'])?>" alt="google-plus"></a></li>
          <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/three-dots.png'])?>" alt="three-dots" width="3"></a></li>
          </ul>
          <div class="bookmark-icon"><a href="#"><img src="<?php echo Url::to(['/themes/front/images/bookmark.png'])?>" alt="bookmark"></a></div>
        </div>
      </div>

