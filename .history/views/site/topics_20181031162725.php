<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

$this->title = Yii::t('app', 'Topics');
?>
<style>
.main-section {
    font-size: 15px;
}
.ias-trigger a {
    background-color: #593466;
    color: #fff;
	padding:10px;
}
</style>
<!-- MAIN SECTION START -->
<div class="main-section technology All-topic">
  <div class="banner-section"><!-- banner section start-->
    <figure><img src="<?=Url::to(['/themes/front/images/all-topic-banner.jpg']);?>" alt="img"></figure>
    <div class="container">
      <div class="text-box">
        <h1><span><img src="<?=Url::to(['/themes/front/images/all-topic.png']);?>" alt="img"></span>All Topics</h1>
        <p>From the cutting-edge to the everyday, see how scientific discovery shapes your world From the cutting-edge to the everyday, </p>
      </div>
    </div>  
  </div><!-- banner section end-->
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="search-heading">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="search-bar">
		<?php $form = ActiveForm::begin([
					'options' => ['enctype' => 'multipart/form-data'],
					'id' => 'search-form',
					'method'=>'GET',
					'action' => ['/site/topics'],
				]); ?>
          <input type="text" name="name" class="form-control" placeholder="Search...">
          <a class="search-btn" href="#"><img src="<?=Url::to(['/themes/front/images/search.png']);?>" alt="search"></a>
		  <?php ActiveForm::end(); ?> 
        </div>
      </div>
    </div>
    <div class="row">
    <?php
     echo ListView::widget([
			//'id'=>'product_list',
			'dataProvider' => $SuperProvider,
			'itemView' => '_topics_ss',
			'itemOptions' => ['class' => 'item'],
			//'layout' => "{items}\n<a class='hhhh'><div class='btn-div'>{pager}</div></a>",
			//'pager' => ['class' => \kop\y2sp\ScrollPager::className()]
			]); ?>
	  </div>

    <hr>
    <div class="row">
      <div class="col-md-6">
        <h3>Topics</h3>
      </div>
    </div>
    <hr>
    <div class="row">
    <?= ListView::widget([
      //'id'=>'product_list',
      'dataProvider' => $TopicProvider,
      'itemView' => '_topics_ss',
      'itemOptions' => ['class' => 'item'],
      //'layout' => "{items}\n<a class='hhhh'><div class='btn-div'>{pager}</div></a>",
      //'pager' => ['class' => \kop\y2sp\ScrollPager::className()]
      ]); ?>
    </div>
	
  </div>
</div>
<?php $form = ActiveForm::begin([
					'id' => 'id-form',
					'method'=>'POST',
					'action' => ['/site/topics'],
				]); ?>
	<input type="hidden" name="id">			
<?php ActiveForm::end(); ?> 				
<script>
$(document).ready(function(){
	$(".topic-id").click(function () {
        var id = $(this).attr("rel");
		//alert(id);
    });
}); 
</script>