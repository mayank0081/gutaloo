<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;



$this->title = Yii::t('app', $slug);
?>
<style>
.main-section {
    font-size: 15px;
}
.ias-trigger a {
    background-color: #593466;
    color: #fff;
	padding:10px;
}
</style>
<!-- MAIN SECTION START -->
<div class="main-section technology Article">
  <div class="banner-section"><!-- banner section start-->
    <figure><img src="<?php echo Url::to(['/themes/front/images/Article-banner.jpg'])?>" alt="img"></figure>
    <div class="container">
      <div class="text-box">
        <span><?= $model->mainTopic->topic_name ?></span>
        <h1><?= $model->title ?></h1>
        <p><?= $model->description ?></p>
        <span><?= date('F d,Y', strtotime($model->created_date)) ?></span>
        <span> Written by <?= $model->author->username ?> </span>
      </div>
    </div>
  </div><!-- banner section end-->
  <div class="container">
    <div class="left-section">
      <div class="Article-social">
        <div class="like-icon">
          <a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-like-icon.png'])?>" alt="img"></a>
          <span>230k</span>
        </div>
        <ul>
          <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-facebook-icon.png'])?>" alt="img"></a></li>
          <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-twitter-icon.png'])?>" alt="img"></a></li>
          <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-instagram-icon.png'])?>" alt="img"></a></li>
          <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-pinterest-icon.png'])?>" alt="img"></a></li>
        </ul>
        <div class="bookmark-icon">
          <a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-bookmark-icon.png'])?>" alt="img"></a>
        </div>
      </div>
    </div>

    <div class="meddle-section">
      <div class="text">
        <p>
            <?=$content?>
        </p>
      </div>
      <!-- <div class="client-testimonials poll">
        <div class="testimonail-profile question">
          <div class="client-des">
            <h4 class="poll-heading">poll</h4>
            <h3>Example - Q: Where is Taj Mahal ? </h3>
            <h3><input type="radio" name="answer"> A. India</h3>
            <h3><input type="radio" name="answer"> B. US</h3>
            <h3><input type="radio" name="answer"> C. China</h3>
            <h3><input type="radio" name="answer"> D. Landon</h3>
            <button type="submit">submit</button>
          </div>
        </div>
        <div class="testimonail-profile answer">
          <div class="client-des">
            <h4 class="poll-heading">poll</h4>
            <h3>Example - Q: Where is Taj Mahal ? </h3>
            <h3><span>A.</span> India</h3>
            <h3><span>B.</span> US</h3>
            <h3><span>C.</span> China</h3>
            <h3><span>D.</span> Landon</h3>
            <div class="progress-box">
              <span>A</span>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%"></div>              
              </div>  
              <span>B</span>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
              </div>
              <span>C</span>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width:55%"></div>
              </div> 
              <span>D</span>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"></div>
              </div>
            </div>   
            <h3>Correct Answer : A India</h3>
          </div>
        </div>
      </div> 
        <div class="text-image-box">
          <div class="client-testimonials">
            <div class="client-des">
              <h4>Find out how much right amount.</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. </p>
              <div class="image-box">
                <div class="left-div">
                  <figure><img src="<?php echo Url::to(['/themes/front/images/img1.jpg'])?>" alt="img"></figure>
                </div>
                <div class="right-div">
                  <div class="right-div1"><figure><img src="<?php echo Url::to(['/themes/front/images/img1.jpg'])?>" alt="img"></figure></div>
                  <div class="right-div2"><figure><img src="<?php echo Url::to(['/themes/front/images/img1.jpg'])?>" alt="img"></figure></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </div>
          </div> 
        </div>
        <div class="table-responsive table-box">
          <table class="table">
            <thead>
              <tr>
                <th>first</th>
                <th>seciond</th>
                <th>third</th>
                <th>forth</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>first</td>
                <td>seciond</td>
                <td>third</td>
                <td>forth</td>
              </tr>
              <tr>
                <td>first</td>
                <td>seciond</td>
                <td>third</td>
                <td>forth</td>
              </tr>
              <tr>
                <td>first</td>
                <td>seciond</td>
                <td>third</td>
                <td>forth</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="quartz">
          <p><span><img src="<?php echo Url::to(['/themes/front/images/quartz-left.png'])?>" alt="img"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Lorem ipsum dolor sit amet,  pharetra efficitur. Lorem ipsum dolor <span><img src="<?php echo Url::to(['/themes/front/images/quartz-right.png'])?>" alt="img"></span></p>
        </div>
        <div class="point">
          <h4>Vestibulum pharetra</h4>
          <ul>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur 
              <ul>
              <li><span>Consectetur adipiscing elit</span> dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. </li>
              <li><span>Consectetur adipiscing elit</span> dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. </li>
              <li><span>Consectetur adipiscing elit</span> dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. </li>
              </ul>
            </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra. onsectetur </li>
          </ul>
        </div>
        <div class="code">
          <h3>Html Codes</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <figure><img src="<?php echo Url::to(['/themes/front/images/code.jpg'])?>" alt="code"></figure>
        </div>
        <div class="client-testimonials">
          <div class="owl-carousel tesimonail">
            <div class="item">
              <div class="testimonail-profile">
                  <figure><img src="<?php echo Url::to(['/themes/front/images/Article-slider-banner.jpg'])?>" alt="img"></figure>
              </div>
            </div>
            <div class="item">
              <div class="testimonail-profile">
                  <figure><img src="<?php echo Url::to(['/themes/front/images/Article-slider-banner.jpg'])?>" alt="img"></figure>
              </div>
            </div>
            <div class="item">
              <div class="testimonail-profile">
                  <figure><img src="<?php echo Url::to(['/themes/front/images/Article-slider-banner.jpg'])?>" alt="img"></figure>
              </div>
            </div>
          </div>
        </div>
        <div class="Article-social">
          <div class="like-icon">
            <a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-like-icon.png'])?>" alt="img"></a>
            <span>567 like </span>
          </div>
          <ul>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-facebook-icon.png'])?>" alt="img"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-twitter-icon.png'])?>" alt="img"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-instagram-icon.png'])?>" alt="img"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-pinterest-icon.png'])?>" alt="img"></a></li>
          </ul>
          <div class="bookmark-icon">
            <span>567 bookmark</span>
            <a href="#"><img src="<?php echo Url::to(['/themes/front/images/A-bookmark-icon.png'])?>" alt="img"></a>
          </div>
        </div>
        <div class="client-section">
          <div class="row">
            <div class="col-sm-3">
              <figure>
                <img src="<?php echo Url::to(['/themes/front/images/client-img.png'])?>" alt="img">
                <div class="icon">
                  <ul>
                    <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/facebook.png'])?>" alt="facebook"></a></li>
                    <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/twitter.png'])?>" alt="twitter"></a></li>
                    <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/instagram.png'])?>" alt="instagram"></a></li>
                  </ul>  
                </div>
              </figure>
            </div>
            <div class="col-sm-9">
              <div class="text">
                <h2>Shahid Khan <span>(BUSINESS TYCOON)</span></h2>
                <p>Net worth: $8.2B</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Aenean sollicitudin, lectus dolor imperdiet libero, id hendrerit turpis metus eu purus. </p>
              </div>
            </div>
          </div>
        </div>
        <div class="client-section">
          <div class="row">
            <div class="col-sm-3">
              <figure>
                <img src="<?php echo Url::to(['/themes/front/images/client-img.png'])?>" alt="img">
                <div class="icon">
                  <ul>
                    <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/facebook.png'])?>" alt="facebook"></a></li>
                    <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/twitter.png'])?>" alt="twitter"></a></li>
                    <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/instagram.png'])?>" alt="instagram"></a></li>
                  </ul>  
                </div>
              </figure>
            </div>
            <div class="col-sm-9">
              <div class="text">
                <h2>Shahid Khan <span>(BUSINESS TYCOON)</span></h2>
                <p>Net worth: $8.2B</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempor augue a auctor pharetra. Duis iaculis iaculis pellentesque. Aliquam erat volutpat. Quisque laoreet lorem ut enim pharetra efficitur. Aenean sollicitudin, lectus dolor imperdiet libero, id hendrerit turpis metus eu purus. </p>
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
    <div class="right-section">
    <h2>trending</h2>
    <ul class="mCustomScrollbar">
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="img-1"></a></figure>
            <a class="youtube-btn" href="#"><img src="<?php echo Url::to(['/themes/front/images/youtube-icon.png'])?>" alt="youtube-icon"></a>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li>
        <li>
            <p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
            <label>BY Author</label>
            <figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
        </li> 
        </ul>
        <div class="topscroll-btn">
        <a href="#"><img src="<?php echo Url::to(['/themes/front/images/topscroll-btn.png'])?>" alt="img"></a>
        </div>
    </div>
  </div>
  <div class="container">
    <div class="related-post">
      <div class="client-testimonials">
        <div class="owl-carousel tesimonail-related-post">
        <?php foreach($related_article as $article) { ?>  
        <div class="item">
            <div class="posts">
              <div class="testimonail-profile">
                <figure><img src="<?=$article->media_thumb ?>" alt="img"></figure>
              </div>
              <div class="client-des">
                <h5><?=$article->title?></h5>
                <p><?=$article->description?></p>
              </div>
            </div>
          </div>
        <?php } ?>
          </div>
      </div>
    </div>
  </div>
</div>
