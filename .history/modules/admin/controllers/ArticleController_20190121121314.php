<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'editor';
        $model = new Article();

        

        if ($model->load(Yii::$app->request->post()) ) {          
            
            if($model->save()){

              $model =  $this->findModel($model->article_id);
              $flag = 1; // Can Publish Artical => Publish_Status = 'publish'
              
              $dir_path = Yii::getAlias('@webroot') .'/media/artical/'. $model->article_id;

              if(! is_dir($dir_path)){
                  if(!FileHelper::createDirectory($dir_path, 0775))
                    throw new LocationException($dir_path . ' is not write');
              }


              //Content 

              $post_array = array_filter($_POST, function ($key) {
                return strpos($key, 'y_') === 0;
              }, ARRAY_FILTER_USE_KEY);
              
              $post_key = array_keys($post_array);
              $post_len = sizeof($post_key);
              
              if($post_len < 0){
                $post_content_ip = intval(str_replace('y_', '', $post_key[$post_len-1]));
              }else{
                $post_content_ip = 0;
              }

              $file_array = array_filter($_FILES, function ($key) {
                    return strpos($key, 'x_') === 0;
                  }, ARRAY_FILTER_USE_KEY);
                  
              $file_key = array_keys($file_array);
              $file_len = sizeof($file_key);
              
              if($file_len < 0){
                $file_content_ip = intval(str_replace('x_', '', $file_key[$file_len-1]));
              }else{
                $file_content_ip = 0;
              }

              $len = 0;
              if($file_content_ip >= $post_content_ip){
                $len = $file_content_ip;
              }else{
                $len = $post_content_ip;
              }
              // Content writter in db
              for($i = 0; $i<=$len; $i++){
                if(isset($_POST['y_'.$i]) || isset($_FILES['x_'.$i])){
                  $ss_slide_content = new SsContent();

                  if(isset($_POST['y_'.$i]) && isset($_FILES['x_'.$i])){
                      $temp_field = 'x_'.$i;
                      // Valid Image
                      $error_type = basename($_FILES[$temp_field]["error"]);
                      if($error_type == 0){
                        $LogoImg = $this->validateContentImg($temp_field, $dir_path , 600, 400);
                        $LogoImg = json_decode($LogoImg);
                        if(isset($LogoImg->success) ){
                            $ss_slide_content->Img_path =  $LogoImg->success;
                        }else{
                            // Error - Image is not uploaded into server

                        }
                      }else{
                        // Page Status - Active then It matters or Updating old logo
                        // Images alredy in database then no need to change page status
                                  
                      } 

                      $ss_slide_content->content = $_POST['y_'.$i];


                  }elseif(isset($_POST['y_'.$i])){
                    // Only Content
                    $ss_slide_content->content = $_POST['y_'.$i];

                  }elseif ( isset($_FILES['x_'.$i])) {
                    // Only Image
                    $temp_field = 'x_'.$i;
                      // Valid Image
                    $error_type = basename($_FILES[$temp_field]["error"]);
                    if($error_type == 0){
                      $LogoImg = $this->validateContentImg($temp_field, $dir_path , 600, 600);
                      $LogoImg = json_decode($LogoImg);
                      if(isset($LogoImg->success) ){
                          $ss_slide_content->Img_path =  $LogoImg->success;
                      }else{
                          // Error - Image is not uploaded into server

                      }
                    }else{
                      // Page Status - Active then It matters or Updating old logo
                      // Images alredy in database then no need to change page status
                                
                    } 
                  }
                  $ss_slide_content->ss_id = $model->ss_id;
                  $ss_slide_content->save();
                }
              }
              

              // Media Thumb and Cover Image
              // Media Thumb
              $error_type = basename($_FILES["Shortstory"]["error"]['media_thumb']);
              if($error_type == 0){
                $LogoImg = $this->validateImg('media_thumb', $dir_path , 220, 180);
                $LogoImg = json_decode($LogoImg);
                if(isset($LogoImg->success) ){
                    $model->media_thumb =  $LogoImg->success;
                }else{
                    // Error - Image is not uploaded into server
                  $flag = 0; // Unpubslish

                }
              }else{
                // Page Status - Active then It matters or Updating old logo
                // Images alredy in database then no need to change page status
                if(is_null($model->media_thumb)){
                    $flag = 0;
                }                    
              }                    

              // Cover Image
              // Give complete url in db either selected from topic default images or uploaded by user
              // First prefrence user uploaded images then default image
              $flag_cover = 1;
              $error_type = basename($_FILES["Shortstory"]["error"]['media_cover']);
              if($error_type == 0){
                $LogoImg = $this->validateImg('media_cover', $dir_path , 1600, 400);
                $LogoImg = json_decode($LogoImg);
                if(isset($LogoImg->success) ){
                    $model->media_cover =  '/media/ss/'. $model->ss_id.'/'.$LogoImg->success;
                }else{
                    // Error - Image is not uploaded into server
                  $flag = 0; // Unpubslish
                  $flag_cover = 0;

                }
              }else{
                // Check User selected Image from default topic option
                
                if(is_null($model->media_cover)){
                    $flag = 0;
                }
                $flag_cover = 0;                    
              }  
              // Now checking whether user selected default cover image
              if($flag_cover == 0){
                if(isset($_POST["coverImage"])){
                  if($_POST["coverImage"] != 0){
                      $path = TopicMedia::findOne($_POST["coverImage"]);
                  
                      $model->media_cover = '/media/topic/t/'.$path->topic_id.'/'.$path->media_path;
                      $flag_cover  = 1;
                  }
                }
              }

              //Sub Topic

              $tempcount = $_POST['Shortstory']['subTopic'];
              if($tempcount != null){
                if(count($tempcount) == 1){

                    $model->topic_1 =$_POST['Shortstory']['subTopic'][0]; 
                }elseif(count($tempcount) == 2){
                    $model->topic_1 = $_POST['Shortstory']['subTopic'][0];
                    $model->topic_2 = $_POST['Shortstory']['subTopic'][1];
                }
              }
              

              //Tags
              $tempcount =  $_POST['Shortstory']['tags'];
              if($tempcount != null){
                for($i = 0; $i < count($tempcount); $i++){
                  $SSTags = new SsTags();    
                  $SSTags->ss_id = $model->ss_id;
                  $SSTags->tag_name = $_POST['Shortstory']['tags'][$i];
                  $SSTags->save();
                }  
              }
              


              //if($flag == 0 || $flag_cover == 0){
              if($flag == 0){
                if($model->published_status == 'publish'){
                  
                      // Update session alert
                    $model->published_status  = 'draft';
                  }  
              }

              // Transprency Score
              if(is_null($model->transparency_score)){
                $model->transparency_score = 50;
              }

              if($model->save()){
                return $this->render('view', [
                    'model' => $this->findModel($model->ss_id),
                ]);  
              }              
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }



    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->article_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
