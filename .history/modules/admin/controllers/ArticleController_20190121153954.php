<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'editor';
        $model = new Article();

       
        
        if ($model->load(Yii::$app->request->post()) ) {          
            
            $model->author_id=1;
            
            $model =  $this->findModel($model->article_id);
            
            $flag = 1; // Can Publish SS => Publish_Status = 'publish'
            
            $dir_path = Yii::getAlias('@webroot') .'/media/artical/'. $model->article_id;

            if(! is_dir($dir_path)){
                if(!FileHelper::createDirectory($dir_path, 0775))
                  throw new LocationException($dir_path . ' is not write');
            }
            print_r($_FILES["Shortstory"]["error"]['media_thumb']);
                 die();
            if($model->save()){
                 // Logo File
                 
                 $error_type = basename($_FILES["Article"]["error"]['logoImg']);
                 if($error_type == 0){
                   $LogoImg = $this->validateImg('logoImg', $dir_path , 220, 150);
                   $LogoImg = json_decode($LogoImg);
                   if(isset($LogoImg->success) ){
                       $model->logo_path =  $LogoImg->success;
                   }else{
                       // Error - Image is not uploaded into server
 
                   }
                 }else{
                     // Page Status - Active then It matters or Updating old logo
                     // Images alredy in database then no need to change page status
                     if(is_null($model->logo_path)){
                         if($model->page_status == 1){
                             $model->page_status = 0;
                             // Update session alert
                         }
                     }                    
                 }
 
                 //Cover image
                 $error_type = basename($_FILES["TopicSuper"]["error"]['coverImg']);
                 if($error_type == 0){
                     $LogoImg = $this->validateImg('coverImg', $dir_path , 1600, 400);
                     $LogoImg = json_decode($LogoImg);
                     if(isset($LogoImg->success)){
                         $model->cover_img_path =  $LogoImg->success;
                     }else{
                         // Update session alert
 
                     }
                 }else{
                     // Page Status - Active then It matters or Updating old logo
                     if(is_null($model->cover_img_path)){
                         if($model->page_status == 1){
                             $model->page_status = 0;
                             // Update session alert
                             
                         }
                     }
                 }
 
 
                 // Extra Cover Image
                 // Check cout of extra image in db and file 
                 $count = TopicSuperMedia::find()->where(['super_topic_id'=>$model->super_topic_id])->count('*');
                 if($count == NULL){
                     $count = 0;
                 }
 
                 $count_up = count($_FILES["TopicSuper"]["name"]['extraCoverImg']);
                
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }



    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->article_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function validateImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){

        $file_name =  basename($_FILES["Shortstory"]["name"][$imgFile]);
        $target_file = $dir .'/'.$file_name;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  
        $new_file_name = $this->generateRandomString().".".$imageFileType;
        $target_file = $dir .'/'.$new_file_name;
  
        if (file_exists($target_file)) {
           // echo "Sorry, file already exists.";
            $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
            $target_file =  $dir ."/". $new_file_name;
        }
  
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $check = getimagesize($_FILES["Shortstory"]["tmp_name"][$imgFile]);
         // Width = $check[0];
         // Height = $check[1];
        if($check !== false) {
            if($check[0] >= $width and $check[1] >= $height){
                // Update Value and Upload photo
            }else{
               // Height and width is min 50px width and height
               return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
               $uploadOk = 0;
            }
        }else{
            return json_encode(array("error" => "File is not an image.")) ;
            $uploadOk = 0;
        }
  
        //Checking format of Image
        if(!in_array($imageFileType, $format, TRUE)){
            return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
           $uploadOk = 0;
        }
  
        if ($uploadOk == 0) {
            return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["Shortstory"]["tmp_name"][$imgFile], $target_file)) {
              // Update Model Data
              return  json_encode(array('success'=> $new_file_name));
            } else {
              // $error = "Sorry, there was an error uploading your file.";
              return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
            }
        }
      }
  
      private function validateContentImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){
  
        $file_name =  basename($_FILES[$imgFile]["name"]);
        $target_file = $dir .'/'.$file_name;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  
        $new_file_name = $this->generateRandomString().".".$imageFileType;
        $target_file = $dir .'/'.$new_file_name;
  
        if (file_exists($target_file)) {
           // echo "Sorry, file already exists.";
            $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
            $target_file =  $dir ."/". $new_file_name;
        }
  
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $check = getimagesize($_FILES[$imgFile]["tmp_name"]);
         // Width = $check[0];
         // Height = $check[1];
        if($check !== false) {
            if($check[0] >= $width and $check[1] >= $height){
                // Update Value and Upload photo
            }else{
               // Height and width is min 50px width and height
               return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
               $uploadOk = 0;
            }
        }else{
            return json_encode(array("error" => "File is not an image.")) ;
            $uploadOk = 0;
        }
  
        //Checking format of Image
        if(!in_array($imageFileType, $format, TRUE)){
            return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
           $uploadOk = 0;
        }
  
        if ($uploadOk == 0) {
            return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES[$imgFile]["tmp_name"], $target_file)) {
              // Update Model Data
              return  json_encode(array('success'=> $new_file_name));
            } else {
              // $error = "Sorry, there was an error uploading your file.";
              return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
            }
        }
      }
}
