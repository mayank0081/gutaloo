<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleTags;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\FileHelper;
use app\models\TopicMedia;
/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'editor';
        $model = new Article();

       
        
        if ($model->load(Yii::$app->request->post()) ) {          
            $data = Yii::$app->request->post();
            
            $artical_content = $data['Article']['content'];
            
            //login User Id in Author Id
            $model->author_id=Yii::$app->user->identity->id;
            $model->content_path ='demo';
            
            if($model->save(false)){
            
                 
                $flag = 1; // Can Publish SS => Publish_Status = 'publish'
            
                //dir path is use as slug it unique
                $dir_path = Yii::getAlias('@webroot') .'/media/article/'. $model->article_id;
                $file_name = 'content.json';
            
                if(! is_dir($dir_path)){
                    if(!FileHelper::createDirectory($dir_path, 0775))
                      throw new LocationException($dir_path . ' is not write');
                }
              
                $Articlejson = json_encode($artical_content);
                $jsonfile= $dir_path.'/'.$file_name;
              
                $fp = fopen($jsonfile, 'w+');
                fwrite($fp, $Articlejson);
                fclose($fp);
                $model->content_path = $jsonfile;
             
                // Media Thumb and Cover Image
              // Media Thumb
              $error_type = basename($_FILES["Article"]["error"]['media_thumb']);
              if($error_type == 0){
                $LogoImg = $this->validateImg('media_thumb', $dir_path , 220, 180);
                $LogoImg = json_decode($LogoImg);
                if(isset($LogoImg->success) ){
                    $model->media_thumb =  $LogoImg->success;
                }else{
                    // Error - Image is not uploaded into server
                  $flag = 0; // Unpubslish

                }
              }else{
                // Page Status - Active then It matters or Updating old logo
                // Images alredy in database then no need to change page status
                if(is_null($model->media_thumb)){
                    $flag = 0;
                }                    
              }                    

              // Cover Image
              // Give complete url in db either selected from topic default images or uploaded by user
              // First prefrence user uploaded images then default image
              $flag_cover = 1;
              $error_type = basename($_FILES["Article"]["error"]['media_cover']);
              if($error_type == 0){
                $LogoImg = $this->validateImg('media_cover', $dir_path , 1600, 400);
                
                $LogoImg = json_decode($LogoImg);
                if(isset($LogoImg->success) ){
                    $model->media_cover =  $dir_path.'/'.$LogoImg->success;
                }else{
                    // Error - Image is not uploaded into server
                  $flag = 0; // Unpubslish
                  $flag_cover = 0;

                }
              }else{
                // Check User selected Image from default topic option
                
                if(is_null($model->media_cover)){
                    $flag = 0;
                }
                $flag_cover = 0;                    
              }  
              // Now checking whether user selected default cover image
              if($flag_cover == 0){
                if(isset($_POST["coverImage"])){
                  if($_POST["coverImage"] != 0){
                      $path = TopicMedia::findOne($_POST["coverImage"]);
                  
                      $model->media_cover = $dir_path.'/'.$path->media_path;
                      $flag_cover  = 1;
                  }
                }
              }
            
             
              $tempcount = $_POST['Article']['subTopic'];
              if($tempcount != null){
                if(count($tempcount) == 1){

                    $model->topic_1 =$_POST['Article']['subTopic'][0]; 
                }elseif(count($tempcount) == 2){
                    $model->topic_1 = $_POST['Article']['subTopic'][0];
                    $model->topic_2 = $_POST['Article']['subTopic'][1];
                }
              }
              

              //Tags
              $tempcount =  $_POST['Article']['tags'];
              if($tempcount != null){
                for($i = 0; $i < count($tempcount); $i++){
                  $ArticleTags = new ArticleTags();    
                  $ArticleTags->article_id = $model->article_id;
                  $ArticleTags->tag_name = $_POST['Article']['tags'][$i];
                  $ArticleTags->save();
                }  
              }
              


              //if($flag == 0 || $flag_cover == 0){
              if($flag == 0){
                if($model->published_status == 'publish'){
                  
                      // Update session alert
                    $model->published_status  = 'draft';
                  }  
              }

              // Transprency Score
              if(is_null($model->transprancy_score)){
                $model->transprancy_score = 50;
              }
            }
            if($model->save()){
                return $this->render('view', [
                    'model' => $this->findModel($model->article_id),
                ]);
            }  
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }



    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->article_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function validateImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){

        $file_name =  basename($_FILES["Article"]["name"][$imgFile]);
        $target_file = $dir .'/'.$file_name;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  
        $new_file_name = $this->generateRandomString().".".$imageFileType;
        $target_file = $dir .'/'.$new_file_name;
  
        if (file_exists($target_file)) {
           // echo "Sorry, file already exists.";
            $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
            $target_file =  $dir ."/". $new_file_name;
        }
  
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $check = getimagesize($_FILES["Article"]["tmp_name"][$imgFile]);
         // Width = $check[0];
         // Height = $check[1];
         print_r($check[0]);
         print_r('X');
         print_r($check[1]);
         die();
        if($check !== false) {
            if($check[0] >= $width and $check[1] >= $height){
                // Update Value and Upload photo
            }else{
               // Height and width is min 50px width and height
               return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
               $uploadOk = 0;
            }
        }else{
            return json_encode(array("error" => "File is not an image.")) ;
            $uploadOk = 0;
        }
  
        //Checking format of Image
        if(!in_array($imageFileType, $format, TRUE)){
            return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
           $uploadOk = 0;
        }
  
        if ($uploadOk == 0) {
            return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["Article"]["tmp_name"][$imgFile], $target_file)) {
              // Update Model Data
              return  json_encode(array('success'=> $new_file_name));
            } else {
              // $error = "Sorry, there was an error uploading your file.";
              return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
            }
        }
      }
  
      private function validateContentImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){
  
        $file_name =  basename($_FILES[$imgFile]["name"]);
        $target_file = $dir .'/'.$file_name;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  
        $new_file_name = $this->generateRandomString().".".$imageFileType;
        $target_file = $dir .'/'.$new_file_name;
  
        if (file_exists($target_file)) {
           // echo "Sorry, file already exists.";
            $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
            $target_file =  $dir ."/". $new_file_name;
        }
  
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $check = getimagesize($_FILES[$imgFile]["tmp_name"]);
         // Width = $check[0];
         // Height = $check[1];
        if($check !== false) {
            if($check[0] >= $width and $check[1] >= $height){
                // Update Value and Upload photo
            }else{
               // Height and width is min 50px width and height
               return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
               $uploadOk = 0;
            }
        }else{
            return json_encode(array("error" => "File is not an image.")) ;
            $uploadOk = 0;
        }
  
        //Checking format of Image
        if(!in_array($imageFileType, $format, TRUE)){
            return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
           $uploadOk = 0;
        }
  
        if ($uploadOk == 0) {
            return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES[$imgFile]["tmp_name"], $target_file)) {
              // Update Model Data
              return  json_encode(array('success'=> $new_file_name));
            } else {
              // $error = "Sorry, there was an error uploading your file.";
              return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
            }
        }
      }

      private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
