<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Shortstory;
use app\models\ShortstorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\models\Topic;
use app\models\TopicMedia;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use app\models\SsTags;
use app\models\SsContent;
/**
 * SsController implements the CRUD actions for Shortstory model.
 */
class SsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shortstory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShortstorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shortstory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Shortstory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'editor';
        $model = new Shortstory();

        
        
        if ($model->load(Yii::$app->request->post()) ) {          
            
            if($model->save()){

              $model =  $this->findModel($model->ss_id);
              $flag = 1; // Can Publish SS => Publish_Status = 'publish'
              
              $dir_path = Yii::getAlias('@webroot') .'/media/ss/'. $model->ss_id;

              if(! is_dir($dir_path)){
                  if(!FileHelper::createDirectory($dir_path, 0775))
                    throw new LocationException($dir_path . ' is not write');
              }


              //Content 

              $post_array = array_filter($_POST, function ($key) {
                return strpos($key, 'y_') === 0;
              }, ARRAY_FILTER_USE_KEY);
              
              $post_key = array_keys($post_array);
              $post_len = sizeof($post_key);
              
              if($post_len < 0){
                $post_content_ip = intval(str_replace('y_', '', $post_key[$post_len-1]));
              }else{
                $post_content_ip = 0;
              }

              $file_array = array_filter($_FILES, function ($key) {
                    return strpos($key, 'x_') === 0;
                  }, ARRAY_FILTER_USE_KEY);
                  
              $file_key = array_keys($file_array);
              $file_len = sizeof($file_key);
              
              if($file_len < 0){
                $file_content_ip = intval(str_replace('x_', '', $file_key[$file_len-1]));
              }else{
                $file_content_ip = 0;
              }

              $len = 0;
              if($file_content_ip >= $post_content_ip){
                $len = $file_content_ip;
              }else{
                $len = $post_content_ip;
              }
              // Content writter in db
              for($i = 0; $i<=$len; $i++){
                if(isset($_POST['y_'.$i]) || isset($_FILES['x_'.$i])){
                  $ss_slide_content = new SsContent();

                  if(isset($_POST['y_'.$i]) && isset($_FILES['x_'.$i])){
                      $temp_field = 'x_'.$i;
                      // Valid Image
                      $error_type = basename($_FILES[$temp_field]["error"]);
                      if($error_type == 0){
                        $LogoImg = $this->validateContentImg($temp_field, $dir_path , 600, 400);
                        $LogoImg = json_decode($LogoImg);
                        if(isset($LogoImg->success) ){
                            $ss_slide_content->Img_path =  $LogoImg->success;
                        }else{
                            // Error - Image is not uploaded into server

                        }
                      }else{
                        // Page Status - Active then It matters or Updating old logo
                        // Images alredy in database then no need to change page status
                                  
                      } 

                      $ss_slide_content->content = $_POST['y_'.$i];


                  }elseif(isset($_POST['y_'.$i])){
                    // Only Content
                    $ss_slide_content->content = $_POST['y_'.$i];

                  }elseif ( isset($_FILES['x_'.$i])) {
                    // Only Image
                    $temp_field = 'x_'.$i;
                      // Valid Image
                    $error_type = basename($_FILES[$temp_field]["error"]);
                    if($error_type == 0){
                      $LogoImg = $this->validateContentImg($temp_field, $dir_path , 600, 600);
                      $LogoImg = json_decode($LogoImg);
                      if(isset($LogoImg->success) ){
                          $ss_slide_content->Img_path =  $LogoImg->success;
                      }else{
                          // Error - Image is not uploaded into server

                      }
                    }else{
                      // Page Status - Active then It matters or Updating old logo
                      // Images alredy in database then no need to change page status
                                
                    } 
                  }
                  $ss_slide_content->ss_id = $model->ss_id;
                  $ss_slide_content->save();
                }
              }
              

              // Media Thumb and Cover Image
              // Media Thumb
              $error_type = basename($_FILES["Shortstory"]["error"]['media_thumb']);
              if($error_type == 0){
                $LogoImg = $this->validateImg('media_thumb', $dir_path , 220, 180);
                $LogoImg = json_decode($LogoImg);
                if(isset($LogoImg->success) ){
                    $model->media_thumb =  $LogoImg->success;
                }else{
                    // Error - Image is not uploaded into server
                  $flag = 0; // Unpubslish

                }
              }else{
                // Page Status - Active then It matters or Updating old logo
                // Images alredy in database then no need to change page status
                if(is_null($model->media_thumb)){
                    $flag = 0;
                }                    
              }                    

              // Cover Image
              // Give complete url in db either selected from topic default images or uploaded by user
              // First prefrence user uploaded images then default image
              $flag_cover = 1;
              $error_type = basename($_FILES["Shortstory"]["error"]['media_cover']);
              if($error_type == 0){
                $LogoImg = $this->validateImg('media_cover', $dir_path , 1600, 400);
                $LogoImg = json_decode($LogoImg);
                if(isset($LogoImg->success) ){
                    $model->media_cover =  '/media/ss/'. $model->ss_id.'/'.$LogoImg->success;
                }else{
                    // Error - Image is not uploaded into server
                  $flag = 0; // Unpubslish
                  $flag_cover = 0;

                }
              }else{
                // Check User selected Image from default topic option
                
                if(is_null($model->media_cover)){
                    $flag = 0;
                }
                $flag_cover = 0;                    
              }  
              // Now checking whether user selected default cover image
              if($flag_cover == 0){
                if(isset($_POST["coverImage"])){
                  if($_POST["coverImage"] != 0){
                      $path = TopicMedia::findOne($_POST["coverImage"]);
                  
                      $model->media_cover = '/media/topic/t/'.$path->topic_id.'/'.$path->media_path;
                      $flag_cover  = 1;
                  }
                }
              }

              //Sub Topic

              $tempcount = $_POST['Shortstory']['subTopic'];
              if($tempcount != null){
                if(count($tempcount) == 1){

                    $model->topic_1 =$_POST['Shortstory']['subTopic'][0]; 
                }elseif(count($tempcount) == 2){
                    $model->topic_1 = $_POST['Shortstory']['subTopic'][0];
                    $model->topic_2 = $_POST['Shortstory']['subTopic'][1];
                }
              }
              

              //Tags
              $tempcount =  $_POST['Shortstory']['tags'];
              if($tempcount != null){
                for($i = 0; $i < count($tempcount); $i++){
                  $SSTags = new SsTags();    
                  $SSTags->ss_id = $model->ss_id;
                  $SSTags->tag_name = $_POST['Shortstory']['tags'][$i];
                  $SSTags->save();
                }  
              }
              


              //if($flag == 0 || $flag_cover == 0){
              if($flag == 0){
                if($model->published_status == 'publish'){
                  
                      // Update session alert
                    $model->published_status  = 'draft';
                  }  
              }

              // Transprency Score
              if(is_null($model->transparency_score)){
                $model->transparency_score = 50;
              }

              if($model->save()){
                return $this->render('view', [
                    'model' => $this->findModel($model->ss_id),
                ]);  
              }              
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Shortstory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = 'editor';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

          // echo "<pre>";
          // print_r($_POST);
          // echo "</pre>";
          // die();

          $flag = 1; // Can Publish SS => Publish_Status = 'publish'
          
          $dir_path = Yii::getAlias('@webroot') .'/media/ss/'. $model->ss_id;

          if(! is_dir($dir_path)){
              if(!FileHelper::createDirectory($dir_path, 0775))
                throw new LocationException($dir_path . ' is not write');
          }


          //Content 

          // Content Update

          $post_array = array_filter($_POST, function ($key) {
            return strpos($key, 'Y_') === 0;
          }, ARRAY_FILTER_USE_KEY);
          
          $post_key = array_keys($post_array);
          $post_len = sizeof($post_key);
          
          
          for($i =0; $i < $post_len; $i++){
            $temp_content_id = intval(str_replace('Y_', '', $post_key[$i]));

            $temp_model = SsContent::find()->where(['id'=>$temp_content_id])->one();
            $temp_model->content = $_POST['Y_'.$temp_content_id];
            $temp_model->save();
          }
   



          $file_array = array_filter($_FILES, function ($key) {
                return strpos($key, 'X_') === 0;
              }, ARRAY_FILTER_USE_KEY);
              
          $file_key = array_keys($file_array);
          $file_len = sizeof($file_key);
          
          for($i =0; $i < $file_len; $i++){
            $temp_content_id = intval(str_replace('X_', '', $file_key[$i]));
            $temp_model = SsContent::find()->where(['id'=>$temp_content_id])->one();

            $temp_field = 'X_'.$temp_content_id;
                // Valid Image
              $error_type = basename($_FILES[$temp_field]["error"]);
              if($error_type == 0){
                $LogoImg = $this->validateContentImg($temp_field, $dir_path , 600, 400);
                $LogoImg = json_decode($LogoImg);
                if(isset($LogoImg->success) ){
                    $temp_model->Img_path =  $LogoImg->success;
                }else{
                    // Error - Image is not uploaded into server

                }
              }else{
                // Page Status - Active then It matters or Updating old logo
                // Images alredy in database then no need to change page status
                          
              } 
              $temp_model->save();

            
          } 

          // Content New

          $post_array = array_filter($_POST, function ($key) {
            return strpos($key, 'y_') === 0;
          }, ARRAY_FILTER_USE_KEY);
          
          $post_key = array_keys($post_array);
          $post_len = sizeof($post_key);
          
          if($post_len < 0){
            $post_content_ip = intval(str_replace('y_', '', $post_key[$post_len-1]));
          }else{
            $post_content_ip = 0;
          }
          
          
          $file_array = array_filter($_FILES, function ($key) {
                return strpos($key, 'x_') === 0;
              }, ARRAY_FILTER_USE_KEY);
              
          $file_key = array_keys($file_array);
          $file_len = sizeof($file_key);
          
          if($file_len < 0){
            $file_content_ip = intval(str_replace('x_', '', $file_key[$file_len-1]));
          }else{
            $file_content_ip = 0;
          }
          
          $len = 0;
          if($file_content_ip >= $post_content_ip){
            $len = $file_content_ip;
          }else{
            $len = $post_content_ip;
          }
          // Content writter in db
          for($i = 0; $i<=$len; $i++){
            
            if(isset($_POST['y_'.$i]) || isset($_FILES['x_'.$i])){
              $ss_slide_content = new SsContent();
              if(isset($_POST['y_'.$i]) && isset($_FILES['x_'.$i])){
                  $temp_field = 'x_'.$i;
                  // Valid Image
                  $error_type = basename($_FILES[$temp_field]["error"]);
                  if($error_type == 0){
                    $LogoImg = $this->validateContentImg($temp_field, $dir_path , 600, 400);
                    $LogoImg = json_decode($LogoImg);
                    if(isset($LogoImg->success) ){
                        $ss_slide_content->Img_path =  $LogoImg->success;
                    }else{
                        // Error - Image is not uploaded into server

                    }
                  }else{
                    // Page Status - Active then It matters or Updating old logo
                    // Images alredy in database then no need to change page status
                              
                  } 

                  $ss_slide_content->content = $_POST['y_'.$i];


              }elseif(isset($_POST['y_'.$i])){
                // Only Content
                $ss_slide_content->content = $_POST['y_'.$i];

              }elseif ( isset($_FILES['x_'.$i])) {
                // Only Image
                $temp_field = 'x_'.$i;
                  // Valid Image
                $error_type = basename($_FILES[$temp_field]["error"]);
                if($error_type == 0){
                  $LogoImg = $this->validateContentImg($temp_field, $dir_path , 600, 600);
                  $LogoImg = json_decode($LogoImg);
                  if(isset($LogoImg->success) ){
                      $ss_slide_content->Img_path =  $LogoImg->success;
                  }else{
                      // Error - Image is not uploaded into server

                  }
                }else{
                  // Page Status - Active then It matters or Updating old logo
                  // Images alredy in database then no need to change page status
                            
                } 
              }
              $ss_slide_content->ss_id = $model->ss_id;
              $ss_slide_content->save();
            }
          }
          

          // Media Thumb and Cover Image
          // Media Thumb
          $error_type = basename($_FILES["Shortstory"]["error"]['media_thumb']);
          if($error_type == 0){
            $LogoImg = $this->validateImg('media_thumb', $dir_path , 220, 150);
            $LogoImg = json_decode($LogoImg);
            if(isset($LogoImg->success) ){
                $model->media_thumb =  $LogoImg->success;
            }else{
                // Error - Image is not uploaded into server
              $flag = 0; // Unpubslish

            }
          }else{
            // Page Status - Active then It matters or Updating old logo
            // Images alredy in database then no need to change page status
            if(is_null($model->media_thumb)){
                $flag = 0;
            }                    
          }                    

          // Cover Image
          // Give complete url in db either selected from topic default images or uploaded by user
          // First prefrence user uploaded images then default image
          $flag_cover = 1;
          $error_type = basename($_FILES["Shortstory"]["error"]['media_cover']);
          if($error_type == 0){
            $LogoImg = $this->validateImg('media_cover', $dir_path , 1600, 400);
            $LogoImg = json_decode($LogoImg);
            if(isset($LogoImg->success) ){
                $model->media_cover =  '/media/ss/'. $model->ss_id.'/'.$LogoImg->success;
            }else{
                // Error - Image is not uploaded into server
              $flag = 0; // Unpubslish
              $flag_cover = 0;

            }
          }else{
            // Check User selected Image from default topic option
            
            if(is_null($model->media_cover)){
                $flag = 0;
            }
            $flag_cover = 0;                    
          }  
          // Now checking whether user selected default cover image
          if($flag_cover == 0){
            if(isset($_POST["coverImage"])){
              if($_POST["coverImage"] != 0){
                  $path = TopicMedia::findOne($_POST["coverImage"]);
                  $model->media_cover = '/media/topic/t/'.$path->topic_id.'/'.$path->media_path;
                  $flag_cover  = 1;
              }
            }
          }

          //Sub Topic

          $tempcount =  $_POST['Shortstory']['subTopic'];
  
          if($tempcount == null){
           
            if(!is_null($model->topic_1)){
              $temp = Topic::find()->where(['topic_id'=>$model->topic_1])->one();
         
              if(!is_null($model->main_topic)){
                if($model->main_topic != $temp->super_topic_id){
                  $model->topic_1 = null;

                  if(!is_null($model->topic_2)){
                    $model->topic_2 = null;
                  }
                }
              }
            }
          }elseif (count($tempcount) == 1) {
            $model->topic_1 =$_POST['Shortstory']['subTopic'][0]; 
            if(!is_null($model->topic_2)){
              $model->topic_2 = null;
            }
          }elseif(count($tempcount) == 2){
            $model->topic_1 = $_POST['Shortstory']['subTopic'][0];
            $model->topic_2 = $_POST['Shortstory']['subTopic'][1];
          }
            

          //Tags
          $ss_old_tags = SsTags::deleteAll(['ss_id'=> $model->ss_id]);
          $tempcount =  $_POST['Shortstory']['tags'];
          if($tempcount != null){
            for($i = 0; $i < count($tempcount); $i++){
              $SSTags = new SsTags();    
              $SSTags->ss_id = $model->ss_id;
              $SSTags->tag_name = $_POST['Shortstory']['tags'][$i];
              $SSTags->save();
            }  
          }
          


          //if($flag == 0 || $flag_cover == 0){
          if($flag == 0){
            if($model->published_status == 'publish'){
              
                  // Update session alert
                $model->published_status  = 'draft';
              }  
          }

          // Transprency Score
          if(is_null($model->transparency_score)){
            $model->transparency_score = 50;
          }

          if($model->save()){
            return $this->render('view', [
                'model' => $this->findModel($model->ss_id),
            ]);  
          }              
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Shortstory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shortstory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shortstory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shortstory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAb(){
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $topic_id = $parents[0];

              //  $out = self::getSubCatList($cat_id); 
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                $super = Topic::find()->select(['topic_id','topic_name'])->where(['super_topic_id'=>$topic_id, "visibility_status" => 1])->all(); 
                $out = ArrayHelper::toArray($super, [
                    'app\models\Topic' => [
                        'id'=>'topic_id',
                        'name'=>'topic_name',
                    ],
                ]);
    
                /*$out = [
                    ['id'=>$parents[0], 'name'=>'abc'],
                    ['id'=>2, 'name'=>'xyz']
                 ];*/
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }


    public function actionCoverimage(){
        if(Yii::$app->request->isAjax){
            $data  = Yii::$app->request->post();
            $topic_id = $data['topic_id'];
            // foreach (array_values($topic_id) as $value){
            //    $newarray[] = $value; 
            // }
            $super = TopicMedia::find()->where(['topic_id'=> $topic_id])->all(); 
            $out = ArrayHelper::toArray($super, [
                    'app\models\TopicMedia' => [
                        'id',
                        'topic_id',                  
                        'media_path',
                    ],
                ]); 
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'search' => $out,
                'code' => 100,
            ];
        }
    } 

    private function validateImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){

      $file_name =  basename($_FILES["Shortstory"]["name"][$imgFile]);
      $target_file = $dir .'/'.$file_name;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

      $new_file_name = $this->generateRandomString().".".$imageFileType;
      $target_file = $dir .'/'.$new_file_name;

      if (file_exists($target_file)) {
         // echo "Sorry, file already exists.";
          $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
          $target_file =  $dir ."/". $new_file_name;
      }

      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $check = getimagesize($_FILES["Shortstory"]["tmp_name"][$imgFile]);
       // Width = $check[0];
       // Height = $check[1];
      if($check !== false) {
          if($check[0] >= $width and $check[1] >= $height){
              // Update Value and Upload photo
          }else{
             // Height and width is min 50px width and height
             return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
             $uploadOk = 0;
          }
      }else{
          return json_encode(array("error" => "File is not an image.")) ;
          $uploadOk = 0;
      }

      //Checking format of Image
      if(!in_array($imageFileType, $format, TRUE)){
          return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
         $uploadOk = 0;
      }

      if ($uploadOk == 0) {
          return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["Shortstory"]["tmp_name"][$imgFile], $target_file)) {
            // Update Model Data
            return  json_encode(array('success'=> $new_file_name));
          } else {
            // $error = "Sorry, there was an error uploading your file.";
            return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
          }
      }
    }

    private function validateContentImg($imgFile, $dir , $width, $height, $format=['png', 'eps', 'gif', 'jpg', 'jpeg'] ){

      $file_name =  basename($_FILES[$imgFile]["name"]);
      $target_file = $dir .'/'.$file_name;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

      $new_file_name = $this->generateRandomString().".".$imageFileType;
      $target_file = $dir .'/'.$new_file_name;

      if (file_exists($target_file)) {
         // echo "Sorry, file already exists.";
          $new_file_name =  rand(10,99)."_".$this->generateRandomString().".".$imageFileType;
          $target_file =  $dir ."/". $new_file_name;
      }

      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $check = getimagesize($_FILES[$imgFile]["tmp_name"]);
       // Width = $check[0];
       // Height = $check[1];
      if($check !== false) {
          if($check[0] >= $width and $check[1] >= $height){
              // Update Value and Upload photo
          }else{
             // Height and width is min 50px width and height
             return json_encode(array("error" => "Image is not correct size. Min height require $height px and Min Width require $width px")) ;
             $uploadOk = 0;
          }
      }else{
          return json_encode(array("error" => "File is not an image.")) ;
          $uploadOk = 0;
      }

      //Checking format of Image
      if(!in_array($imageFileType, $format, TRUE)){
          return json_encode(array("error" => "Sorry, files are not in correct allowed.")) ;
         $uploadOk = 0;
      }

      if ($uploadOk == 0) {
          return json_encode(array("error" => "Sorry, your file was not uploaded.")) ;
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES[$imgFile]["tmp_name"], $target_file)) {
            // Update Model Data
            return  json_encode(array('success'=> $new_file_name));
          } else {
            // $error = "Sorry, there was an error uploading your file.";
            return  json_encode(array('error'=>'Sorry, there was an error uploading your file.'));
          }
      }
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function actionAr(){
      $test = "";
      if(Yii::$app->request->isAjax){
        $data = Yii::$app->request->post();
        $index = intval($data["content_id"]);
        $content = SsContent::find()->where(['id'=>$index])->one();
        if($content->delete()){
          $test = "Deleted Successfully";
        }
        // do your query stuff here
      }else{
        $test = "Refresh Page";
        // do your query stuff here
      }

        // return Json    
      return \yii\helpers\Json::encode($test);
    }

}

