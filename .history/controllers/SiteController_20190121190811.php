<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\Shortstory;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function beforeAction($action) {


        Yii::$app->view->theme = new \yii\base\Theme([
            'pathMap' => ['@app/views' => '@app/themes/front'],
            //'baseUrl' => '@web/themes/admin',
        ]);
        $this->layout = 'main';
        return parent::beforeAction($action);
    }
    
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['logout'],
    //             'rules' => [
    //                 [
    //                     'actions' => ['logout'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new \app\models\ShortstorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
        $dataProvider->query->andWhere(['published_status'=>'publish'])->orderBy(["ss_id"=>SORT_DESC]);
        //$dataProvider->pagination->pageSize=1;
        return $this->render('/site/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $addUser = new SignupForm();
        if ($addUser->load(Yii::$app->request->post())) {

            if ($user = $addUser->NewUser()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }


        $model->password = '';
        return $this->render('login', [
            'model' => $model, 'new' => $addUser
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionTopics($name=null)
    {
        // Super Topic
        $searchModel = new \app\models\TopicSuperSearch();
        $dataProvider = $searchModel->search_front(Yii::$app->request->queryParams);
    //    $dataProvider->pagination->pageSize=40;
        $dataProvider->query->andWhere(['page_status'=>1]);

        // Topic
        $searchModel_1 = new \app\models\TopicSearch();
        $dataProvider_1 = $searchModel_1->search_front(Yii::$app->request->queryParams);
        $dataProvider_1->query->andWhere(['page_status'=>1]);
    //    $dataProvider_1->pagination->pageSize=40;


        return $this->render('topics', [
            'SuperModel' => $searchModel,
            'SuperProvider' => $dataProvider,
            'TopicModel' => $searchModel_1,
            'TopicProvider' =>  $dataProvider_1
        ]);
    }
    
    public function actionTopic($type, $id, $name)
    {   
        if($type == 's'){
            $model = \app\models\TopicSuper::find()->where(['super_topic_id' => $id])->one(); 
            $searchModel = new \app\models\ShortstorySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
            $dataProvider->query->andWhere(['main_topic' => $model->super_topic_id, 'published_status'=>'publish'])->orderBy(["ss_id"=>SORT_DESC]);
            // echo "<pre>";
            // print_r($dataProvider);
            // echo "</pre>";
           // die;
        }elseif($type == 't'){
            $model = \app\models\Topic::find()->where(['topic_id' => $id])->one();
            $searchModel = new \app\models\ShortstorySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
            $dataProvider->query->orWhere(['or','topic_1' => $model->topic_id, 'topic_2'=> $model->topic_id]);
            $dataProvider->query->andWhere(['published_status'=>'publish'])->orderBy(["ss_id"=>SORT_DESC]);
        }    
        return $this->render('topic_detail', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,          
        ]);
    }

    public function actionSs($slug='', $id=0){
     
        if($id==0){
            $ss_main = Shortstory::find()->where('slug LIKE :slug',[':slug'=>$slug])->all();
            if(!empty($ss_main)){$id = $ss_main['0']['ss_id'];}
        }else{
            
            $ss_main = Shortstory::find()->where(['ss_id'=>$id])->all();
            $slug = $ss_main['0']['slug'];
         
        } 
        return $this->render('ss', [
            'model'=>$ss_main['0'],
            'slug' => $slug,
            'id'=>$id
        ]);
    }

    
}
