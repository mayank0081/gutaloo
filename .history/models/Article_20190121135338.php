<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property int $article_id
 * @property string $title
 * @property string $slug
 * @property string $created_date
 * @property string $published_status
 * @property int $main_topic
 * @property int $topic_1
 * @property int $topic_2
 * @property string $content_path
 * @property string $media_thumb
 * @property string $media_cover
 * @property int $transprancy_score
 * @property string $description
 * @property string $language
 * @property string $article_type
 * @property string $published_date
 * @property int $author_id
 * @property int $approved_user_id
 * @property int $channel_id
 *
 * @property TopicSuper $mainTopic
 * @property Topic $topic1
 * @property Topic $topic2
 * @property User $author
 * @property User $approvedUser
 * @property Channel $channel
 * @property ArticleTags[] $articleTags
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $subTopic;
    public $content;
    public $tags;

    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content_path', 'author_id'], 'required'],
            [['created_date', 'published_date'], 'safe'],
            [['main_topic', 'topic_1', 'topic_2', 'transprancy_score', 'author_id', 'approved_user_id', 'channel_id'], 'integer'],
            [['title', 'media_thumb', 'media_cover'], 'skipOnError' => true, ,'string', 'max' => 150],
            [['slug'], 'string', 'max' => 200],
            [['published_status', 'language'], 'string', 'max' => 20],
            [['content_path', 'description'], 'string', 'max' => 255],
            [['article_type'], 'string', 'max' => 50],
            [['main_topic'], 'exist', 'skipOnError' => true, 'targetClass' => TopicSuper::className(), 'targetAttribute' => ['main_topic' => 'super_topic_id']],
            [['topic_1'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::className(), 'targetAttribute' => ['topic_1' => 'topic_id']],
            [['topic_2'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::className(), 'targetAttribute' => ['topic_2' => 'topic_id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['approved_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['approved_user_id' => 'id']],
            [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Channel::className(), 'targetAttribute' => ['channel_id' => 'channel_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'article_id' => 'Article ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'created_date' => 'Created Date',
            'published_status' => 'Published Status',
            'main_topic' => 'Main Topic',
            'topic_1' => 'Topic 1',
            'topic_2' => 'Topic 2',
            'content_path' => 'Content Path',
            'media_thumb' => 'Media Thumb',
            'media_cover' => 'Media Cover',
            'transprancy_score' => 'Transprancy Score',
            'description' => 'Description',
            'language' => 'Language',
            'article_type' => 'Article Type',
            'published_date' => 'Published Date',
            'author_id' => 'Author ID',
            'approved_user_id' => 'Approved User ID',
            'channel_id' => 'Channel ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainTopic()
    {
        return $this->hasOne(TopicSuper::className(), ['super_topic_id' => 'main_topic']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic1()
    {
        return $this->hasOne(Topic::className(), ['topic_id' => 'topic_1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic2()
    {
        return $this->hasOne(Topic::className(), ['topic_id' => 'topic_2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'approved_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['channel_id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleTags()
    {
        return $this->hasMany(ArticleTags::className(), ['article_id' => 'article_id']);
    }
}
