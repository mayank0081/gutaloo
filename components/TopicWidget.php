<?php


namespace app\components;
use yii\base\Widget;
use yii\helpers\Html;
use Yii;
use app\models\TopicNode;

class TopicWidget extends Widget{
   
    
    public function init(){
        parent::init();
        
    }
    
    public function run()
    {
		//$model = \app\models\Topic::find()->limit(5)->all();

		if(isset($_GET["type"]) && isset($_GET["id"])){
			// Topic-Detail Page
			if($_GET["type"] == 's'){
				// Super Topic Details page
				$model = \app\models\Topic::find()->where(['visibility_status' => 1, 'super_topic_id' => $_GET["id"]])->all();		
			}else{
				// Topic Details Page
				// Type == 't'
				$node = \app\models\TopicNode::find()->where(['or', 'topic_id'=>$_GET["id"], 'in_relation' => $_GET["id"]])->asArray()->all();
				$a = array_merge(array_column($node, "topic_id"),array_column($node, "in_relation"));
				$topic_ids = array_unique($a);
				$model = \app\models\Topic::find()->where(['topic_id' => $topic_ids ])->all();
				
			}
		}else{
			// Index Page
			$model = \app\models\Topic::find()->where(['visibility_status' => 1])->all();	
		}
		
		return $this->render('topic', ['model'=>$model]);
	}
}
?>