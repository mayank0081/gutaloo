<?php
namespace app\components;
use yii\base\Widget;
use yii\helpers\Html;
use Yii;
class HeaderWidget extends Widget{
   
    
    public function init(){
        parent::init();
        
    }
    
    public function run()
    {
	//	$model = \app\models\Sitesetting::findOne(['id'=>'1']);		
		$user = \app\models\User::findOne(['id'=>Yii::$app->user->id]);
	//	return $this->render('header', ['model'=>$model,'user'=>$user]);
    	return $this->render('header', ['user' => $user]);
	}
}
?>