<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
?>
<!-- SIDENAV BAR START -->
<div id="mySidenav" class="sidenav mCustomScrollbar">

  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="logo">
      <figure>
        <a href="<?=Url::to(['/']);?>"><img src="<?php echo Url::to(['/themes/front/images/logo-m.png']); ?>" alt="logo"></a>
      </figure>
    </div>
<nav class="navbar navbar-default">
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?=Url::to(['/']);?>"><img src="<?php echo Url::to(['/themes/front/images/home.png']); ?>" alt="img"> <?php echo Yii::t('app', 'Home')?><span class="sr-only">(current)</span></a></li>
         
          <li><a href="<?=Url::to(['/trending']);?>"><img src="<?php echo Url::to(['/themes/front/images/trending.png']); ?>" alt="img"> <?php echo Yii::t('app', 'Trending')?><span class="sr-only">(current)</span></a></li>

          <li><a href="<?=Url::to(['/bookmark']);?>"><img src="<?php echo Url::to(['/themes/front/images/page.png']); ?>" alt="img"> <?php echo Yii::t('app', 'Bookmark')?><span class="sr-only">(current)</span></a></li> 

        <?php if(!empty($categories)) { ?>
          <li><a href="javascript:void(0);"><img src="<?php echo Url::to(['/themes/front/images/category.png']); ?>" alt="img">Categories <span class="caret"></span></a></li>
          <ul class="category-menu mCustomScrollbar">
            <?php foreach($categories as $catData){?>
              <li><a href="<?=Url::to(['/site/topic','type'=> 's', 'id' => $catData->super_topic_id, 'name' => $catData->topic_name]);?>"><?=ucfirst($catData->topic_name);?></a></li>
           <?php } ?>
         </ul>
       <?php } ?>
          
        </ul>
      </div><!-- /.navbar-collapse -->
    </nav>
    <footer>
      <div class="btn-bar">
        <a class="btn-login" href="#"><img src="<?php echo Url::to(['/themes/front/images/login.png'])?>" alt="login">login</a>
        <a class="btn-signup" href="#"><img src="<?php echo Url::to(['/themes/front/images/signup.png'])?>" alt="sign-up">sign up</a>
      </div>
      <h2>Newsletter</h2>
      <p>Don't miss a thing!Sign up to receive daily news</p>
      <div class="search-bar">
        <input type="text" class="form-control" placeholder="Email...">
        <a class="search-btn" href="#"><img src="<?php echo Url::to(['/themes/front/images/search.png']); ?>" alt="search"></a>
      </div>
      <div class="social-icon">
        <ul>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/f-facebook.png']); ?>" alt="facebook"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/f-twitter.png']); ?>" alt="twitter"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/f-instagram.png']); ?>" alt="instagram"></a></li>
            <li><a href="#"><img src="<?php echo Url::to(['/themes/front/images/f-google-plus.png']); ?>" alt="google-plus"></a></li>
        </ul>
      </div>
      <div class="menu-btn">
        <ul>
          <li>
            <a href="#">About Us</a>
          </li>
          <li>
            <a href="#">FAQ</a>
          </li>
          <li>
            <a href="#">Contact Us</a>
          </li>
          <li>
            <a href="#">Privacy</a>
          </li>
          <li>
            <a href="#">Terms & Condition</a>
          </li>
        </ul>
      </div>
    </footer>
</div>
<!-- SIDENAV BAR END -->
  <div id="overlay" onclick="off()"></div>
<!-- SIDENAV BAR END -->