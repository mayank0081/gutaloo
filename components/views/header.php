<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
?>
<header>
    <div class="main-header">
        <div class="container">
            <div class="row margin-top-mobile">
                <div class="col-sm-3 col-xs-2 flex-column">
                    <div id="side-menu">
                        <span id="one"  onclick="openNav()"><i class="fas fa-bars"></i></span>
                        <span  id="two" onclick="closeNav()"><i class="fas fa-bars"></i></span>
                        <a href="#"><i class="fas fa-chart-line"></i></a>
                        <a href="#"><i class="far fa-bookmark"></i></a>
                        <a href="#"><i class="far fa-bell"></i></a>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <div class="logo-center">
                        <a href="<?=Url::to(['/']);?>" > <img src="<?php echo Url::to(['/themes/front/images/Gutaloo-logo-center.png']); ?>" alt="logo.png"></a>
                    </div>
                </div>
                <div class="column-interchange">
                    <div class="col-sm-12 flex-desktop">
                        <div class="col-sm-5 col-xs-5 order-change">
                            <div class="text-right">
                                <div class="btn-bar">
                                    <a class="btn-login" data-toggle="modal" data-target="#myModal"  href="#">Log in/Sign up</a>
                                    <div class="user-img">
                                        <figure>
                                            <?php 
                                              if(!empty($user->avatar)){
                                                  echo Html::img('@web/media/profileimage/thumb/'.$user->avatar,['class' => 'userImg','width' => 34, 'height' => 34]); 
                                              }else{
                                                echo Html::img('@web/media/gallery/profile-pic.jpg',['class' => 'userImg']);    
                                              }
                                            ?>
                                        </figure>
                                    </div>
           
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 col-xs-12 flex-search-box">          
                            <div class="col-sm-12 flex-search-box">
                                <form class="searchbox">
                                    <input type="search" placeholder="Search......" name="search" class="searchbox-input" onkeyup="buttonUp();" required>
                                    <button type="submit" class="searchbox-submit"><i class="fa fa-search"></i></button>
                                    <span class="searchbox-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                </form>
                            </div>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>     
</header>
<!-- Modal -->

<div class="modal-wrapper">
    <div class="container">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
       
                    <div class="modal-body">
        
                        <div class="login-sign-page">
                            <div class="container modal-container">
  

                                <div class="w-background">
                                    <div class="logo">
                                        <figure><img src="<?php echo Url::to(['/themes/front/images/logo.png'])?>" alt="img"></figure>
                                        <h4>A place to share knowledge and better understand the world</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <form>
                                                <h2>Sign Up</h2>
                                                <p>Fill in the form below to get instant access </p>
                                                <div class="same">
                                                    <label>First Name</label>
                                                    <input class="form-control" type="text" name="first name">
                                                </div>
                                                <div class="same">
                                                    <label>Last Name</label>
                                                    <input class="form-control" type="text" name="Last name">
                                                </div>
                                                <label>Email</label>
                                                <input class="form-control" type="email" name="email">
                                                <label>Password</label>
                                                <input class="form-control" type="email" name="Password">
                                                <span><input type="checkbox"> I Agree to the terms of use </span>
                                                <button type="submit"><img src="<?php echo Url::to(['/themes/front/images/w-signup.png'])?>" alt="w-sign-up">sign up</button>
                                            </form>
                                        </div>  
                                        <div class="col-sm-6">
                                            <form class="login-form">
                                                <h2>Login</h2>
                                                <p class="login-p">Enter username and password to log on</p>
                                                <input class="form-control" type="text" placeholder="First Name" name="first-name">
                                                <input class="form-control" type="text" placeholder="Last Name" name="last-name">
                                                <span><a href="#">Forget Password ?</a></span>
                                                <button type="submit"><img src="<?php echo Url::to(['/themes/front/images/w-login.png'])?>" alt="w-login">sign up</button>
                                                <p class="login-with">...or login with</p>
                                                <div class="social-btn">
                                                     <a href="#"><img src="<?php echo Url::to(['/themes/front/images/googel-btn.png'])?>" alt="googel-btn"></a>
                                                    <a href="#"><img src="<?php echo Url::to(['/themes/front/images/facebook-btn.png'])?>" alt="facebook-btn"></a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn theme-btn" data-dismiss="modal">Close</button>
                        <button type="button" class="btn theme-btn">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
