<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
?>

<div class="left-section-wrapper">
	<h2><a href="#">feed topics</a></h2>
	<div class="left-section container-flow" id="style-1">
		<div class="feed-topic-relative">
			<div class="text">
	    		<ul>
	    			<?php if(!empty($model)){
		  			  foreach($model as $topic){
					?>
	              <li><a href="<?=Url::to(['/t','id' => $topic->topic_id]);?>"><figure><img src="<?php echo Url::to(['/media/topic/t/'.$topic->topic_id.'/'.$topic->logo_path])?>" alt="menu1"></figure><span><?=ucfirst($topic->topic_name);?></span></a></li>
	               <?php } } ?>
	            </ul>
			</div>
		 </div>
		<div class="bottom-absolute">
		<h2><a href="<?=Url::to(['/site/topics']);?>"><?=Yii::t('app','All topics');?></a></h2>
		</div>
	</div>
</div>