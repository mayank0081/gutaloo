<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
?>

<h2><a href="#">Trending</a></h2>
<div class="right-section container-flow" id="style-1">
	<div class="feed-topic-relative">
		<div class="text">
			<ul>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="img-1"></a></figure>
					<a class="youtube-btn" href="#"><img src="<?php echo Url::to(['/themes/front/images/youtube-icon.png'])?>" alt="youtube-icon"></a>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li>
				<li>
					<p><a href="#">Lorem ipsum dolor sit, consectetur adipiscing elit.</a></p>
					<label>BY Author</label>
					<figure><a href="#"><img src="<?php echo Url::to(['/themes/front/images/tranding.jpg'])?>" alt="tranding"></a></figure>
				</li> 
			</ul>
		</div>
	</div>
	<div class="bottom-absolute">
	<h2><a href="#">All Topics</a></h2>
	</div>
</div>
			