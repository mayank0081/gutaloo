<?php
namespace app\components;
use yii\base\Widget;
use yii\helpers\Html;
use Yii;
class SidenavWidget extends Widget{
   
    
    public function init(){
        parent::init();
        
    }
    
    public function run()
    {
		$categories = \app\models\TopicSuper::find()->where(['page_status' => 1])->all();
		return $this->render('sidenav',['categories' => $categories]);
	}
}
?>