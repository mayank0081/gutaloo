$.fn.isOnScreen = function(){

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top ));

};

$(window).bind("scroll",function(){
 if($("#aricle-social").isOnScreen()===true){
     $(".left-section").hide()
 }
 else{
    $(".left-section").show()
 }
})


$(window).bind("scroll",function(){
 if($("#aricle-social").isOnScreen()===true){
     $(".right-section-wrapper").hide()
 }
 else{
    $(".right-section-wrapper").show()
 }
})

