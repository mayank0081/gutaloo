  $(document).ready(function(){
            var submitIcon = $('.searchbox-alltopic-icon');
            var inputBox = $('.searchbox-alltopic-input');
            var searchBox = $('.searchbox-alltopic');
            var isOpen = false;
            submitIcon.click(function(){
                if(isOpen == false){
                    searchBox.addClass('searchbox-alltopic-open');
                    inputBox.focus();
                    isOpen = true;
                } else {
                    searchBox.removeClass('searchbox-alltopic-open');
                    inputBox.focusout();
                    isOpen = false;
                }
            });  
             submitIcon.mouseup(function(){
                    return false;
                });
            searchBox.mouseup(function(){
                    return false;
                });
            $(document).mouseup(function(){
                    if(isOpen == true){
                        $('.searchbox-alltopic-icon').css('display','block');
                        submitIcon.click();
                    }
                });
        });
            function buttonUp(){
                var inputVal = $('.searchbox-alltopic-input').val();
                inputVal = $.trim(inputVal).length;
                if( inputVal !== 0){
                    $('.searchbox-alltopic-icon').css('display','none');
                } else {
                    $('.searchbox-alltopic-input').val('');
                    $('.searchbox-alltopic-icon').css('display','block');
                }
            }			